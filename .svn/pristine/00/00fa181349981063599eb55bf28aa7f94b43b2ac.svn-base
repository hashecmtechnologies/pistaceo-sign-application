import {
  Component,
  OnInit,
  ChangeDetectorRef
} from '@angular/core';
import {
  ToastrService
} from 'ngx-toastr';
import {
  ReportService
} from '../../../service/report.service';
import {
  TranslateService
} from '@ngx-translate/core';
import {
  DomSanitizer, SafeResourceUrl
} from '@angular/platform-browser';
import {
  BreadcrumbService
} from '../layout/breadcrumb/breadcrumb.service';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  IntegrationService
} from '../../../service/integration.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { DocumentService } from '../../../service/document.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  public reportResults = [];
  public reportPdf = false;
  public pdfUrl: SafeResourceUrl;
  public reportName: any;
  public showResports = false;
  public formFilter:any;
  public tempelateRender: FormGroup;
  public selectedReport;
  public activeIndexNumber = 0;
  public formFilterproperties = [];
  public ddmmyy = false;
  public htmlData;
  public reportHTML= false;
  public reportFilter;
  constructor(private tr: ToastrService, private rs: ReportService, private changeDetectorRef: ChangeDetectorRef, public ngxSmartModalService: NgxSmartModalService,
      private translate: TranslateService, private fb: FormBuilder, public integrationservice: IntegrationService,private ds: DocumentService,
      private _sanitizer: DomSanitizer, private breadcrumbeServece: BreadcrumbService, private spinerService: Ng4LoadingSpinnerService) {

        const browserLang: string = translate.getBrowserLang();
      translate.use('en');
      translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
      this.breadcrumbeServece.setItems([{
          label: this.translate.instant('Reports')
      }]);
      this.tempelateRender = this.fb.group({});

  }

  ngOnInit() {
    if(localStorage.getItem('Date Format') === 'DD/MM/YYYY'){
        this.ddmmyy = true;
    }else{
        this.ddmmyy = false;
    }
      this.rs.getReports().subscribe(data => {
          this.reportsResult(data);
      });
  }
  reportsResult(data) {
      this.reportResults = [];
      this.reportResults = JSON.parse(data._body);
      if (this.reportResults.length > 0) {
          this.showResports = true;
      } else {
          this.showResports = false;
      }
  }
  executeReports(report) {
    this.spinerService.show();
      this.selectedReport = report;
      this.reportName = report.name;
      if (report.formDef !== undefined && report.formDef !== null) {
          this.ds.getJSONFromDocument(report.formDef).subscribe(data => this.getFilterFormJSON(data));
      } else {
          this.rs.executeHTMLReport(report).subscribe(data => {
              this.executedHTMLResports(data);
          }, error => {this.spinerService.hide();;this.tr.error('','Couldn\'t get reports');});

      }
  }

  formFilterSubmit(event) {
    this.spinerService.show();
      for (const inputField of [].slice.call(event.target)) {
          for (let index = 0; index < this.formFilter.properties.length; index++) {
              if (inputField.id === this.formFilter.properties[index].name) {
                  this.formFilter.properties[index].value = [{
                      'value': inputField.value
                  }];
              }
              delete this.formFilter.properties[index].dbValue;
              delete this.formFilter.properties[index].dbDummyValue;
              delete this.formFilter.properties[index].lookupOptions;
          }
      }
      for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
          for (let index = 0; index < this.formFilter.properties.length; index++) {
              if (inputField.id === this.formFilter.properties[index].name) {
                  this.formFilter.properties[index].value = [{
                      'value': inputField.children['0'].children['0'].value
                  }];
              }
          }
      }
      for (const inputField of [].slice.call(event.target.getElementsByTagName('p-editor'))) {
          for (let index = 0; index < this.formFilter.properties.length; index++) {
              if (inputField.id === this.formFilter.properties[index].name) {
                  this.formFilter.properties[index].value = [{
                      'value': inputField.children[0].children[1].children[0].innerHTML
                  }];
              }
          }
      }
      // for (const inputField of [].slice.call(event.target.getElementsByTagName('p-checkbox'))) {
      //   for (let index = 0; index < this.formFilter.properties.length; index++) {
      //     if (inputField.id === this.formFilter.properties[index].name) {
      //       this.formFilter.properties[index].value = [{
      //         'value': inputField.children[0].children[1].children[0].innerHTML
      //                  }];
      //                }
      //   }
      // }
      this.selectedReport = {
          'name': this.selectedReport.name,
          'id': this.selectedReport.id,
          'defid': this.selectedReport.defid,
          'filterForm': {
              'name': this.formFilter.name,
              'properties': this.formFilter.properties,
              'datatable': this.formFilter.datatable
          }
      };
      this.ngxSmartModalService.getModal('reportFilterModel').close();
      this.rs.executeHTMLReport(this.selectedReport).subscribe(data => {
          this.executedHTMLResports(data);
      }, error => {this.spinerService.hide();;this.tr.error('','Couldn\'t get reports');});
  }

  executedHTMLResports(data){
    let blob = data._body;
    this.htmlData = this._sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(blob));
    this.reportHTML = true;
    this.changeDetectorRef.detectChanges();
    this.activeIndexNumber = 1;
    this.ngxSmartModalService.getModal('reportFilterModel').close();
    this.formFilterproperties = [];
    this.spinerService.hide();
  }

  dbLookupDropdown(res, k) {
      this.formFilter.properties[k].value = [];
      this.formFilter.properties[k].value.push({
          name: res.label,
          value: res.value
      });
  }

  executedResports(data) {
      this.pdfUrl = data._body;
        const file3 = new Blob([data._body], {type: 'application/pdf'});
        this.pdfUrl = this
            ._sanitizer
            .bypassSecurityTrustResourceUrl(window.URL.createObjectURL(file3));
      this.reportPdf = true;
      this.changeDetectorRef.detectChanges();
      this.activeIndexNumber = 1;
      this.ngxSmartModalService.getModal('reportFilterModel').close();
      this.formFilterproperties = [];
      this.spinerService.hide();
      // window.open(this.pdfUrl);
}

  assignDblookupvalue(res, k) {
      const value = JSON.parse(res._body);
      for (let val = 0; val < value.length; val++) {
          const elementval = {
              label: value[val].label,
              value: value[val].value
          };
          this.formFilter.properties[k].lookupOptions.push(elementval);
      }
      if (this.formFilter.properties[k].value.length > 0) {
          this.tempelateRender.controls[this.formFilter.properties[k].name].patchValue(this.formFilter.properties[k].value[0].value);
      }
  }

  onTabChange(event) {
    this.reportPdf = false;
    this.reportHTML = false;
    this.changeDetectorRef.detectChanges();
    this.activeIndexNumber = 0;
  }

  getFilterFormJSON(data) {
    this.formFilter = JSON.parse(data._body);
    if(this.formFilter.properties.length > 0){
        this.formFilterproperties = this.formFilter.properties;
        this.tempelateRender = this.fb.group({});
        for (let index = 0; index < this.formFilter.properties.length; index++) {
            if (this.formFilter.properties[index].req === 'TRUE') {
                const control: FormControl = new FormControl(null, Validators.required);
                this.tempelateRender.addControl(this.formFilter.properties[index].name, control);
            } else {
                const control: FormControl = new FormControl(null);
                this.tempelateRender.addControl(this.formFilter.properties[index].name, control);
            }
        }
        for (let index = 0; index < this.formFilter.properties.length; index++) {
            if (this.formFilter.properties[index].type === 'TEXT') {
                if (this.formFilter.properties[index].value.length !== 0) {
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
                } else {
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value);
                }
            } else if (this.formFilter.properties[index].type === 'LOOKUP') {
                if (this.formFilter.properties[index].value.length !== 0) {
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
                } else {
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].lookups[0].value);
                }
            } else if (this.formFilter.properties[index].type === 'DATE') {
                if (this.formFilter.properties[index].value.length !== 0) {
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
                } else {
                    const today = new Date(); // needs to be resolved helplinks https://github.com/kekeh/mydatepicker/blob/master/sampleapp/sample-date-picker-access-modifier/sample-date-picker-access-modifier.html
                                            let dd = today.getDate();
                                            let mm = today.getMonth() + 1; // January is 0!
        
                                            const yyyy = today.getFullYear();
                                            if (dd < 10) {
                                                dd = 0 + dd;
                                            }
                                            if (mm < 10) {
                                                mm = 0 + mm;
                                            }
                                            let todays;
                                            if(this.ddmmyy){
                                               todays = dd + '/' + mm + '/' + yyyy;
                                          }else{
                                             todays = mm + '/' + dd + '/' + yyyy;
                                          }
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(todays);
                }
            } else if (this.formFilter.properties[index].type === 'NUMBER') {
                if (this.formFilter.properties[index].value.length !== 0) {
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
                } else {
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value);
                }
            } else if (this.formFilter.properties[index].type === 'TEXTAREA') {
                if (this.formFilter.properties[index].value.length !== 0) {
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
                } else {
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value);
                }
            } else if (this.formFilter.properties[index].type === 'CHECKBOX') {
                if (this.formFilter.properties[index].value.length !== 0) {
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value[0].value);
                } else {
                    this.tempelateRender.controls[this.formFilter.properties[index].name].patchValue(this.formFilter.properties[index].value);
                }
            } else {

            }
        }
        for (let k = 0; k < this.formFilter.properties.length; k++) {
            if (this.formFilter.properties[k].type === 'STEXTTA' || this.formFilter.properties[k].type === 'MTEXTTA' || this.formFilter.properties[k].type === 'DBLOOKUP') {
                this.formFilter.properties[k].dbValue = [];
                this.formFilter.properties[k].dbDummyValue = [];
            }
            if (this.formFilter.properties[k].type === 'DBLOOKUP') {
                this.formFilter.properties[k].lookupOptions = [];
                this.integrationservice.getDBLookup(this.formFilter.properties[k].dblookup).subscribe(res => this.assignDblookupvalue(res, k));
            }
            if (this.formFilter.properties[k].type === 'LOOKUP') {
                this.formFilter.properties[k].lookupOptions = [];
                if (this.formFilter.properties[k].value.length === 0) {
                    this.formFilter.properties[k].lookupOptions = [];
                    const select = {
                        name: this.formFilter.properties[k].lookups[0].name,
                        value: this.formFilter.properties[k].lookups[0].value
                    };
                    this.formFilter.properties[k].value.push(select);
                }
            }
    
            if (this.formFilter.properties[k].type === 'MTEXTTA') {
                this.formFilter.properties[k].dbDummyValue = [];
                this.formFilter.properties[k].dbValue = [];
                if (this.formFilter.properties[k].value.length === 0) {
                    this.formFilter.properties[k].value = [];
                } else {
                    for (let l = 0; l < this.formFilter.properties[k].value.length; l++) {
                        const element = {
                            'value': this.formFilter.properties[k].value[l].value,
                            'name': this.formFilter.properties[k].value[l].name
                        };
                        this.formFilter.properties[k].dbDummyValue.push(element);
                    }
                }
    
            } else if (this.formFilter.properties[k].type === 'STEXTTA') {
                this.formFilter.properties[k].dbDummyValue = [];
                this.formFilter.properties[k].dbValue = [];
                if (this.formFilter.properties[k].value.length === 0) {
                    this.formFilter.properties[k].value = [];
                } else {
                    for (let l = 0; l < this.formFilter.properties[k].value.length; l++) {
                        const element = {
                            'value': this.formFilter.properties[k].value[l].value,
                            'name': this.formFilter.properties[k].value[l].name
                        };
                        this.formFilter.properties[k].dbDummyValue = element;
                    }
                }
    
            } else if (this.formFilter.properties[k].type === 'DBLOOKUP') {
                this.formFilter.properties[k].dbDummyValue = [];
                this.formFilter.properties[k].dbValue = [];
                if (this.formFilter.properties[k].value === '') {
                    this.formFilter.properties[k].value = [];
                } else {
                    for (let l = 0; l < this.formFilter.properties[k].value.length; l++) {
                        const element = {
                            'id': this.formFilter.properties[k].value[l].value,
                            'itemName': this.formFilter.properties[k].value[l].name
                        };
                        this.formFilter.properties[k].dbDummyValue.push(element);
                    }
                }
            } else if (this.formFilter.properties[k].type === 'LOOKUP') {
                this.formFilter.properties[k].lookupOptions = [];
                for (const options of this.formFilter.properties[k].lookups) {
                    const lookup = {
                        label: options.name,
                        value: options.value
                    };
                    this.formFilter.properties[k].lookupOptions.push(lookup);
                }
            } else if (this.formFilter.properties[k].type === 'CHECKBOX') {
                this.formFilter.properties[k].dbDummyValue = [];
                this.formFilter.properties[k].dbValue = '';
                if (this.formFilter.properties[k].value.length === 0) {
                    this.formFilter.properties[k].dbValue = false;
                } else {
                    if (this.formFilter.properties[k].value[0].value === 'TRUE') {
                        this.formFilter.properties[k].dbValue = true;
                    } else {
                        this.formFilter.properties[k].dbValue = false;
                    }
                    // for (let l = 0; l < this.formFilter.properties[k].value.length; l++) {
                    //     const element = {
                    //         'value': this.formFilter.properties[k].value[l].value,
                    //         'name': this.formFilter.properties[k].value[l].name
                    //     };
                    //     this.formFilter.properties[k].dbDummyValue.push(element);
                    // }
                }
            } else if (this.formFilter.properties[k].type === 'TEXTAREA') {
                this.formFilter.properties[k].dbDummyValue = [];
                this.formFilter.properties[k].dbValue = '';
                if (this.formFilter.properties[k].value.length === 0) {
                    this.formFilter.properties[k].dbValue = '';
                } else {
                    let textstring = '';
                    textstring = this.formFilter.properties[k].value[0].value;
                    this.formFilter.properties[k].dbValue = textstring.replace(/<\/p><p>/gm, '</p><br><p>');
                }
            }
        }
        this.spinerService.hide();
        this.ngxSmartModalService.getModal('reportFilterModel').open();
      }else {
        this.selectedReport = {
            'name': this.selectedReport.name,
            'id': this.selectedReport.id,
            'defid': this.selectedReport.defid,
            'filterForm': {
                'name': this.formFilter.name,
                'properties': this.formFilter.properties,
                'datatable': this.formFilter.datatable
            }
        };
        this.ngxSmartModalService.getModal('reportFilterModel').close();
        this.rs.executeHTMLReport(this.selectedReport).subscribe(data => {
            this.executedHTMLResports(data);
        }, error => {this.spinerService.hide();;this.tr.error('','Couldn\'t get reports');});
    }
    } 

    printDocument(){
        this.spinerService.show();
        this.ngxSmartModalService.getModal('reportFilterModel').close();
        this.rs.executePDFReport(this.selectedReport).subscribe(data => {
            this.executedResports(data);this.ngxSmartModalService.getModal('printDocumentModel').open();this.spinerService.hide();
        }, error => {this.spinerService.hide();;this.tr.error('','Couldn\'t get reports');});
    }
}
