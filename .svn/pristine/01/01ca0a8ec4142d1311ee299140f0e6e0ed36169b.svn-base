import {Injectable} from '@angular/core';
import {Http, Headers, ResponseContentType} from '@angular/http';
import * as global from '../global.variables';
import { HttpInterceptor } from './interceptor.service';
import { UserService } from './user.service';
import { environment } from '../../environments/environment';

@Injectable()
export class ContentService {
  private base_url: string;
private header: Headers;

  constructor(private http: HttpInterceptor, private us: UserService) {
    if(environment.production) {
      this.base_url = global.AppURLSettings.SANDBOX_URL;
    }else {
      this.base_url = global.AppURLSettings.DEV_URL;
    }
    this.header = new Headers();
    this.header.append('token', localStorage.getItem('token'));
    this.header.append('repo', localStorage.getItem('repoName'));
  }

  getRepositories() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/getRepositories?sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
      res => res
    );
  }

  getAppRepository() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/getAppRepository?sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
      res => res
    );
  }

  getTopFolders() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/getTopFolders?sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
      res => res
    );
  }
  getSubfolders(folderId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/getSubfolders?id=${folderId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
      res => res
    );
  }
  getDocumentClasses() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/getDocumentClasses?sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
      res => res
    );
  }



  getFolderDocuments(folderId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/getFolderDocuments?id=${folderId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
      res => res
    );
  }

  createSubfolder(foldername, folderId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/createSubfolder?parentid=${folderId}&name=${foldername}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
      res => res
    );
  }

  getUnfiledDocuments() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/getUnfiledDocuments?sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
      res => res
    );
  }

  getDocumentFolders(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/getDocumentFolders?id=${docId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
      res => res
    );
  }

  getRecent(empNo) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/getRecent?empno=${empNo}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
      res => res
    );
  }

  getFavorites(empNo) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getFavorites?empno=${empNo}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

addToFavorites(empno , id) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/addToFavorites?empno=${empno}&id=${id}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

removeFromFavorites(empno , id) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/removeFromFavorites?empno=${empno}&id=${id}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

moveToFolder(fromId , toid) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/moveToFolder?sourceid=${fromId}&targetid=${toid}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

getFolderActions(folderId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getFolderActions?id=${folderId}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

deleteDocument(documentId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/deleteDocument?id=${documentId}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

deleteFolder(folderId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/deleteFolder?folderid=${folderId}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

fileInFolder(folderId , documentId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/fileInFolder?folderid=${folderId}&id=${documentId}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

unfileFromFolder(folderId , documentId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/unfileFromFolder?folderid=${folderId}&id=${documentId}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

updateProperties(val) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/updateProperties?sysdatetime=${fulldatetime}`;
  return this.http.post(url, val, {headers : this.header}).map(
    res => res
  );
}

addDocument(form) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/addDocument?sysdatetime=${fulldatetime}`;
  return this.http.post(url, form, {headers : this.header}).map(
    res => res
  );
}
downloadMultipleDocuments(docIds) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    this.header.append('responseType' , 'arraybuffer');
    const url = `${this.base_url}ContentService/downloadMultipleDocuments?sysdatetime=${fulldatetime}`;
    return this.http.post(url, docIds , {headers : this.header}).map(
      res => res
    );
}

downloadDocument(docId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/downloadDocument?id=${docId}&sysdatetime=${fulldatetime}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {responseType: ResponseContentType.Blob, headers: this.header}).map(
    res => res
  );
}

downloadThisDocument(docId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/downloadThisDocument?id=${docId}&sysdatetime=${fulldatetime}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {responseType: ResponseContentType.Blob, headers: this.header}).map(
    res => res
  );
}

downloadMultiDocuments(docId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/downloadMultiDocuments?docids=${docId}&sysdatetime=${fulldatetime}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {responseType: ResponseContentType.Blob, headers: this.header}).map(
    res => res
  );
}
checkOut(docId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/checkOut?id=${docId}&sysdatetime=${fulldatetime}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

getThisDocument(docId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getThisDocument?id=${docId}&sysdatetime=${fulldatetime}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

searchDocuments(request: any): any {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/search?sysdatetime=${fulldatetime}`;
  return this.http.post(url, request, {headers : this.header}).map(res => res.json());
}

cancelCheckOut(docId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/cancelCheckOut?id=${docId}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

checkIn(formData) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/checkIn?sysdatetime=${fulldatetime}`;
  return this.http.post(url, formData, {headers : this.header}  ).map(
    res => res
  );
}

viewDocument(docId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/downloadDocument?id=${docId}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {responseType: ResponseContentType.Blob, headers: this.header} ).map(
    res => res
  );
}

getDocumentVersions(id) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getDocumentVersions?id=${id}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

getDocumentPermissions(id) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getDocumentPermissions?id=${id}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

getDocument(id) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getDocument?id=${id}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

getDocumentActions(id) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getDocumentActions?id=${id}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

getDocumentPageCount(docId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getDocumentPageCount?id=${docId}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

getPreviewPage(docId, pagenumber) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getPreviewPage?id=${docId}&page=${pagenumber}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

downloadPreviewPage(docId, pagenumber) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/downloadPreviewPage?id=${docId}&page=${pagenumber}&empno=${this.us.getCurrentUser().EmpNo}&sysdatetime=${fulldatetime}`;
  return url;
}

downloadAnnotation(docId , pagenumber) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/downloadAnnotation?id=${docId}&page=${pagenumber}&empno=${this.us.getCurrentUser().EmpNo}&sysdatetime=${fulldatetime}`;
  return url;
}


saveAnnotation(annotation) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/saveAnnotation?sysdatetime=${fulldatetime}`;
  return this.http.post(url, annotation, {headers : this.header}).map(
    res => res
  );
}

getAnnotations(documentId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getAnnotations?id=${documentId}&status=ALL&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

getDocumentHistory(documentId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getDocumentHistory?id=${documentId}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

getDocumentLinks(documentId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/getDocumentLinks?docid=${documentId}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers : this.header}).map(
    res => res
  );
}

downloadAnnotatedDocument(documentId) {
  const sysDateTime = new Date();
  const fulldatetime = sysDateTime.getTime();
  const url = `${this.base_url}ContentService/downloadAnnotatedDocument?id=${documentId}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {responseType: ResponseContentType.Blob, headers: this.header}).map(
    res => res
  );
  }

  downloadSignedDocument(documentId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/downloadSignedDocument?id=${documentId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {responseType: ResponseContentType.Blob, headers: this.header}).map(
      res => res
    );
    }

  completeAnnotation(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}ContentService/completeAnnotation?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers : this.header}).map(
      res => res
    );
    }

    deleteAnnotation(id) {
      const sysDateTime = new Date();
      const fulldatetime = sysDateTime.getTime();
      const url = `${this.base_url}ContentService/deleteAnnotation?id=${id}&sysdatetime=${fulldatetime}`;
      return this.http.get(url, {headers : this.header}).map(
        res => res
      );
      }

      getJSONFromDocument(docId) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/getJSONFromDocument?docId=${docId}&sysdatetime=${fulldatetime}`;
        return this.http.get(url , {headers : this.header}).map(
          res => res
        );
      }

      addJSONDocument(formDetails) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/addJSONDocument?sysdatetime=${fulldatetime}`;
        return this.http.post(url, formDetails , {headers : this.header}).map(res => res);
      }

      getFolder(folderId) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/getFolder?id=${folderId}&sysdatetime=${fulldatetime}`;
        return this.http.get(url , {headers : this.header}).map(
          res => res
        );
      }

      updateFolderSecurity(folderObj) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/updateFolderSecurity?sysdatetime=${fulldatetime}`;
        return this.http.post(url , folderObj, {headers : this.header}).map(
          res => res
        );
      }

      setFolderPriority(folderId) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/setFolderPriority?folderId=${folderId}&sysdatetime=${fulldatetime}`;
        return this.http.get(url , {headers : this.header}).map(
          res => res
        );
      }

      updateDocumentSecurity(documentObj) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/updateDocumentSecurity?sysdatetime=${fulldatetime}`;
        return this.http.post(url , documentObj, {headers : this.header}).map(
          res => res
        );
      }

      getDocumentPagePermission(docId) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/setFolderPriority?id=${docId}&sysdatetime=${fulldatetime}`;
        return this.http.get(url , {headers : this.header}).map(
          res => res
        );
      }

      downloadSharedDocument(docId) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const token = localStorage.getItem('token');
        const url = `${this.base_url}ContentService/downloadSharedDocument?id=${docId}&token=${token}&sysdatetime=${fulldatetime}`;
        return  url;
      }

      renameFolder(folderId, folderName) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/renameFolder?folderid=${folderId}&name=${folderName}&sysdatetime=${fulldatetime}`;
        return this.http.get(url , {headers : this.header}).map(
          res => res
        );
      }

      getSearchPreviewPage(docId, pageNum, searchText) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/getSearchPreviewPage?id=${docId}&page=${pageNum}&text=${searchText}&sysdatetime=${fulldatetime}`;
        return this.http.get(url , {headers : this.header}).map(
          res => res
        );
      }

      downloadHLDocument(docId, pageNum, searchText) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/downloadHLDocument?id=${docId}&text=${searchText}&sysdatetime=${fulldatetime}`;
        return this.http.get(url , {headers : this.header}).map(
          res => res
        );
      }

      linkDocuments(firstdoc, seconddoc) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/linkDocuments?firstdoc=${firstdoc}&seconddoc=${seconddoc}&sysdatetime=${fulldatetime}`;
        return this.http.get(url , {headers : this.header}).map(
          res => res
        );
      }

      unlinkDocument(firstdoc, seconddoc) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/unlinkDocuments?firstdoc=${firstdoc}&seconddoc=${seconddoc}&sysdatetime=${fulldatetime}`;
        return this.http.get(url , {headers : this.header}).map(
          res => res
        );
      }

      indexDocument(firstdoc) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/indexDocument?id=${firstdoc}&sysdatetime=${fulldatetime}`;
        return this.http.get(url , {headers : this.header}).map(
          res => res
        );
      }
      downloadPrintDocument(docId) {
        const sysDateTime = new Date();
        const fulldatetime = sysDateTime.getTime();
        const url = `${this.base_url}ContentService/downloadPrintDocument?id=${docId}&sysdatetime=${fulldatetime}`;
        return this.http.get(url , {responseType: ResponseContentType.Blob, headers : this.header}).map(res => res);
      }
      // deleteAnnotation(annotationId) {
      //   const sysDateTime = new Date();
      //   const fulldatetime = sysDateTime.getTime();
      //   const url = `${this.base_url}ContentService/linkDocuments?firstdoc=${firstdoc}&seconddoc=${seconddoc}&sysdatetime=${fulldatetime}`;
      //   return this.http.get(url , {headers : this.header}).map(
      //     res => res
      //   );
      // }
}
