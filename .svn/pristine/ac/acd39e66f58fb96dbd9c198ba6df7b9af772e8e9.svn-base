import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import * as global from '../global.variables';
import {UserService} from './user.service';
import { HttpInterceptor } from './interceptor.service';
import { environment } from '../../environments/environment';

@Injectable()
export class WorkService {

  private current_user: any;
  private base_url: String;
  private header: Headers;
  constructor(private http: HttpInterceptor, private us: UserService) {
    if (environment.production) {
      this.base_url = global.AppURLSettings.SANDBOX_URL;
    }else {
      this.base_url = global.AppURLSettings.DEV_URL;
    }
    this.header = new Headers();
    this.header.append('token', sessionStorage.getItem('token'));
  }

  saveWork(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/saveWork?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value , {headers: this.header}).map(res => res);
  }
  getWorkStatistics(empno, datetype, type) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getWorkStatistics?empNo=${empno}&dateType=${datetype}&type=${type}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getActivityStatistics(empno, datetype, type) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getActivityStatistics?empNo=${empno}&dateType=${datetype}&type=${type}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getOrgWorkStatistics(orgId, datetype, type) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getOrgWorkStatistics?orgId=${orgId}&dateType=${datetype}&type=${type}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getOrgActivityStatistics(orgId, datetype, type) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getOrgActivityStatistics?orgId=${orgId}&dateType=${datetype}&type=${type}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getOrgWorkItems(orgId, datetype, status) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getOrgWorkItems?orgId=${orgId}&dateType=${datetype}&status=${status}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );

  }
  getOrgWorkItemsType(orgId, dateType, typeId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getOrgWorkItems?orgId=${orgId}&dateType=${dateType}&typeId=${typeId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
    getUserWorkItemsType(dateType, typeId) {
      const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
      const url = `${this.base_url}WorkService/getUserWorkItems?dateType=${dateType}&typeId=${typeId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }

  getUserWorkItemsStatus(dateType, status) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getUserWorkItems?dateType=${dateType}&status=${status}&sysdatetime=${fulldatetime}`;
  return this.http.get(url, {headers: this.header}).map(
    res => res
  );
}

  getOrgAcitityItemsType(orgId, dateType, typeId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getOrgActivityItems?orgId=${orgId}&dateType=${dateType}&typeId=${typeId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getOrgActivityItems(orgId, datetype, status) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getOrgActivityItems?orgId=${orgId}&dateType=${datetype}&status=${status}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );

  }


  getAcitvityHistory(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getWorkHistory?activityId=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }

  getSentItems(empId, page) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getSentItems?empNo=${empId}&pageNo=${page}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }

  getInboxItems(empId, page) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getInboxItems?empNo=${empId}&pageNo=${page}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getDraftItems(empId, page) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getDraftItems?empNo=${empId}&pageNo=${page}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getArchiveItems(empId, page) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getArchiveItems?empNo=${empId}&pageNo=${page}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }

  getActivityInfo(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getActivityInfo?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }

  saveActivity(formdetails) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/saveActivity?sysdatetime=${fulldatetime}`;
    return this.http.post(url, formdetails, {headers: this.header}).map(res => res);
  }

  finishActivity(activity) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/finishActivity?sysdatetime=${fulldatetime}`;
    return this.http.post(url, activity, {headers: this.header}).map(res => res);
  }

  markActivityAsRead(empNo , id , roleId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/markActivityAsRead?id=${id}&empNo=${empNo}&roleId=${roleId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }

  getWorkHistory(workId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getWorkHistory?workId=${workId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }

  searchInbox() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/searchInbox?sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }

  searchSentItems() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/searchSentItems?sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  recallActivity(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/recallActivity?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getActivityResponses(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getActivityResponses?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  finishMultipleActivities(formdetails) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/finishMultipleActivities?sysdatetime=${fulldatetime}`;
    return this.http.post(url, formdetails, {headers: this.header}).map(res => res);
  }
  getWorkRegister(type, category) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getWorkRegister?type=${type}&category=${category}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getWorkAttachments(workId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getWorkAttachments?workId=${workId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getWorkDraftActivity(workid) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getWorkDraftActivity?workId=${workid}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getWorkActivity(workId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getWorkActivity?workId=${workId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  searchWork(request: any): any {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/searchWork?sysdatetime=${fulldatetime}`;
    return this.http.post(url, request, {headers: this.header}).map(res => res.json());
  }

  updatePrimaryDocument(workId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/updatePrimaryDocument?id=${workId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }

  updateSubject(activityId, subject) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/updateSubject?actid=${activityId}&subject=${subject}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }

  initiateTaskFlow(workId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/initiateTaskFlow?docid=${workId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
  getActivityItems(datetype, status) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getUserActivityItems?dateType=${datetype}&status=${status}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res);
  }
  getActivityItemsType(datetype, typeId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getUserActivityItems?dateType=${datetype}&typeId=${typeId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res);
  }

  flagActivity(workid, flag, roleId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/flagActivity?id=${workid}&flag=${flag}&roleId=${roleId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res);
  }

  setAsPrimaryDocument(workid, docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/setAsPrimaryDocument?workid=${workid}&docid=${docId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res);
  }

  getActivityForAction(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}WorkService/getActivityForAction?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, {headers: this.header}).map(
      res => res
    );
  }
}
