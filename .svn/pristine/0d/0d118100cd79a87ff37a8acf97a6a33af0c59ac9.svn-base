<form [formGroup]="dynamicform" (ngSubmit)="formDynamicSubmit($event)" *ngIf="dynamicFormJSON.length > 0">
    <div class="ui-g" *ngFor="let item of formObject;let ind = index">
        <div class="ui-g-12 ui-lg-12 ui-sm-12 ui-g-nopad" *ngFor="let section of item.sections;let i = index">
            <div *ngIf="section.visible === 'TRUE' || section.visible === undefined">
                <div class="ui-panel-titlebar ui-widget-header ui-helper-clearfix ui-corner-all ng-tns-c6-5" *ngIf="section.showHeader === 'TRUE'">
                    <div class=" ui-g-12 ui-lg-12 ui-sm-12 ui-g-nopad">
                        <div class=" ui-g-12 ui-lg-5 ui-sm-2 "></div>
                        <div class=" ui-g-12 ui-lg-7 ui-sm-10 ">
                            {{section.heading | translate}}
                        </div>
                    </div>
                </div>
                <div class="ui-g ui-lg-12 ui-sm-12 ui-g-nopad" *ngIf="converToUppercase(section.type) === 'FORM'">
                    <div [class]="section.columns.length === 2 ?'ui-g-6 ui-lg-6 ui-sm-6' : section.columns.length === 3 ? 'ui-g-4 ui-lg-4 ui-sm-4' : 'ui-g-12 ui-lg-12 ui-sm-12'" *ngFor="let columns of section.columns;let j = index">
                        <div class="ui-g-12 ui-lg-12 ui-sm-12" *ngFor="let prop of columns.properties;let k = index">
                            <div *ngIf="prop.visible === 'TRUE' || prop.visible === undefined ">
                                <div [class]="section.columns.length > 1? '' : 'ui-lg-2  ui-g-12'"></div>
                                <div [class]="section.columns.length === 1?'ui-lg-2 ui-sm-2 ui-g-2':'ui-lg-4 ui-sm-4 ui-g-4'" *ngIf="prop.type !== 'CHECKBOX'">
                                    <label>{{prop.label | translate}} <sup class="redstar" *ngIf="prop.req === 'TRUE' ? true : false">*</sup></label>
                                </div>
                                <div [class]="section.columns.length === 1 ? 'ui-lg-2 ui-sm-2 ui-g-2':'ui-lg-4 ui-sm-4 ui-g-4'" *ngIf="prop.type === 'CHECKBOX'">
                                    <p-checkbox [name]="prop.name" [id]="prop.name" *ngIf="prop.value.length > 0" [disabled]="dynamicform.controls[prop.name].disabled === true ? true : false" [(ngModel)]="prop.value[0].value" [ngModelOptions]="{standalone: true}" binary="true" [style]="{'float': 'right'}"></p-checkbox>
                                    <p-checkbox [name]="prop.name" [id]="prop.name" *ngIf="prop.value.length === 0" [formControlName]="prop.name" [style]="{'float': 'right'}"></p-checkbox>

                                </div>
                                <div [class]="section.columns.length === 1?'ui-lg-6 ui-sm-6 ui-md-6 ui-g-6':'ui-lg-8 ui-sm-8 ui-md-4 ui-g-4 '">
                                    <input type="text" (mouseover)="focusElement($event)" *ngIf="prop.type === 'TEXT' " [formControlName]="prop.name" [id]="prop.name" [name]="prop.name" placeholder="{{'Enter' | translate}} {{prop.label | translate}} " class="md-inputfield" [attr.maxlength]="prop.length"
                                        pInputText [ngClass]="{'ng-dirty ng-invalid': dynamicform.controls[prop.name].hasError('required') && dynamicform.controls[prop.name].touched && dynamicform.controls[prop.name].invalid}">
                                    <input type="text" (mouseover)="focusElement($event)" (focusout)="focusOutFunction($event , prop)" *ngIf="prop.type === 'TEXTAF' " [formControlName]="prop.name" [id]="prop.name" [name]="prop.name" placeholder="{{'Enter' | translate}} {{prop.label | translate}} "
                                        class="md-inputfield" [attr.maxlength]="prop.length" pInputText [ngClass]="{'ng-dirty ng-invalid': dynamicform.controls[prop.name].hasError('required') && dynamicform.controls[prop.name].touched && dynamicform.controls[prop.name].invalid}">
                                    <input type="number" (mouseover)="focusElement($event)" [name]="prop.name" [id]="prop.name" [formControlName]="prop.name" *ngIf="prop.type === 'NUMBER' " placeholder="{{'Enter' | translate}} {{prop.label | translate}}" class="form-control" [attr.maxlength]="prop.length"
                                        pInputText [ngClass]="{'ng-dirty ng-invalid': dynamicform.controls[prop.name].hasError('required') && dynamicform.controls[prop.name].touched && dynamicform.controls[prop.name].invalid}">
                                    <div *ngIf="prop.type === 'TEXTAREA'">
                                        <textarea pInputTextarea type="text" (mouseover)="focusElement($event)" [id]="prop.name" [formControlName]="prop.name" placeholder="{{'Enter' | translate}} {{prop.label | translate}}" rows="8" [ngClass]="{'ng-dirty ng-invalid': dynamicform.controls[prop.name].hasError('required') && dynamicform.controls[prop.name].touched && dynamicform.controls[prop.name].invalid}">
                                    </textarea>
                                        <!-- <quill-editor [id]="prop.name" [formControlName]="prop.name"></quill-editor> -->
                                        <!-- <p-editor theme="'Bubble'" [style]="{'height':'250px'}" [id]="prop.name" [formControlName]="prop.name"></p-editor> -->
                                    </div>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="prop.type === 'TIME'">
                                        <div class="ui-grid-row">
                                            <div class="ui-grid-col-12">
                                                <p-calendar [id]="prop.name" (mouseover)="focusElement($event)" showTime="true" [timeOnly]="true" [formControlName]="prop.name" placeholder="{{'Select' | translate}} {{prop.label | translate}}" [ngClass]="{'ng-dirty ng-invalid': dynamicform.controls[prop.name].hasError('required') && dynamicform.controls[prop.name].touched && dynamicform.controls[prop.name].invalid}"></p-calendar>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="prop.type === 'DATERANGE'">
                                        <div class="ui-grid-row">
                                            <div class="ui-grid-col-12">
                                                <p-calendar dateRangeSelector [showIcon]="true" [name]="prop.name" placeholder="Date" *ngIf="ddmmyy" dateFormat="dd/mm/yy" [id]="prop.name" [formControlName]="prop.name" selectionMode="range" [ngClass]="{'ng-dirty ng-invalid': dynamicform.controls[prop.name].hasError('required') && dynamicform.controls[prop.name].touched && dynamicform.controls[prop.name].invalid}"></p-calendar>
                                                <p-calendar dateRangeSelector [showIcon]="true" [name]="prop.name" placeholder="Date" *ngIf="!ddmmyy" dateFormat="mm/dd/yy" [id]="prop.name" [formControlName]="prop.name" selectionMode="range" [ngClass]="{'ng-dirty ng-invalid': dynamicform.controls[prop.name].hasError('required') && dynamicform.controls[prop.name].touched && dynamicform.controls[prop.name].invalid}"></p-calendar>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <my-date-picker class="col-12" [id]="prop.name" *ngIf="prop.type === 'DATE' " [options]="myDatePickerOptions" [formControlName]="prop.name" [attr.required]="prop.rOnly === 'TRUE' ? true : false"></my-date-picker> -->
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="prop.type === 'DATE' ">
                                        <div class="ui-grid-row">
                                            <div class="ui-grid-col-12">
                                                <p-calendar [showIcon]="true" [name]="prop.name" placeholder="Date" *ngIf="ddmmyy" dateFormat="dd/mm/yy" [id]="prop.name" [formControlName]="prop.name" [ngClass]="{'ng-dirty ng-invalid': dynamicform.controls[prop.name].hasError('required') && dynamicform.controls[prop.name].touched && dynamicform.controls[prop.name].invalid}"></p-calendar>
                                                <p-calendar [showIcon]="true" [name]="prop.name" placeholder="Date" *ngIf="!ddmmyy" dateFormat="mm/dd/yy" [id]="prop.name" [formControlName]="prop.name" [ngClass]="{'ng-dirty ng-invalid': dynamicform.controls[prop.name].hasError('required') && dynamicform.controls[prop.name].touched && dynamicform.controls[prop.name].invalid}"></p-calendar>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="prop.type ===  'LOOKUP'">
                                        <div class="ui-grid-row">
                                            <div class="ui-grid-col-12">
                                                <p-dropdown [options]="prop.lookupOptions" [formControlName]="prop.name" [autoWidth]="false" [id]="prop.name" (onChange)="dbLookupDropdown($event,ind, i , j , k )"></p-dropdown>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="prop.type ===  'LOOKUPCONDITION'">
                                        <div class="ui-grid-row">
                                            <div class="ui-grid-col-12">
                                                <p-dropdown [options]="prop.lookupOptions" [formControlName]="prop.name" [autoWidth]="false" [id]="prop.name" (onChange)="dbLookupDropdownCondition($event,ind, i , j , k , prop)"></p-dropdown>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="prop.type === 'DBLOOKUP'">
                                        <div class=" ui-grid-row ">
                                            <div class="ui-grid-col-12 ">
                                                <p-dropdown [options]="prop.lookupOptions" [formControlName]="prop.name" [autoWidth]="false" [id]="prop.name" filter="true" placeholder="{{'Select' | translate}}" (onChange)="dbLookupDropdown($event,ind, i , j , k )"></p-dropdown>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="prop.type ===  'STEXTTA'">
                                        <div class=" ui-grid-row ">
                                            <div class="ui-grid-col-12 ">
                                                <p-autoComplete [formControlName]="prop.name" [(ngModel)]="prop.dbDummyValue" field="name" [suggestions]="prop.dbValue" searchtext (completeMethod)="onSearchAutoStextTA($event , prop.dblookup , i , j , k)" [multiple]="false" [id]="prop.name" [dropdown]="true"
                                                    [minLength]="3" placeholder="{{'Single Search' | translate}}" (onSelect)="selectSingle($event, ind, i , j, k)" [ngClass]="{'ng-dirty ng-invalid': dynamicform.controls[prop.name].hasError('required') && dynamicform.controls[prop.name].touched && dynamicform.controls[prop.name].invalid}"></p-autoComplete>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="prop.type ===  'MTEXTTA'">
                                        <div class=" ui-grid-row ">
                                            <div class="ui-grid-col-12 ">
                                                <p-autoComplete [formControlName]="prop.name" [(ngModel)]="prop.dbDummyValue" field="name" [suggestions]="prop.dbValue" (completeMethod)="onSearchAuto($event , prop.dblookup , i , j , k)" [multiple]="true" [id]="prop.name" [dropdown]="true" [minLength]="3"
                                                    placeholder="{{'Multiple Search' | translate }}" (onSelect)="selctedMultiple($event, ind, i , j , k);" (onUnselect)="unselectMultiple($event, ind, i , j , k)" immediate="true" [ngClass]="{'ng-dirty ng-invalid': dynamicform.controls[prop.name].hasError('required') && dynamicform.controls[prop.name].touched && dynamicform.controls[prop.name].invalid}"></p-autoComplete>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="prop.type === 'CHECKBOX'">
                                        <div class=" ui-grid-row ">
                                            <div class="ui-grid-col-12 ">
                                                <label> <sup class="redstar" *ngIf="prop.req === 'TRUE' ? true : false">*</sup>{{prop.label | translate}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ui-g global-sales" *ngIf="converToUppercase(section.type) === 'TABLE'">
                    <table class="ui-g-12 ui-md-12">
                        <thead>
                            <tr style="border-top: 0px solid #dbdbdb">
                                <th [attr.width]="row.width" *ngFor="let row of section.rowheader">
                                    <label>{{row.label | translate}} <sup class="redstar" *ngIf="row.req === 'TRUE' ? true : false">*</sup></label>
                                </th>
                                <th *ngIf="section.rOnly !== 'TRUE' && readOnly !== 'true'">
                                    Edit
                                </th>
                            </tr>
                        </thead>
                        <tbody *ngFor="let row of section.rows ;let isLast = last;let pos = index; let isFirst = first">
                            <tr *ngIf="row.row !== (9090 + i)" style="border-top: 0px solid #dbdbdb">
                                <td *ngFor="let rowItem of row.items;let ind = index">
                                    <input type="number" *ngIf="rowItem.type === 'AUTONUMBER' " (input)="eventCalculated($event, row, rowItem, pos, ind, i, 'false' )" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" placeholder="{{'Enter' | translate}} {{rowItem.label | translate}} "
                                        class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                    <input type="text" *ngIf="rowItem.type === 'TEXT'" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" placeholder="{{'Enter' | translate}} {{rowItem.label | translate}} "
                                        class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                    <input type="number" *ngIf="rowItem.type === 'NUMBER' " (mouseover)="focusElement($event)" (input)="eventCalculated($event, row, rowItem, pos, ind, i, 'false' )" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row"
                                        placeholder="{{'Enter' | translate}} {{rowItem.label | translate}}  " class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                    <input type="number" *ngIf="rowItem.type === 'ROWSUM' " [attr.value]="row.row" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" placeholder="{{'Enter' | translate}} {{rowItem.label | translate}} "
                                        class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                    <input type="number" *ngIf="rowItem.type === 'ROWAVG' " [attr.value]="row.row" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" placeholder="{{'Enter' | translate}} {{rowItem.label | translate}} "
                                        class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                    <input type="number" *ngIf="rowItem.type === 'MULTIPLICATION' " [attr.value]="row.row" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" placeholder="{{'Enter' | translate}} {{rowItem.label | translate}} "
                                        class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="rowItem.type === 'DATE' ">
                                        <div class="ui-grid-row">
                                            <div class="ui-grid-col-12">
                                                <p-calendar [showIcon]="true" placeholder="Select date" [formControlName]="rowItem.name + row.row" *ngIf="ddmmyy" dateFormat="dd/mm/yy" [name]="rowItem.name+ row.row" [id]="rowItem.name+ row.row"></p-calendar>
                                                <p-calendar [showIcon]="true" placeholder="Select date" [formControlName]="rowItem.name + row.row" *ngIf="!ddmmyy" dateFormat="mm/dd/yy" [name]="rowItem.name+ row.row" [id]="rowItem.name+ row.row"></p-calendar>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="rowItem.type ===  'LOOKUP'">
                                        <div class="ui-grid-row">
                                            <div class="ui-grid-col-12">
                                                <p-dropdown [options]="rowItem.lookupOptions" [formControlName]="rowItem.name + row.row" [autoWidth]="false" [id]="rowItem.name+ row.row" [name]="rowItem.name+ row.row" (onChange)="dbLookupDropdownTable($event,pos,ind)"></p-dropdown>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td *ngIf="section.rOnly !== 'TRUE' && readOnly !== 'true'">
                                    <!-- <button pButton type="button" *ngIf="!isLast" icon="fa-trash"></button> -->
                                    <!-- <div class="ui-g ui-g-12 ui-g-nopad" *ngIf="sectionsArray.length > 0">
                                        <div class="ui-g ui-g-12 ui-g-nopad" *ngFor="let sections of sectionsArray">
                                            <button pButton type="button" *ngIf="!isFirst && (sections === i && !isLast)" (click)="removeExistingTableRow(i, row, pos)" icon="ui-icon-remove"></button>
                                        </div>
                                    </div> -->
                                    <!-- <div class="ui-g u-g-12 ui-g-nopad" *ngIf="sectionsArray.length === 0"> -->
                                    <button pButton type="button" *ngIf="!isFirst" (click)="removeExistingTableRow(i, row, pos)" icon="ui-icon-remove"></button>
                                    <!-- </div> -->
                                    <!-- <div class="ui-g ui-g-12 ui-g-nopad"> -->
                                    <button pButton type="button" *ngIf="isFirst" (click)="addNewTableRow(i, 'row')" icon="fa-plus"></button>
                                    <!-- </div> -->
                                </td>
                            </tr>
                            <tr *ngIf="row.row === (9090 + i)" style="border-top: 0px solid #dbdbdb">
                                <td *ngFor="let rowItem of row.items;let ind = index">
                                    <input type="text" *ngIf="ind === 0" [formControlName]="rowItem.name + row.row" value="Total" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                    <input type="number" *ngIf="rowItem.type === 'AUTONUMBER' && ind !== 0 " [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                    <input type="text" *ngIf="rowItem.type === 'TEXT' && ind !== 0" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" class="md-inputfield" [attr.maxlength]="rowItem.length"
                                        pInputText>
                                    <input type="number" *ngIf="rowItem.type === 'NUMBER' && ind !== 0" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" class="md-inputfield" [attr.maxlength]="rowItem.length"
                                        pInputText>
                                    <input type="number" *ngIf="rowItem.type === 'ROWSUM' && ind !== 0 " [attr.value]="row.row" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" class="md-inputfield"
                                        [attr.maxlength]="rowItem.length" pInputText>
                                    <input type="number" *ngIf="rowItem.type === 'ROWAVG' && ind !== 0" [attr.value]="row.row" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" class="md-inputfield"
                                        [attr.maxlength]="rowItem.length" pInputText>
                                    <input type="number" *ngIf="rowItem.type === 'MULTIPLICATION' && ind !== 0" [attr.value]="row.row" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" class="md-inputfield"
                                        [attr.maxlength]="rowItem.length" pInputText>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="rowItem.type === 'DATE' && ind !== 0">
                                        <div class="ui-grid-row">
                                            <div class="ui-grid-col-12">
                                                <p-calendar [showIcon]="true" [formControlName]="rowItem.name + row.row" *ngIf="ddmmyy" dateFormat="dd/mm/yy" [name]="rowItem.name+ row.row" [id]="rowItem.name+ row.row"></p-calendar>
                                                <p-calendar [showIcon]="true" [formControlName]="rowItem.name + row.row" *ngIf="!ddmmyy" dateFormat="mm/dd/yy" [name]="rowItem.name+ row.row" [id]="rowItem.name+ row.row"></p-calendar>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="rowItem.type ===  'LOOKUP' && ind !== 0">
                                        <div class="ui-grid-row">
                                            <div class="ui-grid-col-12">
                                                <p-dropdown [options]="rowItem.lookupOptions" [formControlName]="rowItem.name + row.row" [autoWidth]="false" [id]="rowItem.name+ row.row" [name]="rowItem.name+ row.row" (onChange)="dbLookupDropdownTable($event,pos,ind)"></p-dropdown>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <!-- <tfoot *ngFor="let foot of sectionsArray; let footInd = index">
                            <tr *ngIf="foot === i">
                                <td *ngFor="let row of section.rows ;let posfoot = index; ">
                                    <span *ngFor="let rowItem of row.items;let footInd = index">
                                        <input type="number" *ngIf="rowItem.type === 'AUTONUMBER' " [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" placeholder="{{'Enter' | translate}} {{rowItem.label | translate}} " class="md-inputfield"
                                            [attr.maxlength]="rowItem.length" pInputText>
                                        <input type="text" *ngIf="rowItem.type === 'TEXT'" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" placeholder="{{'Enter' | translate}} {{rowItem.label | translate}} "
                                            class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                        <input type="number" *ngIf="rowItem.type === 'NUMBER' " (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" placeholder="{{'Enter' | translate}} {{rowItem.label | translate}}  "
                                            class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                        <input type="number" *ngIf="rowItem.type === 'MULTIPLICATION' " [attr.value]="row.row" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" placeholder="{{'Enter' | translate}} {{rowItem.label | translate}} "
                                            class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                        <input type="number" *ngIf="rowItem.type === 'ROWAVG' " [attr.value]="row.row" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" placeholder="{{'Enter' | translate}} {{rowItem.label | translate}} "
                                            class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                        <input type="number" *ngIf="rowItem.type === 'MULTIPLICATION' " [attr.value]="row.row" (mouseover)="focusElement($event)" [formControlName]="rowItem.name + row.row" [id]="rowItem.name + row.row" [name]="rowItem.name + row.row" placeholder="{{'Enter' | translate}} {{rowItem.label | translate}} "
                                            class="md-inputfield" [attr.maxlength]="rowItem.length" pInputText>
                                        <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="rowItem.type === 'DATE' ">
                                            <div class="ui-grid-row">
                                                <div class="ui-grid-col-12">
                                                    <p-calendar [showIcon]="true" placeholder="Select date" [formControlName]="rowItem.name + row.row" *ngIf="ddmmyy" dateFormat="dd/mm/yy" [name]="rowItem.name+ row.row" [id]="rowItem.name+ row.row"></p-calendar>
                                                    <p-calendar [showIcon]="true" placeholder="Select date" [formControlName]="rowItem.name + row.row" *ngIf="!ddmmyy" dateFormat="mm/dd/yy" [name]="rowItem.name+ row.row" [id]="rowItem.name+ row.row"></p-calendar>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="ui-grid ui-grid-responsive ui-fluid" *ngIf="rowItem.type ===  'LOOKUP'">
                                            <div class="ui-grid-row">
                                                <div class="ui-grid-col-12">
                                                    <p-dropdown [options]="rowItem.lookupOptions" [formControlName]="rowItem.name + row.row" [autoWidth]="false" [id]="rowItem.name+ row.row" [name]="rowItem.name+ row.row" (onChange)="dbLookupDropdownTable($event,pos,ind)"></p-dropdown>
                                                </div>
                                            </div>
                                        </div>
                                    </span>
                                </td>
                            </tr>
                        </tfoot> -->
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="ui-md ui-sm" *ngIf="readOnly !== 'true'">
        <div class=" ui-g-12 ui-lg-12 ui-sm-12  text-center ui-md ui-sm">
            <div class="ui-g-12 ui-lg ui-sm text-center">
                <div class="ui-lg-3 ui-g-12 ui-sm-4"></div>
                <div class="ui-lg-2  ui-g-12 ui-sm-2">
                    <!-- <button pButton type="button" label="{{ 'Cancel' | translate }}" (click)="cancelForm()" icon="ui-icon-cancel" class="width"></button> -->
                </div>
                <div class="ui-lg-2  ui-g-12 ui-sm-2" *ngIf="submitButton">
                    <button pButton type="submit" label="{{'Next' | translate }}" *ngIf="activitytype === 'CREATE'" [disabled]="!dynamicform.valid" icon="ui-icon-save" class="wrapperCenter"></button>
                    <button pButton type="submit" label="{{'Save' | translate }}" *ngIf="activitytype === 'ACTIVITY'" [disabled]="!dynamicform.valid" icon="ui-icon-save" class="wrapperCenter"></button>
                </div>
            </div>
        </div>
    </div>
</form>