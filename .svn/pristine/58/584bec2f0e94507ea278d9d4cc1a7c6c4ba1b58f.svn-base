import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {AppRoutes} from './app.routes';
import 'rxjs/add/operator/toPromise';

import {AccordionModule} from 'primeng/primeng';
import {AutoCompleteModule} from 'primeng/primeng';
import {BreadcrumbModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
import {CarouselModule} from 'primeng/primeng';
import {ChartModule} from 'primeng/primeng';
import {CheckboxModule} from 'primeng/primeng';
import {ChipsModule} from 'primeng/primeng';
import {CodeHighlighterModule} from 'primeng/primeng';
import {ConfirmDialogModule} from 'primeng/primeng';
import {ColorPickerModule} from 'primeng/primeng';
import {SharedModule} from 'primeng/primeng';
import {ContextMenuModule} from 'primeng/primeng';
import {DataGridModule} from 'primeng/primeng';
import {DataListModule} from 'primeng/primeng';
import {DataScrollerModule} from 'primeng/primeng';
import {DataTableModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {DragDropModule} from 'primeng/primeng';
import {DropdownModule} from 'primeng/primeng';
import {EditorModule} from 'primeng/primeng';
import {FieldsetModule} from 'primeng/primeng';
import {GalleriaModule} from 'primeng/primeng';
import {GMapModule} from 'primeng/primeng';
import {GrowlModule} from 'primeng/primeng';
import {InputMaskModule} from 'primeng/primeng';
import {InputSwitchModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {InputTextareaModule} from 'primeng/primeng';
import {LightboxModule} from 'primeng/primeng';
import {ListboxModule} from 'primeng/primeng';
import {MegaMenuModule} from 'primeng/primeng';
import {MenuModule} from 'primeng/primeng';
import {MenubarModule} from 'primeng/primeng';
import {MessagesModule} from 'primeng/primeng';
import {MultiSelectModule} from 'primeng/primeng';
import {OrderListModule} from 'primeng/primeng';
import {OrganizationChartModule} from 'primeng/primeng';
import {OverlayPanelModule} from 'primeng/primeng';
import {PaginatorModule} from 'primeng/primeng';
import {PanelModule} from 'primeng/primeng';
import {PanelMenuModule} from 'primeng/primeng';
import {PasswordModule} from 'primeng/primeng';
import {PickListModule} from 'primeng/primeng';
import {ProgressBarModule} from 'primeng/primeng';
import {RadioButtonModule} from 'primeng/primeng';
import {RatingModule} from 'primeng/primeng';
import {ScheduleModule} from 'primeng/primeng';
import {SelectButtonModule} from 'primeng/primeng';
import {SlideMenuModule} from 'primeng/primeng';
import {SliderModule} from 'primeng/primeng';
import {SpinnerModule} from 'primeng/primeng';
import {SplitButtonModule} from 'primeng/primeng';
import {StepsModule} from 'primeng/primeng';
import {TabMenuModule} from 'primeng/primeng';
import {TabViewModule} from 'primeng/primeng';
import {TerminalModule} from 'primeng/primeng';
import {TieredMenuModule} from 'primeng/primeng';
import {ToggleButtonModule} from 'primeng/primeng';
import {ToolbarModule} from 'primeng/primeng';
import {TooltipModule} from 'primeng/primeng';
import {TreeModule} from 'primeng/primeng';
import {TreeTableModule} from 'primeng/primeng';
import { FileUploadModule } from 'ng2-file-upload';

import {AppComponent} from './app.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SigninComponent } from './components/app-components/signin/signin.component';
import { UserService } from './service/user.service';
import { ContentService } from './service/content.service';
import { DocumentService } from './service/document.service';
import { AdministrationService } from './service/Administration.service';
import { SchemaService } from './service/schema.service';
import { IntegrationService } from './service/integration.service';
import { WorkService } from './service/work.service';
import { HttpInterceptor } from './service/interceptor.service';
import { AdminLayoutComponent } from './components/app-components/layout/admin-layout/admin-layout.component';
import { FooterComponent } from './components/app-components/layout/footer/footer.component';
import { TopbarComponent } from './components/app-components/layout/topbar/topbar.component';
import { BreadcrumbComponent } from './components/app-components/layout/breadcrumb/breadcrumb.component';
import { DashboardComponent } from './components/app-components/dashboard/dashboard.component';
import { BreadcrumbService } from './components/app-components/layout/breadcrumb/breadcrumb.service';
import { MenuItemsComponent, SubMenuMenuItemsComponent } from './components/app-components/layout/menu-items/menu-items.component';
import { CreateComponent } from './components/app-components/create/create.component';
import { SignatureComponent } from './components/generic-components/signature-pad/signature-pad.component';
import { AnnotationComponent } from './components/app-components/annotation/annotation.component';
import { DocumentAnnotationComponent } from './components/generic-components/document-annotation/document-annotation.component';
import { ItemListComponent } from './components/generic-components/item-list/item-list.component';
import { InboxComponent } from './components/app-components/inbox/inbox.component';
import { SentComponent } from './components/app-components/sent/sent.component';
import { DraftComponent } from './components/app-components/draft/draft.component';
import { DocViewerComponent } from './components/generic-components/doc-viewer/doc-viewer.component';
import { ActivityComponent } from './components/app-components/activity/activity.component';
import { DocumentViewComponent } from './components/app-components/document-view/document-view.component';
import { FolderViewComponent } from './components/generic-components/folder-view/folder-view.component';
import { ToastrModule } from 'ngx-toastr';
import { SearchComponent } from './components/app-components/search/search.component';
import { RegisterComponent } from './components/app-components/register/register.component';
import { ArchiveComponent } from './components/app-components/archive/archive.component';
import { AdvancedSearchComponent } from './components/app-components/advanced-search/advanced-search.component';
import { DocumentSearchComponent } from './components/generic-components/document-search/document-search.component';
import { SettingsComponent } from './components/app-components/settings/settings.component';
import { AngularDraggableModule } from 'angular2-draggable';
import { ReportsComponent } from './components/app-components/reports/reports.component';
import { ReportService } from './service/report.service';
import { ConfigurationService } from './service/Configuration.service';
import { DocPropertyComponent } from './components/generic-components/doc-property/doc-property.component';
import { RecentDocumentsComponent } from './components/generic-components/recent-documents/recent-documents.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { DynamicFormComponent } from './components/generic-components/dynamic-form/dynamic-form.component';
import { FolderTreeComponent } from './components/generic-components/folder-tree/folder-tree.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { CreateFormNamesComponent } from './components/app-components/create-form-names/create-form-names.component';
import { AuthGuard } from './service/auth.guard';
import { IndexDocumentComponent } from './components/app-components/index-document/index-document.component';


export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  }

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutes,
        HttpModule,
        BrowserAnimationsModule,
        AccordionModule,
        AutoCompleteModule,
        BreadcrumbModule,
        ButtonModule,
        CalendarModule,
        CarouselModule,
        ChartModule,
        CheckboxModule,
        ChipsModule,
        CodeHighlighterModule,
        ConfirmDialogModule,
        ColorPickerModule,
        SharedModule,
        ContextMenuModule,
        DataGridModule,
        DataListModule,
        DataScrollerModule,
        DataTableModule,
        DialogModule,
        DragDropModule,
        DropdownModule,
        EditorModule,
        FieldsetModule,
        GalleriaModule,
        GMapModule,
        GrowlModule,
        InputMaskModule,
        InputSwitchModule,
        InputTextModule,
        InputTextareaModule,
        LightboxModule,
        ListboxModule,
        MegaMenuModule,
        MenuModule,
        MenubarModule,
        MessagesModule,
        MultiSelectModule,
        OrderListModule,
        OrganizationChartModule,
        OverlayPanelModule,
        PaginatorModule,
        PanelModule,
        PanelMenuModule,
        PasswordModule,
        PickListModule,
        ProgressBarModule,
        RadioButtonModule,
        RatingModule,
        ScheduleModule,
        SelectButtonModule,
        SlideMenuModule,
        SliderModule,
        SpinnerModule,
        SplitButtonModule,
        StepsModule,
        TabMenuModule,
        TabViewModule,
        TerminalModule,
        TieredMenuModule,
        ToggleButtonModule,
        ToolbarModule,
        TooltipModule,
        TreeModule,
        TreeTableModule,
        HttpClientModule,
        ReactiveFormsModule,
        FileUploadModule,
        TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useFactory: (createTranslateLoader),
              deps: [HttpClient]
            }
          }),
          FileUploadModule,
          ToastrModule.forRoot({
            timeOut: 5000,
            positionClass: 'toast-bottom-right',
            preventDuplicates: true
          }),
           AngularDraggableModule,
           NgxSmartModalModule.forRoot(),
           Ng4LoadingSpinnerModule.forRoot()
    ],
    declarations: [
        SigninComponent,
        AppComponent,
        AdminLayoutComponent,
        FooterComponent,
        TopbarComponent,
        BreadcrumbComponent,
        DashboardComponent,
        MenuItemsComponent,
        SubMenuMenuItemsComponent,
        CreateComponent,
        SignatureComponent,
        AnnotationComponent,
        DocumentAnnotationComponent,
        DocumentViewComponent,
        ItemListComponent,
        InboxComponent,
        SentComponent,
        DraftComponent,
        DocViewerComponent,
        ActivityComponent,
        DocViewerComponent,
        FolderViewComponent,
        SearchComponent,
        RegisterComponent,
        ArchiveComponent,
        AdvancedSearchComponent,
        DocumentSearchComponent,
        SettingsComponent,
        ReportsComponent,
        MenuItemsComponent,
        DocPropertyComponent,
        RecentDocumentsComponent,
        DynamicFormComponent,
        FolderTreeComponent,
        CreateFormNamesComponent,
        IndexDocumentComponent,
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy },
        UserService, BreadcrumbService,
    ContentService,
    DocumentService,
    AdministrationService,
    SchemaService,
    IntegrationService,
    WorkService,
    HttpInterceptor,
    ReportService,
    ConfigurationService,
    AuthGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
