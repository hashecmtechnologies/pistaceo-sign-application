'use strict';
export let stringOperator = [
  {label: 'Contains', value: 'Contains'},
  {label: 'starts With', value: 'Starts With'},
  {label: 'Ends with', value: 'Ends With'},
  {label: 'Equals To', value: 'Equals To'}
  ];


export let dateOperator = [
{label: 'Before', value: 'Before'},
{label: 'Equals To', value: 'Equals To'},
{label: 'After', value: 'After'},
{label: 'Between', value: 'Between'}];

export let corrspondenceType = [
{value: 'All', displayname: 'All'},
{value: 'Incoming', displayname: 'Incoming'},
{value: 'Outgoing', displayname: 'Outgoing'}
];

export let Security = [
  {name: 'Public', value: 1},
  {name: 'Restricted', value: 2},
  {name: 'Confidential', value: 3},
  {name: 'Secret', value: 4}
  // {name:'Top Secret', value:5}
];

export let Type = [
  {name: 'Incoming', value: 'Incoming'},
  {name: 'Outgoing', value: 'Outgoing'}
];

export let languageOptions = [
  {label: 'English', value: 'en'},
  {label: 'Kannada', value: 'ka'},
  {label: 'Arabic', value: 'ar'}
];
