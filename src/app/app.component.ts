import {Component, ElementRef, Renderer, ViewChild} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(public renderer: Renderer, translate: TranslateService) {
    }

}

