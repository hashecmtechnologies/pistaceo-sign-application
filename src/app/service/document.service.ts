import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';

import * as global from '../global.variables';
import { HttpInterceptor } from './interceptor.service';
import { UserService } from './user.service';
import { environment } from '../../environments/environment';

@Injectable()
export class DocumentService {
  private header: Headers;
  private base_url: String;
  constructor(private http: HttpInterceptor, private us: UserService) {
    if (environment.production) {
      this.base_url = global.AppURLSettings.SANDBOX_URL;
    } else {
      this.base_url = global.AppURLSettings.DEV_URL;
    }
    this.header = new Headers();
    this.header.append('token', localStorage.getItem('signapptoken'));
  }

  addDocument(form) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/addDocument?sysdatetime=${fulldatetime}`;
    return this.http.post(url, form, { headers: this.header }).map(
      res => res
    );
  }
  downloadMultipleDocuments(docIds) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/downloadMultipleDocuments?sysdatetime=${fulldatetime}`;
    return this.http.post(url, docIds, { headers: this.header }).map(
      res => res
    );
    //  return url;
  }

  downloadDocument(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/downloadDocument?id=${docId}&sysdatetime=${fulldatetime}`;
    //  return url;
    return this.http.get(url, { responseType: ResponseContentType.Blob, headers: this.header }).map(
      res => res
    );
  }

  downloadThisDocument(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/downloadThisDocument?id=${docId}&sysdatetime=${fulldatetime}`;
    // return url;
    return this.http.get(url, { responseType: ResponseContentType.Blob, headers: this.header }).map(
      res => res
    );
  }
  checkOut(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/checkOut?id=${docId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  getThisDocument(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getThisDocument?id=${docId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  // searchDocuments(request: any): any {
  //   const url = `${this.base_url}DocumentService/search`;
  //   return this.http.post(url, request, {headers : this.header}).map(res => res.json());
  // }

  cancelCheckOut(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/cancelCheckOut?id=${docId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  checkIn(formData) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/checkIn?sysdatetime=${fulldatetime}`;
    return this.http.post(url, formData, { headers: this.header }).map(
      res => res
    );
  }

  viewDocument(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/downloadDocument?id=${docId}&sysdatetime=${fulldatetime}`;
    // return url;
    return this.http.get(url, { responseType: ResponseContentType.Blob, headers: this.header }).map(
      res => res
    );
  }

  getDocumentFolders(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getDocumentFolders?id=${docId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  getFavorites(empNo) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getFavorites?empno=${empNo}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  addToFavorites(empno, id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/addToFavorites?empno=${empno}&id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  removeFromFavorites(empno, id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/removeFromFavorites?empno=${empno}&id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  getDocumentVersions(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getDocumentVersions?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  getDocumentPermissions(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getDocumentPermissions?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  getRecent(empNo) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getRecent?empno=${empNo}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  getDocumentPageCount(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getDocumentPageCount?id=${docId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  getPreviewPage(docId, pagenumber) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getPreviewPage?id=${docId}&page=${pagenumber}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  downloadPreviewPage(docId, pagenumber) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const token = localStorage.getItem('signapptoken');
    const url = `${this.base_url}DocumentService/downloadPreviewPage?id=${docId}&page=${pagenumber}&empno=${this.us.getCurrentUser().EmpNo}&token=${token}&sysdatetime=${fulldatetime}`;
    return url;
  }

  downloadAnnotation(docId, pagenumber) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const token = localStorage.getItem('signapptoken');
    const url = `${this.base_url}DocumentService/downloadAnnotation?id=${docId}&page=${pagenumber}&empno=${this.us.getCurrentUser().EmpNo}&token=${token}&sysdatetime=${fulldatetime}`;
    return url;
  }


  saveAnnotation(annotation) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/saveAnnotation?sysdatetime=${fulldatetime}`;
    return this.http.post(url, annotation, { headers: this.header }).map(
      res => res
    );
  }

  getAnnotations(documentId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getAnnotations?id=${documentId}&status=ALL&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  updateProperties(val) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/updateProperties?sysdatetime=${fulldatetime}`;
    return this.http.post(url, val, { headers: this.header }).map(
      res => res
    );
  }
  downloadAnnotatedDocument(documentId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/downloadAnnotatedDocument?id=${documentId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { responseType: ResponseContentType.Blob, headers: this.header }).map(
      res => res
    );
  }

  downloadSignedDocument(documentId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/downloadSignedDocument?id=${documentId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { responseType: ResponseContentType.Blob, headers: this.header }).map(
      res => res
    );
  }

  completeAnnotation(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/completeAnnotation?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  deleteAnnotation(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/deleteAnnotation?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  getJSONFromDocument(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getJSONFromDocument?docId=${docId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  addJSONDocument(formDetails) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/addJSONDocument?sysdatetime=${fulldatetime}`;
    return this.http.post(url, formDetails, { headers: this.header }).map(res => res);
  }


  downloadPrintDocument(docId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/downloadPrintDocument?id=${docId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { responseType: ResponseContentType.Blob, headers: this.header }).map(res => res);
  }

  getFormJSONDocumentForActivity(docId, activitiId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getFormJSONDocumentForActivity?docId=${docId}&activityId=${activitiId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(res => res);
  }

  getDocumentActions(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getDocumentActions?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  saveFormDocument(formDetails) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/saveFormDocument?sysdatetime=${fulldatetime}`;
    return this.http.post(url, formDetails, { headers: this.header }).map(res => res);
  }
  canSignDocumentPage(docid, pageno, activityid) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/canSignDocumentPage?docid=${docid}&pageno=${pageno}&&activityid=${activityid}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  signDocumentPageWithStoredSignature(docid, pageno) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/signDocPageWithStoredSignature?docid=${docid}&pageno=${pageno}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  signDocPageWithNewSignature(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/signDocPageWithNewSignature?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header }).map(
      res => res
    );
  }
  signDocPageWithFileSignature(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/signDocPageWithFileSignature?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header }).map(
      res => res
    );
  }
  validateSignRequest(value) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/validateSignRequest?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value).map(
      res => res
    );
  }
  validateSignaturePIN(value, token) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    // localStorage.removeItem('token');
    // localStorage.setItem('token', token);
    this.header = new Headers();
    this.header.append('token', token);
    const url = `${this.base_url}DocumentService/validateSignaturePIN?sysdatetime=${fulldatetime}`;
    return this.http.post(url, value, { headers: this.header }).map(
      res => res
    );
  }
  getPagesForSignature(docid, roleid) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getPagesForSignature?docid=${docid}&roleid=${roleid}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  getSignHolders(docid, page, roleid) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}DocumentService/getSignHolders?docid=${docid}&page=${page}&roleid=${roleid}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
}
