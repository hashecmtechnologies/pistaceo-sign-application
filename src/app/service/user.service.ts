import { Injectable } from '@angular/core';
import { Http, Response, Headers, ResponseContentType, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import * as global from '../global.variables';
import { HttpInterceptor } from './interceptor.service';
import { environment } from '../../environments/environment';

@Injectable()
export class UserService {
  private base_url: string;
  private header: Headers;
  constructor(private http: HttpInterceptor) {
    if (environment.production) {
      this.base_url = global.AppURLSettings.SANDBOX_URL;
    } else {
      this.base_url = global.AppURLSettings.DEV_URL;
    }
    this.header = new Headers();
    if (localStorage.getItem('signapptoken') !== null) {
      this.header.append('signapptoken', localStorage.getItem('signapptoken'));
    }
  }
  authenticateUser(username, password, policy) {
    localStorage.removeItem('token');
    const headers = new Headers();
    headers.append('userid', username);
    headers.append('password', password);
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const timeZone = new Date().getTimezoneOffset();
    headers.append('timezone', timeZone + '');
    // const url = `${this.base_url}UserService/authenticateUser?savelogin=${policy}&sysdatetime=${fulldatetime}`;
    // return this.http.get(url, { headers: headers }).map(
    //   response => { this.responsefromlog(response); }
    // );
  }
  responsefromlog(data) {
    localStorage.setItem('signapptoken', data._body);
    this.header.append('signapptoken', localStorage.getItem('signapptoken'));
  }

  searchUsers(text) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/searchUsers?key=NAME&text=${text}&usertype=role&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  errorHandler(error: Response) {
    return Observable.throw(error || 'Server Error');
  }

  userDetail(res) {
    const x = res._body;
  }

  getCurrentUser(): any {
    return JSON.parse(localStorage.getItem('user'));
  }

  getRoles() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/getRoles?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  getUserDetails(userId) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/getUserDetails?userid=${userId}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  getUsers() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/getUsers?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  saveUser(user) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/saveUser?sysdatetime=${fulldatetime}`;
    return this.http.post(url, user, { headers: this.header }).map(res => res);
  }

  saveRole(role) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/saveRole?sysdatetime=${fulldatetime}`;
    return this.http.post(url, role, { headers: this.header }).map(res => res);
  }

  getOrgRoles() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/getOrgRoles?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(res => res);
  }

  getSubOrgRoles(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/getSubOrgRoles?orgId=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(res => res);
  }

  getUserDelegations() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/getUserDelegations?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(res => res);
  }

  saveDelegation(userDetail) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/saveDelegation?sysdatetime=${fulldatetime}`;
    return this.http.post(url, userDetail, { headers: this.header }).map(res => res);
  }
  getRoleMembers(roleid) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/getRoleMembers?roleId=${roleid}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(res => res);
  }
  revokeDelegation(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/revokeDelegation?id=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(res => res);
  }
  generatePin() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/generatePIN?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  saveSignature(id) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/saveSignature?signature=${id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  validatepin(pin) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/validatePIN?pin=${pin}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  searchRoles(text) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/searchRoles?text=${text}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }

  changeUserPassword(password) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/changeUserPassword?sysdatetime=${fulldatetime}`;
    return this.http.post(url, password, { headers: this.header }).map(
      res => res
    );
  }

  updateUserSettings(updateSetting) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/updateUserSettings?sysdatetime=${fulldatetime}`;
    return this.http.post(url, updateSetting, { headers: this.header }).map(
      res => res
    );
  }
  getUserHistoryItems() {
    const sysDateTime = new Date();
    const otherDate = new Date().toUTCString();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/getUserHistory?before=${otherDate}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(res => res);

  }
  searchSubordinateRoles(text) {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/searchSubordinateRoles?text=${text}&roleid=${this.getCurrentUser().roles[0].id}&sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
  logoutUser() {
    const sysDateTime = new Date();
    const fulldatetime = sysDateTime.getTime();
    const url = `${this.base_url}UserService/logoutUser?sysdatetime=${fulldatetime}`;
    return this.http.get(url, { headers: this.header }).map(
      res => res
    );
  }
}
