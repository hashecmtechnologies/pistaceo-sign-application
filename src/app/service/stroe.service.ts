import { Injectable } from '@angular/core';
import { HttpInterceptor } from './interceptor.service';
import { UserService } from './user.service';

@Injectable()

export class StoreService {

    public activityid: any;
    public docid: any;
    public roleid: any;
    public signIn: any;
    public token: any;


    constructor(private http: HttpInterceptor, private us: UserService) {
    }

}
