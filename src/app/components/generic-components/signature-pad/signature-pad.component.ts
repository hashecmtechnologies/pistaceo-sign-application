'use strict';

import { Component, ElementRef, EventEmitter, Input, Output, AfterContentInit, OnChanges, OnInit } from '@angular/core';

declare var require: any;

export interface Point {
  x: number;
  y: number;
  time: number;
}

export type PointGroup = Array<Point>;

@Component({
  templateUrl: './signature-pad.component.html',
  selector: 'app-signature-pad',
})

export class SignatureComponent implements AfterContentInit {

  @Input() public options: Object;
  @Input() public canvasWidth: any;
  @Input() public canvasHeight: any;
  @Output() public onBeginEvent: EventEmitter<boolean>;
  @Output() public onEndEvent: EventEmitter<boolean>;
  @Input() public backgroundImages: any[];
  @Input() public disabled: any;
  private signaturePad: any;
  private elementRef: ElementRef;

  constructor(elementRef: ElementRef) {
    this.elementRef = elementRef;
    this.options = this.options || {};
    this.onBeginEvent = new EventEmitter();
    this.onEndEvent = new EventEmitter();
  }

  public ngAfterContentInit(): void {
    const sp: any = require('signature_pad')['default'];
    const canvas: any = this.elementRef.nativeElement.querySelector('canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = this.canvasWidth;
    canvas.height = this.canvasHeight;
    this.signaturePad = new sp(canvas, this.options);
    this.signaturePad.onBegin = this.onBegin.bind(this);
    this.signaturePad.onEnd = this.onEnd.bind(this);
    if (this.disabled === 'readonly') {
      this.signaturePad.off();
    }
  }

  public resizeCanvas(): void {
    const ratio: number = Math.max(window.devicePixelRatio || 1, 1);
    const canvas: any = this.signaturePad._canvas;
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext('2d').scale(ratio, ratio);
    this.signaturePad.clear();
  }

  public toData(): Array<PointGroup> {
    return this.signaturePad.toData();
  }

  public fromData(points: Array<PointGroup>): void {
    this.signaturePad.fromData(points);
  }
  public toDataURL(imageType?: string, quality?: number): string {
    return this.signaturePad.toDataURL(imageType, quality);
  }

  public fromDataURL(backgroundImages): void {
    let imageUrl = '';
    for (let index = backgroundImages.length; index > 0; index--) {
      imageUrl = imageUrl + `url(${backgroundImages[index - 1].url})`.replace('NaN', '1');
      if (index - 1 > 0) {
        imageUrl = imageUrl + ',';
      }
    }
    this.signaturePad.canvas.style.backgroundImage = imageUrl;
    this.signaturePad.canvas.style.backgroundSize = '100% 100%';
  }


  public clear(): void {
    this.signaturePad.clear();
  }

  public isEmpty(): boolean {
    return this.signaturePad.isEmpty();
  }

  // Unbinds all event handlers
  public off(): void {
    this.signaturePad.off();
  }

  public on(): void {
    this.signaturePad.on();
  }

  public set(option: string, value: any): void {

    switch (option) {
      case 'canvasHeight':
        this.signaturePad._canvas.height = value;
        break;
      case 'canvasWidth':
        this.signaturePad._canvas.width = value;
        break;
      default:
        this.signaturePad[option] = value;
    }
  }

  // notify subscribers on signature begin
  public onBegin(): void {
    this.onBeginEvent.emit(true);
  }

  // notify subscribers on signature end
  public onEnd(): void {
    this.onEndEvent.emit(true);
  }
}
