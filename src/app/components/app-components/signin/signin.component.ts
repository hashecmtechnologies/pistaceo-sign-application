import { UserService } from '../../../service/user.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { DocumentService } from '../../../service/document.service';
import { ToastrService } from 'ngx-toastr';
import { StoreService } from '../../../service/stroe.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  public form: FormGroup;
  errorMessage = false;
  signIn: boolean;
  private user: any = localStorage.getItem('user');
  public capsLock = false;
  public capsOn;
  public showcookiepolicy = true;
  public uname: any;
  public validateToken = false;
  public activityid: any;
  public docid: any;
  public roleid: any;
  public token: any;
  public errorText: any;
  constructor(private router: Router, private userservice: UserService,
    private ds: DocumentService, private tr: ToastrService, private store: StoreService, public route: ActivatedRoute) {

  }

  ngOnInit() {
    this.store.signIn = true; // change later
    this.uname = undefined;
    localStorage.removeItem('signapptoken');
    this.route.params.subscribe(data => {
      const token1 = data.urlLink.split(',');
      const token = token1[0];
      const val = token1[1];
      const value = {
        'key': token,
        'value': val
      };
      this.ds.validateSignRequest(value).subscribe(datas => { this.validToken(datas, value.key); }, error => { this.validateToken = false; this.showError(error); });
    });
    if (environment.sso) {
      let value = 0;
      if (this.showcookiepolicy === true) {
        value = 1;
      }
    } else {
      document.getElementById('spinner').style.display = 'none';
    }
  }
  showError(error) {
    const err = JSON.parse(error._body);
    this.errorText = err.message;
  }
  validToken(data, key) {
    if (data._body) {
      if (data.status === 200) {
        this.validateToken = true;
        const dataValue = JSON.parse(data._body);
        this.activityid = dataValue.activityid;
        this.docid = dataValue.docid;
        this.roleid = dataValue.roleid;
        this.token = key;
        this.store.token = key;
        localStorage.removeItem('signapptoken');
        localStorage.setItem('signapptoken', key);
      }
    }

  }
  getDeploymentType(data) {
    if (data._body === 'CLOUD') {
      const emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
    }
  }
  keyup(event, value) {
    const nextInput = event.srcElement.nextElementSibling; // get the sibling element

    const target = event.target || event.srcElement;
    const id = target.id;
    if (nextInput == null) {  // check the maxLength from here
      return;
    } else {
      nextInput.focus();
    } // focus if not null
  }

  onSubmit(form) {
    if ((this.uname + '').length === 4) {
      const value = {
        pin: this.uname,
        activityid: this.activityid,
        docid: this.docid,
        roleid: this.roleid
      };
      document.getElementById('spinner').style.display = 'block';
      this.ds.validateSignaturePIN(value, this.token).subscribe(data => { this.pinSucess(data); }, error => { document.getElementById('spinner').style.display = 'none'; }); // this.tr.error(error.message);
    } else {
      this.tr.error('', 'Please enter valid 4 didgit pin');
      return;
    }
  }
  pinSucess(data) {
    this.router.navigate(['/home'], { queryParams: { activityid: this.activityid, docid: this.docid, roleid: this.roleid } });
    // this.app.signIn = true;
    document.getElementById('spinner').style.display = 'none';
  }
  login(username, datares) {
    this.userservice.getUserDetails(username).subscribe(
      data => this.signInUser(data), error => {
        this.errorMessage = true;
        document.getElementById('spinner').style.display = 'none';
        localStorage.removeItem('token');
      });
  }
  signTyped() {
    this.signIn = false;
  }

  signInUser(data) {
    if (data._body !== '') {
      this.user = JSON.parse(data._body);
      localStorage.setItem('user', JSON.stringify(this.user));
      if (this.user.settings !== undefined) {
        for (let index = 0; index < this.user.settings.length; index++) {
          if (this.user.settings[index].key === 'Default Document View') {
            localStorage.setItem(this.user.settings[index].key, this.user.settings[index].val);
            break;
          }
        }
        for (let index = 0; index < this.user.settings.length; index++) {
          if (this.user.settings[index].key === 'Default Theme') {
            localStorage.setItem(this.user.settings[index].key, this.user.settings[index].val);
            break;
          }
        }
        for (let other = 0; other < this.user.settings.length; other++) {
          if (this.user.settings[other].key !== 'Default Document View' && this.user.settings[other].key !== 'Default Theme') {
            localStorage.setItem(this.user.settings[other].key, this.user.settings[other].val);
          }
        }
      }
      this.router.navigateByUrl(`/home`);
    } else {
      this.signIn = true;
      document.getElementById('spinner').style.display = 'none';
    }
  }


  detectCapsLock(event) {
    if (event.getModifierState('CapsLock')) {
      this.capsLock = true;
    } else {
      this.capsLock = false;
    }
  }
  saveLoginDetails(event) {
    if (event === true) {
      this.showcookiepolicy = true;
    } else {
      this.showcookiepolicy = false;
    }
  }
  policy() {
    window.open('../../../../assets/CookiePolicy/Cookie Policy.html');
  }
}
