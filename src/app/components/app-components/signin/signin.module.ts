import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SigninRoutes } from './signin.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutComponent } from '../layout/admin-layout/admin-layout.component';
// import { TrackCapsDirective } from './capsLock';
@NgModule({
  imports: [CommonModule, RouterModule.forChild(SigninRoutes), FormsModule, ReactiveFormsModule],
  declarations: [],
})

export class SigninModule {
}
