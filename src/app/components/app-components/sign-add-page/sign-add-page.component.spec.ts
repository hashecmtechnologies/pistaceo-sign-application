import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignAddPageComponent } from './sign-add-page.component';

describe('SignAddPageComponent', () => {
  let component: SignAddPageComponent;
  let fixture: ComponentFixture<SignAddPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignAddPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
