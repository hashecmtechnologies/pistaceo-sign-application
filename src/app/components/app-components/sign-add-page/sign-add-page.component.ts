import { Component, OnInit, ViewChild, OnChanges, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DocumentService } from '../../../service/document.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { SignatureComponent } from '../../generic-components/signature-pad/signature-pad.component';
import { WorkService } from '../../../service/work.service';
import { ToastrService } from 'ngx-toastr';
import { FileUploader } from 'ng2-file-upload';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-sign-add-page',
  templateUrl: './sign-add-page.component.html',
  styleUrls: ['./sign-add-page.component.css']
})
export class SignAddPageComponent implements OnChanges, OnInit {
  @ViewChild('signatureSignAdd') signaturePadSignAdd: SignatureComponent;
  menuClick: boolean;

  menuButtonClick: boolean;

  topbarMenuButtonClick: boolean;

  topbarMenuClick: boolean;

  topbarMenuActive: boolean;

  activeTopbarItem: Element;
  mobileMenuActive: boolean;
  layoutStatic = true;
  public activityid: any;
  public docid: any;
  public roleid: any;
  public pageValues: any;
  public timestamp;
  public AnnotationCurrentPage = 1;
  public changedValue: any;
  public showSignTab = false;
  public addSignture: any;
  public docType = 'annotate';
  public isMobileYup = false;
  public globelFileUploder = new FileUploader({});
  public imageShow = false;
  public reader = new FileReader();
  public imgURL: any;
  public formData: any;
  public disableSubmitbut = false;
  constructor(private router: Router, private activeRoute: ActivatedRoute, private ds: DocumentService,
    private spinner: Ng4LoadingSpinnerService, public changeDetectRef: ChangeDetectorRef,
    private ws: WorkService, private tr: ToastrService, private ngxModal: NgxSmartModalService, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.spinner.show();
    this.showSignTab = false;
    this.activeRoute.queryParams.subscribe(params => {
      this.activityid = params.activityid;
      this.docid = params.docid;
      this.roleid = params.roleid;
      this.ds.getPagesForSignature(this.docid, this.roleid).subscribe(data => {
        this.pagesResult(data);
        this.spinner.hide();
      }, error => { this.spinner.hide(); });
      if (window.innerWidth <= 1024) {
        this.isMobileYup = true;
      } else {
        this.isMobileYup = false;
      }
    });
  }
  ngOnChanges() {
    if (window.innerWidth <= 1024) {
      this.isMobileYup = true;
    } else {
      this.isMobileYup = false;
    }
    this.docType = 'annotate';
  }
  pagesResult(data) {
    if (data) {
      const value = JSON.parse(data._body);
      this.pageValues = value;
      if (this.pageValues) {
        for (let i = 0; i < this.pageValues.length; i++) {
          if (this.pageValues[i].status === 'SIGNED') {
            this.disableSubmitbut = false;
          } else {
            this.disableSubmitbut = true;
            break;
          }
        }
        if (this.pageValues.length === 0) {
          this.disableSubmitbut = false;
        }
      } else {
        this.disableSubmitbut = false;
      }
    }
  }
  logout() {
    this.router.navigateByUrl('/');
    setTimeout(() => {
      window.location.reload();
    }, 10);
  }
  onWrapperClick() {
    if (!this.menuClick && !this.menuButtonClick) {
      this.mobileMenuActive = false;
    }

    if (!this.topbarMenuClick && !this.topbarMenuButtonClick) {
      this.topbarMenuActive = false;
      this.activeTopbarItem = null;
    }

    this.menuClick = false;
    this.menuButtonClick = false;
    this.topbarMenuClick = false;
    this.topbarMenuButtonClick = false;
  }
  isMobile() {
    return window.innerWidth <= 1024;
  }
  onMenuButtonClick(event: Event) {
    this.menuButtonClick = true;

    if (this.isMobile()) {
      this.mobileMenuActive = !this.mobileMenuActive;
    }

    event.preventDefault();
  }

  pageChangedEvent(event) {
    this.changedValue = event;
    this.AnnotationCurrentPage = event.pageno;
    this.showSignTab = false;



  }
  submitClicked(event) {
    if (event === 'yes') {
      this.spinner.show();
      this.ws.getActivityForAction(this.activityid).subscribe(data => this.getactivityinfo(data), error => { this.spinner.hide(); });
    }
  }
  submittedValue() {
    this.spinner.show();
    this.ws.getActivityForAction(this.activityid).subscribe(data => this.getactivityinfo(data), error => { this.spinner.hide(); });
  }
  getactivityinfo(datas) {
    const activityinfo = JSON.parse(datas._body);
    if (datas) {
      const value = {
        id: this.activityid,
        routes: [{
          activityType: activityinfo.type.responses[0].routeToId,
          roleId: activityinfo.type.responses[0].roleId
        }],
        responseId: activityinfo.type.responses[0].id,
        comments: 'signed'
      };
      this.ws.finishActivity(value).subscribe(data => { this.submitSuccessed(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    }
  }
  submitSuccessed(data) {
    this.router.navigateByUrl('/successPage');
    localStorage.removeItem('signapptoken');
  }
  pageChangeEmitted(event) {
    this.spinner.show();
    this.AnnotationCurrentPage = event.page + 1;
  }
  pageChanged(value) {
    this.spinner.show();
    this.AnnotationCurrentPage = value.pageno;
    const date = new Date();
    this.timestamp = date.getSeconds();
  }
  AddSignComplete(event) {
    this.addSignture = this.signaturePadSignAdd.toDataURL();
    if (this.isMobile()) {
      this.mobileMenuActive = false;
    }
  }
  clearSignature() {
    if (!this.imageShow) {
      this.signaturePadSignAdd.clear();
    } else {
      this.globelFileUploder = new FileUploader({});
      this.imgURL = '';
      this.imageShow = false;
      this.addSignture = null;
    }
  }
  savesignature() {
    if (!this.imageShow) {
      if (this.addSignture) {
        this.addSignture = this.signaturePadSignAdd.toDataURL();
        const value = {
          docId: this.docid,
          pageNo: this.AnnotationCurrentPage,
          comment: this.addSignture
        };
        this.spinner.show();
        this.ds.signDocPageWithNewSignature(value).subscribe(data => { this.savedsignature(data, this.docid); this.spinner.hide(); }, error => { this.spinner.hide(); });
      } else {
        this.tr.info('', 'Please add sign to save');
      }
    } else {
      const value = {
        docId: this.docid,
        pageNo: this.AnnotationCurrentPage,
      };
      this.formData = new FormData();
      this.formData.append('Signature', JSON.stringify(value));
      this.formData.append('file', this.globelFileUploder.queue[0]._file);

      this.spinner.show();
      this.ds.signDocPageWithFileSignature(this.formData).subscribe(data => { this.savedsignature(data, this.docid); this.spinner.hide(); }, error => { this.spinner.hide(); });
    }
  }
  savedsignature(data, docid) {
    if (!this.imageShow) {
      this.signaturePadSignAdd.clear();
    }
    this.ngxModal.getModal('addPad').close();
    const date = new Date();
    this.timestamp = date.getSeconds();
    this.imageShow = false;
    this.addSignture = null;
    this.ngOnInit();

  }
  showSignPop(event) {
    this.ngxModal.getModal('addPad').open();
    this.addSignture = null;
    this.showSignTab = true;
  }
  closeSignModal(e) {
    this.ngxModal.getModal('addPad').close();
    this.showSignTab = false;
    this.addSignture = null;
  }
  inputfileChanged(event) {
    if (this.globelFileUploder.queue[0]._file.type === 'image/png' || this.globelFileUploder.queue[0]._file.type === 'image/jpeg' || this.globelFileUploder.queue[0]._file.type === 'image/jpg') {
      this.imageShow = true;
      const url = (window.URL) ? window.URL.createObjectURL(this.globelFileUploder.queue[0]._file) : (window as any).webkitURL.createObjectURL(this.globelFileUploder.queue[0]._file);
      this.imgURL = url;
      const value = window.btoa(event.target.files[0]);
      this.addSignture = 'data:image/png;base64,' + value;
    } else {
      this.tr.warning('Please select only .png or .jpeg/.jpg files');
      this.imageShow = false;
    }
  }
  tempObject(event) {
    this.globelFileUploder = new FileUploader({});
  }
}
