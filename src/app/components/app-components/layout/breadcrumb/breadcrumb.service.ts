import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { MenuItem } from 'primeng/primeng';

@Injectable()
export class BreadcrumbService {
private itemsSource = new Subject<MenuItem[]>();
public breadcrumbName;
itemsHandler = this.itemsSource.asObservable();

setItems(items: MenuItem[]) {
this.breadcrumbName = items[0].label;
this.itemsSource.next(items);
}

getItem() {
    return this.breadcrumbName;
}
}
