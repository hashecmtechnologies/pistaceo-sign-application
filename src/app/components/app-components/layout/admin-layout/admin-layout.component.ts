import { Component, OnInit, Renderer, OnChanges } from '@angular/core';
import { TranslateService } from '../../../../../../node_modules/@ngx-translate/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { DocumentService } from '../../../../service/document.service';
import { StoreService } from '../../../../service/stroe.service';
@Component({
    selector: 'app-admin-layout',
    templateUrl: './admin-layout.component.html',
    styleUrls: ['./admin-layout.component.css']
})
export class AdminLayoutComponent implements OnChanges {

    menuClick: boolean;

    menuButtonClick: boolean;

    topbarMenuButtonClick: boolean;

    topbarMenuClick: boolean;

    topbarMenuActive: boolean;

    activeTopbarItem: Element;

    layoutStatic = true;

    sidebarActive: boolean;

    mobileMenuActive: boolean;

    darkMenu: boolean;

    isRTL: boolean;
    public dirType;
    public options = [];
    public show = false;
    public signIn = false;
    public activityid: any;
    public docid: any;
    public roleid: any;
    public itemsToSend: any;
    constructor(public renderer: Renderer, translate: TranslateService, location: Location, router: Router, private ds: DocumentService, private store: StoreService) {


        if (localStorage.getItem('Default Language') === 'ar') {
            this.dirType = 'rtl';
        } else {
            this.dirType = 'ltr';
        }
        if (localStorage.getItem('Default Language') === 'ar') {
            this.isRTL = true;
        } else {
            this.isRTL = false;
        }
        const value = localStorage.getItem('Default Language');
        translate.setDefaultLang(value);
        translate.use(localStorage.getItem('Default Language'));
        // }

        if (localStorage.getItem('Default Theme') === undefined || localStorage.getItem('Default Theme') === null) {
            const themeLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('theme-css');
            themeLink.href = 'assets/theme/theme-' + 'bluegrey' + '.css';
        } else {
            const theme = localStorage.getItem('Default Theme');
            const themeLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('theme-css');
            themeLink.href = 'assets/theme/theme-' + theme + '.css';
            if (theme === 'bluegrey') {
                this.changedLayout('moody');
            } else if (theme === 'indigo') {
                this.changedLayout('reflection');
            } else if (theme === 'pink') {
                this.changedLayout('cityscape');
            } else if (theme === 'purple') {
                this.changedLayout('cloudy');
            } else if (theme === 'deeppurple') {
                this.changedLayout('storm');
            } else if (theme === 'blue') {
                this.changedLayout('palm');
            } else if (theme === 'eezib') {
                this.changedLayout('eezib');
            } else {
                this.changedLayout('flatiron');
            }
        }
        router.events.subscribe((val) => {
            if (location.isCurrentPathEqualTo('/home')) {
                this.show = true;
            } else {
                this.show = true;
            }
        });
    }
    ngOnChanges() {
        if (this.store.signIn === true) {
            this.ds.getPagesForSignature(this.docid, this.roleid).subscribe(data => {
                this.sendDataToMenuItems(data);
            }, error => { });
        }
    }
    sendDataToMenuItems(data) {
        this.itemsToSend = [];
        this.itemsToSend = data._body;
    }
    changedLayout(layout) {
        const layoutLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById('layout-css');
        layoutLink.href = 'assets/layout/css/layout-' + layout + '.css';
    }

    onWrapperClick() {
        if (!this.menuClick && !this.menuButtonClick) {
            this.mobileMenuActive = false;
        }

        if (!this.topbarMenuClick && !this.topbarMenuButtonClick) {
            this.topbarMenuActive = false;
            this.activeTopbarItem = null;
        }

        this.menuClick = false;
        this.menuButtonClick = false;
        this.topbarMenuClick = false;
        this.topbarMenuButtonClick = false;
    }

    onMenuButtonClick(event: Event) {
        this.menuButtonClick = true;

        if (this.isMobile()) {
            this.mobileMenuActive = !this.mobileMenuActive;
        }

        event.preventDefault();
    }

    onTopbarMobileMenuButtonClick(event: Event) {
        this.topbarMenuButtonClick = true;
        this.topbarMenuActive = !this.topbarMenuActive;
        event.preventDefault();
    }

    onTopbarRootItemClick(event: Event, item: Element) {
        if (this.activeTopbarItem === item) {
            this.activeTopbarItem = null;
        } else {
            this.activeTopbarItem = item;
        }

        event.preventDefault();
    }

    onTopbarMenuClick(event: Event) {
        this.topbarMenuClick = true;
    }

    onSidebarClick(event: Event) {
        this.menuClick = true;
    }

    onToggleMenuClick(event: Event) {
        this.layoutStatic = !this.layoutStatic;
    }

    isMobile() {
        return window.innerWidth <= 1024;
    }
    rtlChange(event) {
        if (event === 'ar') {
            this.dirType = 'rtl';
        } else {
            this.dirType = 'ltr';
        }
    }
    updateValue(type, count) {

    }
}
