'use strict';

// export let base_url = 'http://106.51.66.219:9000/Productiviti/resources/';
// export let base_url = 'http://192.168.0.104:8080/Productiviti/resources/';
// export let base_url = 'http://172.20.10.219:9000/CMSV2/resources/';

export class AppURLSettings {
    public static SANDBOX_URL = window.location.origin + '/pistaceo/resources/';
    // public static SANDBOX_URL = 'http://ecmdemo1:8080/pistaceo/resources/';
    // public static DEV_URL = 'http://10.10.10.47:8080/pistaceo/resources/';
    // public static DEV_URL = 'http://10.10.10.44:8080/pistaceo/resources/';
    // public static DEV_URL = 'http://10.10.10.44:8080/Productiviti/resources/';
    // public static DEV_URL = 'http://172.16.10.31:8080/Productiviti/resources/';
    // public static DEV_URL = 'http://192.168.1.66:8080/pistaceo/resources/';
    // public static DEV_URL = 'http://ecmdemo1:8080/pistaceo/resources/'; // Pr0dP@$$
    // public static DEV_URL = 'http://www.pistac.io/pistaceo31/resources/';
    // public static DEV_URL = 'https://jtc-dmsprodtest02.jtckw.com/pistaceo/resources/';
    public static DEV_URL = 'https://www.pistaceo.com/pistaceo/resources/';


}
