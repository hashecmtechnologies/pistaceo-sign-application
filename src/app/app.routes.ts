import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { SigninComponent } from './components/app-components/signin/signin.component';
import { AdminLayoutComponent } from './components/app-components/layout/admin-layout/admin-layout.component';
import { AuthGuard } from './service/auth.guard';
import { SignAddPageComponent } from './components/app-components/sign-add-page/sign-add-page.component';
import { SuccessPageComponent } from './components/app-components/success-page/success-page.component';




export const routes: Routes = [
  { path: 'signDocument/:urlLink', component: SigninComponent },
  {
    path: '',
    component: AdminLayoutComponent, canActivate: [AuthGuard],
    children: [
      { path: 'home', component: SignAddPageComponent },
    ]
  },
  { path: 'successPage', component: SuccessPageComponent }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
