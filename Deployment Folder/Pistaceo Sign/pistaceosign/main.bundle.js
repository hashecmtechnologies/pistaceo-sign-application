webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* based on angular-toastr css https://github.com/Foxandxss/angular-toastr/blob/cb508fe6801d6b288d3afc525bb40fee1b101650/dist/angular-toastr.css */\n.redstar {\n  color: red; }\n/* position */\n.toast-center-center {\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%); }\n.toast-top-center {\n  top: 0;\n  right: 0;\n  width: 100%; }\n.toast-bottom-center {\n  bottom: 0;\n  right: 0;\n  width: 100%; }\n.toast-top-full-width {\n  top: 0;\n  right: 0;\n  width: 100%; }\n.toast-bottom-full-width {\n  bottom: 0;\n  right: 0;\n  width: 100%; }\n.toast-top-left {\n  top: 12px;\n  left: 12px; }\n.toast-top-right {\n  top: 12px;\n  right: 12px; }\n.toast-bottom-right {\n  right: 12px;\n  bottom: 12px; }\n.toast-bottom-left {\n  bottom: 12px;\n  left: 12px; }\n/* toast styles */\n.toast-title {\n  font-weight: bold; }\n.toast-message {\n  word-wrap: break-word; }\n.toast-message a,\n.toast-message label {\n  color: #FFFFFF; }\n.toast-message a:hover {\n  color: #CCCCCC;\n  text-decoration: none; }\n.toast-close-button {\n  position: relative;\n  right: -0.3em;\n  top: -0.3em;\n  float: right;\n  font-size: 20px;\n  font-weight: bold;\n  color: #FFFFFF;\n  text-shadow: 0 1px 0 #ffffff;\n  /* opacity: 0.8; */ }\n.toast-close-button:hover,\n.toast-close-button:focus {\n  color: #000000;\n  text-decoration: none;\n  cursor: pointer;\n  opacity: 0.4; }\n/*Additional properties for button version\r\n   iOS requires the button element instead of an anchor tag.\r\n   If you want the anchor version, it requires `href=\"#\"`.*/\nbutton.toast-close-button {\n  padding: 0;\n  cursor: pointer;\n  background: transparent;\n  border: 0; }\n.toast-container {\n  pointer-events: none;\n  position: fixed;\n  z-index: 999999; }\n.toast-container * {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box; }\n.toast-container .toast {\n  position: relative;\n  overflow: hidden;\n  margin: 0 0 6px;\n  padding: 15px 15px 15px 50px;\n  width: 300px;\n  border-radius: 3px 3px 3px 3px;\n  background-position: 15px center;\n  background-repeat: no-repeat;\n  background-size: 24px;\n  -webkit-box-shadow: 0 0 12px #999999;\n          box-shadow: 0 0 12px #999999;\n  color: #FFFFFF; }\n.toast-container .toast:hover {\n  -webkit-box-shadow: 0 0 12px #000000;\n          box-shadow: 0 0 12px #000000;\n  opacity: 1;\n  cursor: pointer; }\n/* https://github.com/FortAwesome/Font-Awesome-Pro/blob/master/advanced-options/raw-svg/regular/info-circle.svg */\n.toast-info {\n  background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' width='512' height='512'%3E%3Cpath fill='rgb(255,255,255)' d='M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z'/%3E%3C/svg%3E\"); }\n/* https://github.com/FortAwesome/Font-Awesome-Pro/blob/master/advanced-options/raw-svg/regular/times-circle.svg */\n.toast-error {\n  background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' width='512' height='512'%3E%3Cpath fill='rgb(255,255,255)' d='M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z'/%3E%3C/svg%3E\"); }\n/* https://github.com/FortAwesome/Font-Awesome-Pro/blob/master/advanced-options/raw-svg/regular/check.svg */\n.toast-success {\n  background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' width='512' height='512'%3E%3Cpath fill='rgb(255,255,255)' d='M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z'/%3E%3C/svg%3E\"); }\n/* https://github.com/FortAwesome/Font-Awesome-Pro/blob/master/advanced-options/raw-svg/regular/exclamation-triangle.svg */\n.toast-warning {\n  background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 576 512' width='576' height='512'%3E%3Cpath fill='rgb(255,255,255)' d='M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z'/%3E%3C/svg%3E\"); }\n.toast-container.toast-top-center .toast,\n.toast-container.toast-bottom-center .toast {\n  width: 300px;\n  margin-left: auto;\n  margin-right: auto; }\n.toast-container.toast-top-full-width .toast,\n.toast-container.toast-bottom-full-width .toast {\n  width: 96%;\n  margin-left: auto;\n  margin-right: auto; }\n.toast {\n  background-color: #030303;\n  pointer-events: auto; }\n.toast-success {\n  background-color: #51A351; }\n.toast-error {\n  background-color: #BD362F; }\n.toast-info {\n  background-color: #2F96B4; }\n.toast-warning {\n  background-color: #F89406; }\n.toast-progress {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  height: 4px;\n  background-color: #000000;\n  opacity: 0.4; }\n/* Responsive Design */\n@media all and (max-width: 240px) {\n  .toast-container .toast.div {\n    padding: 8px 8px 8px 50px;\n    width: 11em; }\n  .toast-container .toast-close-button {\n    right: -0.2em;\n    top: -0.2em; } }\n@media all and (min-width: 241px) and (max-width: 480px) {\n  .toast-container .toast.div {\n    padding: 8px 8px 8px 50px;\n    width: 18em; }\n  .toast-container .toast-close-button {\n    right: -0.2em;\n    top: -0.2em; } }\n@media all and (min-width: 481px) and (max-width: 768px) {\n  .toast-container .toast.div {\n    padding: 15px 15px 15px 50px;\n    width: 25em; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/@ngx-translate/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(renderer, translate) {
        this.renderer = renderer;
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"], __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_routes__ = __webpack_require__("../../../../../src/app/app.routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/@ngx-translate/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ngx_translate_http_loader__ = __webpack_require__("../../../../@ngx-translate/http-loader/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_app_components_signin_signin_component__ = __webpack_require__("../../../../../src/app/components/app-components/signin/signin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__service_user_service__ = __webpack_require__("../../../../../src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__service_content_service__ = __webpack_require__("../../../../../src/app/service/content.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__service_document_service__ = __webpack_require__("../../../../../src/app/service/document.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__service_schema_service__ = __webpack_require__("../../../../../src/app/service/schema.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__service_work_service__ = __webpack_require__("../../../../../src/app/service/work.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__service_interceptor_service__ = __webpack_require__("../../../../../src/app/service/interceptor.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_app_components_layout_footer_footer_component__ = __webpack_require__("../../../../../src/app/components/app-components/layout/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_app_components_layout_topbar_topbar_component__ = __webpack_require__("../../../../../src/app/components/app-components/layout/topbar/topbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_app_components_layout_breadcrumb_breadcrumb_component__ = __webpack_require__("../../../../../src/app/components/app-components/layout/breadcrumb/breadcrumb.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_app_components_layout_breadcrumb_breadcrumb_service__ = __webpack_require__("../../../../../src/app/components/app-components/layout/breadcrumb/breadcrumb.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_app_components_layout_menu_items_menu_items_component__ = __webpack_require__("../../../../../src/app/components/app-components/layout/menu-items/menu-items.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__components_generic_components_signature_pad_signature_pad_component__ = __webpack_require__("../../../../../src/app/components/generic-components/signature-pad/signature-pad.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__components_app_components_annotation_annotation_component__ = __webpack_require__("../../../../../src/app/components/app-components/annotation/annotation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__components_generic_components_document_annotation_document_annotation_component__ = __webpack_require__("../../../../../src/app/components/generic-components/document-annotation/document-annotation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29_ngx_toastr__ = __webpack_require__("../../../../ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30_angular2_draggable__ = __webpack_require__("../../../../angular2-draggable/angular2-draggable.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31_ngx_smart_modal__ = __webpack_require__("../../../../ngx-smart-modal/esm5/ngx-smart-modal.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_32_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__service_auth_guard__ = __webpack_require__("../../../../../src/app/service/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__components_app_components_sign_add_page_sign_add_page_component__ = __webpack_require__("../../../../../src/app/components/app-components/sign-add-page/sign-add-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__components_app_components_layout_admin_layout_admin_layout_component__ = __webpack_require__("../../../../../src/app/components/app-components/layout/admin-layout/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__service_stroe_service__ = __webpack_require__("../../../../../src/app/service/stroe.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__components_app_components_success_page_success_page_component__ = __webpack_require__("../../../../../src/app/components/app-components/success-page/success-page.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





































































































function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_13__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_6__app_routes__["a" /* AppRoutes */],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["AccordionModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["AutoCompleteModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["BreadcrumbModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["ButtonModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["CalendarModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["CarouselModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["ChartModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["CheckboxModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["ChipsModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["CodeHighlighterModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["ConfirmDialogModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["ColorPickerModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["SharedModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["ContextMenuModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["DataGridModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["DataListModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["DataScrollerModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["DataTableModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["DialogModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["DragDropModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["DropdownModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["EditorModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["FieldsetModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["GalleriaModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["GMapModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["GrowlModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["InputMaskModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["InputSwitchModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["InputTextModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["InputTextareaModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["LightboxModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["ListboxModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["MegaMenuModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["MenuModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["MenubarModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["MessagesModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["MultiSelectModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["OrderListModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["OrganizationChartModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["OverlayPanelModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["PaginatorModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["PanelModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["PanelMenuModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["PasswordModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["PickListModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["ProgressBarModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["RadioButtonModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["RatingModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["ScheduleModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["SelectButtonModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["SlideMenuModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["SliderModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["SpinnerModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["SplitButtonModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["StepsModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["TabMenuModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["TabViewModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["TerminalModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["TieredMenuModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["ToggleButtonModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["ToolbarModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["TooltipModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["TreeModule"],
                __WEBPACK_IMPORTED_MODULE_8_primeng_primeng__["TreeTableModule"],
                __WEBPACK_IMPORTED_MODULE_12__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_11__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_11__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_12__angular_common_http__["a" /* HttpClient */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_29_ngx_toastr__["a" /* ToastrModule */].forRoot({
                    timeOut: 5000,
                    positionClass: 'toast-bottom-right',
                    preventDuplicates: true
                }),
                __WEBPACK_IMPORTED_MODULE_30_angular2_draggable__["a" /* AngularDraggableModule */],
                __WEBPACK_IMPORTED_MODULE_31_ngx_smart_modal__["a" /* NgxSmartModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_32_ng4_loading_spinner__["Ng4LoadingSpinnerModule"].forRoot(),
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_14__components_app_components_signin_signin_component__["a" /* SigninComponent */],
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_35__components_app_components_layout_admin_layout_admin_layout_component__["a" /* AdminLayoutComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_app_components_layout_footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_22__components_app_components_layout_topbar_topbar_component__["a" /* TopbarComponent */],
                __WEBPACK_IMPORTED_MODULE_23__components_app_components_layout_breadcrumb_breadcrumb_component__["a" /* BreadcrumbComponent */],
                __WEBPACK_IMPORTED_MODULE_25__components_app_components_layout_menu_items_menu_items_component__["a" /* MenuItemsComponent */],
                __WEBPACK_IMPORTED_MODULE_25__components_app_components_layout_menu_items_menu_items_component__["b" /* SubMenuMenuItemsComponent */],
                __WEBPACK_IMPORTED_MODULE_26__components_generic_components_signature_pad_signature_pad_component__["a" /* SignatureComponent */],
                __WEBPACK_IMPORTED_MODULE_27__components_app_components_annotation_annotation_component__["a" /* AnnotationComponent */],
                __WEBPACK_IMPORTED_MODULE_28__components_generic_components_document_annotation_document_annotation_component__["a" /* DocumentAnnotationComponent */],
                __WEBPACK_IMPORTED_MODULE_34__components_app_components_sign_add_page_sign_add_page_component__["a" /* SignAddPageComponent */],
                __WEBPACK_IMPORTED_MODULE_37__components_app_components_success_page_success_page_component__["a" /* SuccessPageComponent */],
            ],
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_5__angular_common__["LocationStrategy"], useClass: __WEBPACK_IMPORTED_MODULE_5__angular_common__["HashLocationStrategy"] },
                __WEBPACK_IMPORTED_MODULE_15__service_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_24__components_app_components_layout_breadcrumb_breadcrumb_service__["a" /* BreadcrumbService */],
                __WEBPACK_IMPORTED_MODULE_16__service_content_service__["a" /* ContentService */],
                __WEBPACK_IMPORTED_MODULE_17__service_document_service__["a" /* DocumentService */],
                __WEBPACK_IMPORTED_MODULE_18__service_schema_service__["a" /* SchemaService */],
                __WEBPACK_IMPORTED_MODULE_19__service_work_service__["a" /* WorkService */],
                __WEBPACK_IMPORTED_MODULE_20__service_interceptor_service__["a" /* HttpInterceptor */],
                __WEBPACK_IMPORTED_MODULE_33__service_auth_guard__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_36__service_stroe_service__["a" /* StoreService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_app_components_signin_signin_component__ = __webpack_require__("../../../../../src/app/components/app-components/signin/signin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_app_components_layout_admin_layout_admin_layout_component__ = __webpack_require__("../../../../../src/app/components/app-components/layout/admin-layout/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_auth_guard__ = __webpack_require__("../../../../../src/app/service/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_app_components_sign_add_page_sign_add_page_component__ = __webpack_require__("../../../../../src/app/components/app-components/sign-add-page/sign-add-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_app_components_success_page_success_page_component__ = __webpack_require__("../../../../../src/app/components/app-components/success-page/success-page.component.ts");






var routes = [
    { path: 'signDocument/:urlLink', component: __WEBPACK_IMPORTED_MODULE_1__components_app_components_signin_signin_component__["a" /* SigninComponent */] },
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__components_app_components_layout_admin_layout_admin_layout_component__["a" /* AdminLayoutComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_3__service_auth_guard__["a" /* AuthGuard */]],
        children: [
            { path: 'home', component: __WEBPACK_IMPORTED_MODULE_4__components_app_components_sign_add_page_sign_add_page_component__["a" /* SignAddPageComponent */] },
        ]
    },
    { path: 'successPage', component: __WEBPACK_IMPORTED_MODULE_5__components_app_components_success_page_success_page_component__["a" /* SuccessPageComponent */] }
];
var AppRoutes = __WEBPACK_IMPORTED_MODULE_0__angular_router__["RouterModule"].forRoot(routes);


/***/ }),

/***/ "../../../../../src/app/components/app-components/annotation/annotation.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/app-components/annotation/annotation.component.html":
/***/ (function(module, exports) {

module.exports = "<app-document-annotation [docId]=\"docId\" [documentType]=\"documentType\" [currentPage]=\"currentPage\" [annotationType]=\"annotationType\">\r\n</app-document-annotation>"

/***/ }),

/***/ "../../../../../src/app/components/app-components/annotation/annotation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnnotationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__layout_breadcrumb_breadcrumb_service__ = __webpack_require__("../../../../../src/app/components/app-components/layout/breadcrumb/breadcrumb.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/@ngx-translate/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AnnotationComponent = (function () {
    function AnnotationComponent(breadcrumbService, translate, route) {
        var _this = this;
        this.breadcrumbService = breadcrumbService;
        this.translate = translate;
        this.route = route;
        if (window.location.href.includes('draft')) {
            this.breadcrumbService.setItems([
                { label: this.translate.instant('Draft') },
                { label: this.translate.instant('Annotation') }
            ]);
        }
        else if (window.location.href.includes('inbox')) {
            this.breadcrumbService.setItems([
                { label: this.translate.instant('Inbox') },
                { label: this.translate.instant('Annotation') }
            ]);
        }
        else if (window.location.href.includes('sent')) {
            this.breadcrumbService.setItems([
                { label: this.translate.instant('Sent') },
                { label: this.translate.instant('Annotation') }
            ]);
        }
        else if (window.location.href.includes('archive')) {
            this.breadcrumbService.setItems([
                { label: this.translate.instant('Archive') },
                { label: this.translate.instant('Annotation') }
            ]);
        }
        else {
            this.breadcrumbService.setItems([
                { label: this.translate.instant('Document') },
                { label: this.translate.instant('Annotation') }
            ]);
        }
        this.route.queryParams.subscribe(function (p) { _this.ngOnInit(); });
    }
    AnnotationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.docId = params['docId'];
            _this.documentType = params['documentType'];
            _this.currentPage = params['currentPage'];
            _this.annotationType = params['annotationType'];
        });
    };
    AnnotationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-annotation',
            template: __webpack_require__("../../../../../src/app/components/app-components/annotation/annotation.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/app-components/annotation/annotation.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__layout_breadcrumb_breadcrumb_service__["a" /* BreadcrumbService */], __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["ActivatedRoute"]])
    ], AnnotationComponent);
    return AnnotationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/admin-layout/admin-layout.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".displayTrue {\r\n    display: block;\r\n}\r\n\r\n.displayFalse {\r\n    display: none;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/admin-layout/admin-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<div (click)=\"onWrapperClick()\">\n    <div class=\"layout-main\">\n        <div class=\"layout-content\">\n            <router-outlet></router-outlet>\n        </div>\n        <div class=\"layout-main-mask\" *ngIf=\"mobileMenuActive\"></div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/admin-layout/admin-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/@ngx-translate/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_document_service__ = __webpack_require__("../../../../../src/app/service/document.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_stroe_service__ = __webpack_require__("../../../../../src/app/service/stroe.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AdminLayoutComponent = (function () {
    function AdminLayoutComponent(renderer, translate, location, router, ds, store) {
        var _this = this;
        this.renderer = renderer;
        this.ds = ds;
        this.store = store;
        this.layoutStatic = true;
        this.options = [];
        this.show = false;
        this.signIn = false;
        if (localStorage.getItem('Default Language') === 'ar') {
            this.dirType = 'rtl';
        }
        else {
            this.dirType = 'ltr';
        }
        if (localStorage.getItem('Default Language') === 'ar') {
            this.isRTL = true;
        }
        else {
            this.isRTL = false;
        }
        var value = localStorage.getItem('Default Language');
        translate.setDefaultLang(value);
        translate.use(localStorage.getItem('Default Language'));
        // }
        if (localStorage.getItem('Default Theme') === undefined || localStorage.getItem('Default Theme') === null) {
            var themeLink = document.getElementById('theme-css');
            themeLink.href = 'assets/theme/theme-' + 'bluegrey' + '.css';
        }
        else {
            var theme = localStorage.getItem('Default Theme');
            var themeLink = document.getElementById('theme-css');
            themeLink.href = 'assets/theme/theme-' + theme + '.css';
            if (theme === 'bluegrey') {
                this.changedLayout('moody');
            }
            else if (theme === 'indigo') {
                this.changedLayout('reflection');
            }
            else if (theme === 'pink') {
                this.changedLayout('cityscape');
            }
            else if (theme === 'purple') {
                this.changedLayout('cloudy');
            }
            else if (theme === 'deeppurple') {
                this.changedLayout('storm');
            }
            else if (theme === 'blue') {
                this.changedLayout('palm');
            }
            else if (theme === 'eezib') {
                this.changedLayout('eezib');
            }
            else {
                this.changedLayout('flatiron');
            }
        }
        router.events.subscribe(function (val) {
            if (location.isCurrentPathEqualTo('/home')) {
                _this.show = true;
            }
            else {
                _this.show = true;
            }
        });
    }
    AdminLayoutComponent.prototype.ngOnChanges = function () {
        var _this = this;
        if (this.store.signIn === true) {
            this.ds.getPagesForSignature(this.docid, this.roleid).subscribe(function (data) {
                _this.sendDataToMenuItems(data);
            }, function (error) { });
        }
    };
    AdminLayoutComponent.prototype.sendDataToMenuItems = function (data) {
        this.itemsToSend = [];
        this.itemsToSend = data._body;
    };
    AdminLayoutComponent.prototype.changedLayout = function (layout) {
        var layoutLink = document.getElementById('layout-css');
        layoutLink.href = 'assets/layout/css/layout-' + layout + '.css';
    };
    AdminLayoutComponent.prototype.onWrapperClick = function () {
        if (!this.menuClick && !this.menuButtonClick) {
            this.mobileMenuActive = false;
        }
        if (!this.topbarMenuClick && !this.topbarMenuButtonClick) {
            this.topbarMenuActive = false;
            this.activeTopbarItem = null;
        }
        this.menuClick = false;
        this.menuButtonClick = false;
        this.topbarMenuClick = false;
        this.topbarMenuButtonClick = false;
    };
    AdminLayoutComponent.prototype.onMenuButtonClick = function (event) {
        this.menuButtonClick = true;
        if (this.isMobile()) {
            this.mobileMenuActive = !this.mobileMenuActive;
        }
        event.preventDefault();
    };
    AdminLayoutComponent.prototype.onTopbarMobileMenuButtonClick = function (event) {
        this.topbarMenuButtonClick = true;
        this.topbarMenuActive = !this.topbarMenuActive;
        event.preventDefault();
    };
    AdminLayoutComponent.prototype.onTopbarRootItemClick = function (event, item) {
        if (this.activeTopbarItem === item) {
            this.activeTopbarItem = null;
        }
        else {
            this.activeTopbarItem = item;
        }
        event.preventDefault();
    };
    AdminLayoutComponent.prototype.onTopbarMenuClick = function (event) {
        this.topbarMenuClick = true;
    };
    AdminLayoutComponent.prototype.onSidebarClick = function (event) {
        this.menuClick = true;
    };
    AdminLayoutComponent.prototype.onToggleMenuClick = function (event) {
        this.layoutStatic = !this.layoutStatic;
    };
    AdminLayoutComponent.prototype.isMobile = function () {
        return window.innerWidth <= 1024;
    };
    AdminLayoutComponent.prototype.rtlChange = function (event) {
        if (event === 'ar') {
            this.dirType = 'rtl';
        }
        else {
            this.dirType = 'ltr';
        }
    };
    AdminLayoutComponent.prototype.updateValue = function (type, count) {
    };
    AdminLayoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-layout',
            template: __webpack_require__("../../../../../src/app/components/app-components/layout/admin-layout/admin-layout.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/app-components/layout/admin-layout/admin-layout.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"], __WEBPACK_IMPORTED_MODULE_1__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_3__angular_common__["Location"], __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_4__service_document_service__["a" /* DocumentService */], __WEBPACK_IMPORTED_MODULE_5__service_stroe_service__["a" /* StoreService */]])
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/breadcrumb/breadcrumb.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"layout-breadcrumb\">\n    <ul>\n        <li><a routerLink=\"/home\"><i class=\"material-icons\">home</i></a></li>\n        <li>/</li>\n        <ng-template ngFor let-item let-last=\"last\" [ngForOf]=\"items\">\n            <li>\n                <a [routerLink]=\"item.routerLink\" *ngIf=\"item.routerLink\">{{ item.label | translate}}</a>\n                <ng-container *ngIf=\"!item.routerLink\">{{item.label | translate}}</ng-container>\n            </li>\n            <li *ngIf=\"!last\">/</li>\n        </ng-template>\n    </ul>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/breadcrumb/breadcrumb.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BreadcrumbComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__breadcrumb_service__ = __webpack_require__("../../../../../src/app/components/app-components/layout/breadcrumb/breadcrumb.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BreadcrumbComponent = (function () {
    function BreadcrumbComponent(breadcrumbService) {
        var _this = this;
        this.breadcrumbService = breadcrumbService;
        this.showBack = false;
        this.subscription = breadcrumbService.itemsHandler.subscribe(function (response) {
            _this.items = response;
        });
    }
    BreadcrumbComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    BreadcrumbComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-breadcrumb',
            template: __webpack_require__("../../../../../src/app/components/app-components/layout/breadcrumb/breadcrumb.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__breadcrumb_service__["a" /* BreadcrumbService */]])
    ], BreadcrumbComponent);
    return BreadcrumbComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/breadcrumb/breadcrumb.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BreadcrumbService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var BreadcrumbService = (function () {
    function BreadcrumbService() {
        this.itemsSource = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.itemsHandler = this.itemsSource.asObservable();
    }
    BreadcrumbService.prototype.setItems = function (items) {
        this.breadcrumbName = items[0].label;
        this.itemsSource.next(items);
    };
    BreadcrumbService.prototype.getItem = function () {
        return this.breadcrumbName;
    };
    BreadcrumbService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], BreadcrumbService);
    return BreadcrumbService;
}());



/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"layout-footer clearfix\">\n    <span class=\"footer-text-right\">\n        <span>Copyright &copy;HashECM 2018</span>\n    </span>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/components/app-components/layout/footer/footer.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/menu-items/menu-items.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".footerValue {\r\n    color: #fff !important;\r\n    margin-left: 3.3%;\r\n}\r\n\r\n@media only screen and (max-width: 600px) {\r\n    .buttonStrip {\r\n        display: none !important;\r\n    }\r\n}\r\n\r\n.buttonStrip {\r\n    position: fixed;\r\n    z-index: 1001;\r\n    bottom: 0;\r\n    width: 100%;\r\n    margin: 0;\r\n    padding: 0;\r\n}\r\n\r\n.app-name {\r\n    font-size: 21px !important;\r\n}\r\n\r\n.clickPage {\r\n    color: #fff !important;\r\n    font-size: 20px;\r\n    margin-bottom: 15px;\r\n    margin-top: 22px;\r\n}\r\n\r\n.pageNor {\r\n    text-align: center;\r\n    background: #4C6887;\r\n    padding: 10px;\r\n    margin-bottom: 12px;\r\n    border-radius: 5px;\r\n    margin-left: 15px;\r\n    width: 90%;\r\n    color: #fff;\r\n    font-weight: bold;\r\n    cursor: pointer;\r\n}\r\n\r\n.subBut {\r\n    text-align: center;\r\n    margin-top: 10%;\r\n}\r\n\r\n.green {\r\n    font-size: 20px;\r\n    padding: 0;\r\n    float: left;\r\n    font-weight: bold;\r\n    color: #fff;\r\n}\r\n\r\n.disabled {\r\n    opacity: 0.6;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/menu-items/menu-items.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"layout-sidebar\" [ngClass]=\"{'layout-sidebar-active': app.sidebarActive, 'layout-sidebar-dark': app.darkMenu}\" (click)=\"app.onSidebarClick($event)\" (mouseover)=\"app.sidebarActive=true\" (mouseleave)=\"app.sidebarActive=false\">\n    <div class=\"sidebar-logo center\">\n        <a *ngIf=\"previewLogo !== ''\">\n            <img alt=\"logo\" [src]=\"previewLogo\" style=\"width: 153px;height: 40px; padding-top: 5px;\" />\n        </a>\n        <a *ngIf=\"previewLogo === ''\">\n            <img alt=\"logo\" src=\"assets/images/logos/app-logo.png\" style=\"width: 153px;height: 40px;padding-top: 3px; padding-left: 7px\" />\n        </a>\n        <div class=\"ui-g-12 clickPage\">\n            <label> {{'Click page to sign' | translate }}</label>\n        </div>\n    </div>\n\n    <div #layoutMenuScroller class=\"nano\">\n        <div class=\"nano-content sidebar-scroll-content\" style=\"margin-right: -10px;max-height: 600px; overflow-y: auto\" *ngIf=\"model\">\n            <div class=\"ui-g-12 ui-g-nopad\" *ngFor=\"let value of model\">\n                <div class=\"ui-g-12 ui-g-nopad pageNor\" (click)=\"clicked(value)\" *ngIf=\"value.status !== 'SIGNED' && !disblePage\">\n                    <span>Page  {{value.pageno}}</span>\n                </div>\n                <div class=\"ui-g-12 ui-g-nopad pageNor disabled\" *ngIf=\"disblePage\">\n                    <span>Page {{value.pageno}}</span>\n                </div>\n                <div class=\"ui-g-12 ui-g-nopad pageNor disabled\" *ngIf=\"value.status === 'SIGNED'\">\n                    <span><i class=\"fa fa-check green\"></i> &nbsp; Page {{value.pageno}}</span>\n                </div>\n            </div>\n            <div class=\"ui-g-12 ui-g-nopad subBut\">\n                <button type=\"button\" pButton label=\"Submit\" icon=\"ui-icon-save\" (click)=\"submited()\" class=\"ui-button-success\" [disabled]=\"disableSubmitbut\"></button>\n            </div>\n            <a class=\"nav-link buttonStrip\" style=\"font-size: 10px; text-align: right; left: 0\">\n                <span (click)=\"officialWebsite()\" class=\"ff-headers footerValue pointer\">{{'Copyright' | translate}} &copy; {{'HashECM 2018-20' | translate}}</span>\n            </a>\n        </div>\n    </div>"

/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/menu-items/menu-items.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuItemsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SubMenuMenuItemsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_animations__ = __webpack_require__("../../../animations/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/@ngx-translate/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__admin_layout_admin_layout_component__ = __webpack_require__("../../../../../src/app/components/app-components/layout/admin-layout/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_schema_service__ = __webpack_require__("../../../../../src/app/service/schema.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_content_service__ = __webpack_require__("../../../../../src/app/service/content.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_work_service__ = __webpack_require__("../../../../../src/app/service/work.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__service_user_service__ = __webpack_require__("../../../../../src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__service_document_service__ = __webpack_require__("../../../../../src/app/service/document.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var MenuItemsComponent = (function () {
    function MenuItemsComponent(app, translate, cs, ss, router, ws, us, ds) {
        this.app = app;
        this.translate = translate;
        this.cs = cs;
        this.ss = ss;
        this.router = router;
        this.ws = ws;
        this.us = us;
        this.ds = ds;
        this.model = [];
        this.pageChanged = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.submitted = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.showDrafts = false;
        this.showArchive = false;
        this.showRegister = false;
        this.inboxCount = 0;
        this.draftsCount = 0;
        this.disableSubmitbut = false;
        // this.ss.getWorkTypes().subscribe(data => { this.dynamicItems(data); });
        var browserLang = translate.getBrowserLang();
        translate.use('en');
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
    MenuItemsComponent.prototype.ngOnInit = function () {
        this.previewLogo = '';
        this.model = [];
    };
    MenuItemsComponent.prototype.getPreviewImage = function (data) {
        if (data._body.image !== undefined) {
            var previewImage = JSON.parse(data._body);
            this.previewLogo = 'data:image/png;base64,' + previewImage.image;
        }
        else {
            this.previewLogo = '';
        }
    };
    MenuItemsComponent.prototype.dynamicItems = function (data) {
        var _this = this;
        var datares = JSON.parse(data._body);
        for (var i = 0; i < this.model.length; i++) {
            if (this.model[i].label === 'Create') {
                if (datares.length <= 5) {
                    var _loop_1 = function (k) {
                        var items = {
                            label: datares[k].name,
                            icon: 'edit',
                            id: datares[k].id,
                            multiFormSupport: datares[k].multiFormSupport,
                            subjectProps: datares[k].subjectProps,
                            initActivityType: datares[k].initActivityType,
                            command: function (event) { _this.CreatePage(datares[k].id, datares[k].initActivityType, datares[k].multiFormSupport, datares[k].subjectProps); }
                        };
                        this_1.model[i].items.push(items);
                        if (datares[k].draftSupport === 1) {
                            this_1.showDrafts = true;
                        }
                        if (datares[k].archiveSupport === 1) {
                            this_1.showArchive = true;
                        }
                        if (datares[k].registerSupport === 1) {
                            this_1.showRegister = true;
                        }
                    };
                    var this_1 = this;
                    for (var k = 0; k < datares.length; k++) {
                        _loop_1(k);
                    }
                }
                else {
                    for (var k = 0; k < datares.length; k++) {
                        if (datares[k].draftSupport === 1) {
                            this.showDrafts = true;
                        }
                        if (datares[k].archiveSupport === 1) {
                            this.showArchive = true;
                        }
                        if (datares[k].registerSupport === 1) {
                            this.showRegister = true;
                        }
                    }
                    this.model[i].items = null;
                    this.model[i].routerLink = ['/createFormNames'];
                }
            }
        }
        for (var reg = 0; reg < this.model.length; reg++) {
            if (this.model[reg].label === 'Archive' && this.showArchive === false) {
                break;
            }
        }
        for (var reg = 0; reg < this.model.length; reg++) {
            if (this.model[reg].label === 'Drafts' && this.showDrafts === false) {
                this.model.splice(reg, 1);
                break;
            }
        }
        if (this.showRegister === true) {
            this.ss.getRegisterWorkTypes().subscribe(function (dataresp) { return _this.workCategories(dataresp); });
        }
        else {
            for (var reg = 0; reg < this.model.length; reg++) {
                if (this.model[reg].label === 'Registers') {
                    this.model.splice(reg, 1);
                    break;
                }
            }
        }
    };
    MenuItemsComponent.prototype.DataItems = function (data) {
        var _this = this;
        var datares = JSON.parse(data._body);
        for (var i = 0; i < this.model.length; i++) {
            if (this.model[i].label === 'Data') {
                if (datares && datares.length > 0) {
                    var _loop_2 = function (k) {
                        var items = {
                            label: datares[k].label,
                            icon: 'room_service',
                            name: datares[k].name,
                            formId: datares[k].formId,
                            id: datares[k].id,
                            status: datares[k].status,
                            table: datares[k].table,
                            command: function (event) { _this.dataPage(datares[k].id, datares[k].formId, datares[k].table, datares[k].name, datares[k].label, datares[k].status, datares[k].keyName); }
                        };
                        this_2.model[i].items.push(items);
                    };
                    var this_2 = this;
                    for (var k = 0; k < datares.length; k++) {
                        _loop_2(k);
                    }
                }
                else {
                    this.model.splice(i, 1);
                }
            }
        }
    };
    MenuItemsComponent.prototype.dataPage = function (id, formId, table, name, label, status, keyName) {
        this.router.navigate(['/data'], { queryParams: { 'id': id, 'formId': formId, 'table': table, 'name': name, 'label': label, 'status': status, 'keyName': keyName } });
    };
    MenuItemsComponent.prototype.workCategories = function (data) {
        var _this = this;
        var datares = [];
        datares = JSON.parse(data._body);
        for (var rek = 0; rek < this.model.length; rek++) {
            if (this.model[rek].label === 'Registers') {
                var _loop_3 = function (reg) {
                    var value = {
                        label: this_3.translate.instant(datares[reg].name),
                        icon: 'edit',
                        id: datares[reg].id,
                        command: function (event) { _this.registerPage(datares[reg].id, datares[reg].name); }
                    };
                    this_3.model[rek].items.push(value);
                };
                var this_3 = this;
                for (var reg = 0; reg < datares.length; reg++) {
                    _loop_3(reg);
                }
            }
        }
    };
    MenuItemsComponent.prototype.registerPage = function (id, name) {
        this.router.navigate(['/register'], { queryParams: { id: id, name: name } });
    };
    MenuItemsComponent.prototype.CreatePage = function (id, initActivityType, multiFormSupport, subjectProps) {
        this.router.navigate(['/create'], { queryParams: { 'id': initActivityType, 'typeId': id, 'multtiFromSupport': multiFormSupport, 'subjectProps': subjectProps } });
    };
    MenuItemsComponent.prototype.updateRepositoryValue = function (data) {
        var _this = this;
        var resp = JSON.parse(data._body);
        for (var index = 0; index < this.model.length; index++) {
            if (this.model[index].icon === 'folder') {
                this.model[index].items = [];
            }
        }
        for (var index = 0; index < this.model.length; index++) {
            if (this.model[index].icon === 'folder') {
                this.model[index].items.push({
                    label: resp.name, icon: 'folder',
                    command: function (event) { _this.folderClicked(resp); }
                });
            }
        }
    };
    MenuItemsComponent.prototype.folderClicked = function (repo) {
        this.router.navigate(['/folderview'], { queryParams: { 'repoName': repo.name, 'repoId': repo.id } });
    };
    MenuItemsComponent.prototype.browseDocs = function (data) {
        var _this = this;
        if (data._body !== '') {
            var browseDocs = JSON.parse(data._body);
            if (browseDocs.value === 'YES') {
                this.cs.getAppRepository().subscribe(function (res) { return _this.updateRepositoryValue(res); });
            }
            else {
                for (var reg = 0; reg < this.model.length; reg++) {
                    if (this.model[reg].label === 'Documents') {
                        this.model.splice(reg, 1);
                        break;
                    }
                }
            }
        }
    };
    MenuItemsComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.layoutMenuScroller = this.layoutMenuScrollerViewChild.nativeElement;
        setTimeout(function () {
            jQuery(_this.layoutMenuScroller).nanoScroller({ flash: true });
        }, 10);
    };
    MenuItemsComponent.prototype.changeTheme = function (theme) {
        var themeLink = document.getElementById('theme-css');
        themeLink.href = 'assets/theme/theme-' + theme + '.css';
    };
    MenuItemsComponent.prototype.changeLayout = function (theme) {
        var layoutLink = document.getElementById('layout-css');
        layoutLink.href = 'assets/layout/css/layout-' + theme + '.css';
    };
    MenuItemsComponent.prototype.updateNanoScroll = function () {
        var _this = this;
        setTimeout(function () {
            jQuery(_this.layoutMenuScroller).nanoScroller();
        }, 500);
    };
    MenuItemsComponent.prototype.itemColor = function (model) {
        console.log(model);
    };
    MenuItemsComponent.prototype.ngOnDestroy = function () {
        jQuery(this.layoutMenuScroller).nanoScroller({ flash: true });
    };
    MenuItemsComponent.prototype.ngOnChanges = function () {
        if (this.sideMenuItems) {
            this.model = this.sideMenuItems;
            for (var i = 0; i < this.model.length; i++) {
                if (this.model[i].status === 'SIGNED') {
                    this.disableSubmitbut = false;
                }
                else {
                    this.disableSubmitbut = true;
                    break;
                }
            }
            if (this.model.length === 0) {
                this.disableSubmitbut = false;
            }
        }
        else {
            this.disableSubmitbut = false;
        }
    };
    MenuItemsComponent.prototype.submited = function () {
        this.submitted.emit('yes');
    };
    MenuItemsComponent.prototype.clicked = function (items) {
        this.pageChanged.emit(items);
    };
    MenuItemsComponent.prototype.officialWebsite = function () {
        window.open('http://www.hashecm.com/', 'mywindow', 'status=1,toolbar=1');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], MenuItemsComponent.prototype, "reset", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], MenuItemsComponent.prototype, "type", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], MenuItemsComponent.prototype, "sideMenuItems", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], MenuItemsComponent.prototype, "pageChanged", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], MenuItemsComponent.prototype, "submitted", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], MenuItemsComponent.prototype, "disableSubmit", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], MenuItemsComponent.prototype, "disblePage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('layoutMenuScroller'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], MenuItemsComponent.prototype, "layoutMenuScrollerViewChild", void 0);
    MenuItemsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-menu-items',
            template: __webpack_require__("../../../../../src/app/components/app-components/layout/menu-items/menu-items.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/app-components/layout/menu-items/menu-items.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__admin_layout_admin_layout_component__["a" /* AdminLayoutComponent */], __WEBPACK_IMPORTED_MODULE_4__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_7__service_content_service__["a" /* ContentService */],
            __WEBPACK_IMPORTED_MODULE_6__service_schema_service__["a" /* SchemaService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_8__service_work_service__["a" /* WorkService */], __WEBPACK_IMPORTED_MODULE_9__service_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_10__service_document_service__["a" /* DocumentService */]])
    ], MenuItemsComponent);
    return MenuItemsComponent;
}());

var SubMenuMenuItemsComponent = (function () {
    function SubMenuMenuItemsComponent(app, router, location) {
        this.app = app;
        this.router = router;
        this.location = location;
    }
    SubMenuMenuItemsComponent.prototype.itemClick = function (event, item, index) {
        // avoid processing disabled items
        if (item.disabled) {
            event.preventDefault();
            return true;
        }
        // activate current item and deactivate active sibling if any
        if (item.routerLink || item.items || item.command || item.url) {
            this.activeIndex = (this.activeIndex === index) ? null : index;
        }
        // execute command
        if (item.command) {
            item.command({ originalEvent: event, item: item });
        }
        // prevent hash change
        if (item.items || (!item.url && !item.routerLink)) {
            event.preventDefault();
        }
        // hide menu
        if (!item.items) {
            if (this.app.isMobile()) {
                this.app.sidebarActive = false;
                this.app.mobileMenuActive = false;
            }
        }
    };
    SubMenuMenuItemsComponent.prototype.isActive = function (index) {
        // if (this.activeIndex !== undefined) {
        return this.activeIndex === index;
        // }
    };
    Object.defineProperty(SubMenuMenuItemsComponent.prototype, "reset", {
        get: function () {
            return this._reset;
        },
        set: function (val) {
            this._reset = val;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], SubMenuMenuItemsComponent.prototype, "item", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], SubMenuMenuItemsComponent.prototype, "root", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], SubMenuMenuItemsComponent.prototype, "visible", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], SubMenuMenuItemsComponent.prototype, "reset", null);
    SubMenuMenuItemsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            /* tslint:disable:component-selector */
            selector: '[app-submenu]',
            /* tslint:enable:component-selector */
            template: "\n        <ng-template ngFor let-child let-i=\"index\" [ngForOf]=\"(root ? item : item.items)\">\n            <li [ngClass]=\"{'active-menuitem': isActive(i)}\" [class]=\"child.badgeStyleClass\">\n                <a [href]=\"child.url||'#'\" (click)=\"itemClick($event,child,i);\" *ngIf=\"!child.routerLink\"\n                   [attr.tabindex]=\"!visible ? '-1' : null\" [attr.target]=\"child.target\"\n                    (mouseenter)=\"hover=true\" (mouseleave)=\"hover=false\" class=\"ripplelink\">\n                    <i class=\"material-icons\">{{child.icon}}</i>\n                    <span class=\"menuitem-text\">{{ child.label | translate }}</span>\n                    <i class=\"material-icons layout-submenu-toggler\" *ngIf=\"child.items\">keyboard_arrow_down</i>\n                    <span class=\"menuitem-badge\" *ngIf=\"child.badge\">{{child.badge}}</span>\n                </a>\n\n                <a (click)=\"itemClick($event,child,i);\" *ngIf=\"child.routerLink\"\n                    [routerLink]=\"child.routerLink\" routerLinkActive=\"active-menuitem-routerlink\"\n                   [routerLinkActiveOptions]=\"{exact: true}\" [attr.tabindex]=\"!visible ? '-1' : null\" [attr.target]=\"child.target\"\n                    (mouseenter)=\"hover=true\" (mouseleave)=\"hover=false\" class=\"ripplelink\">\n                    <i class=\"material-icons\">{{child.icon}}</i>\n                    <span class=\"menuitem-text\">{{ child.label | translate }}</span>\n                    <i class=\"material-icons layout-submenu-toggler\" *ngIf=\"child.items\">>keyboard_arrow_down</i>\n                    <span class=\"menuitem-badge\" *ngIf=\"child.badge\">{{child.badge}}</span>\n                </a>\n                <ul app-submenu [item]=\"child\" *ngIf=\"child.items\" [visible]=\"isActive(i)\" [reset]=\"reset\"\n                    [@children]=\"isActive(i) ? 'visible' : 'hidden'\"></ul>\n            </li>\n        </ng-template>\n    ",
            animations: [
                Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["trigger"])('children', [
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["state"])('visible', Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                        height: '*'
                    })),
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["state"])('hidden', Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["style"])({
                        height: '0px'
                    })),
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('visible => hidden', Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["transition"])('hidden => visible', Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["animate"])('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
                ])
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__admin_layout_admin_layout_component__["a" /* AdminLayoutComponent */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__angular_common__["Location"]])
    ], SubMenuMenuItemsComponent);
    return SubMenuMenuItemsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/topbar/topbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".ui-inputtext {\r\n    width: 100% !important;\r\n}\r\n\r\n.listSearchWidth {\r\n    width: 300px;\r\n}\r\n\r\n.toggleWidth {\r\n    margin-top: 7px;\r\n}\r\n\r\n.pointer {\r\n    cursor: pointer;\r\n}\r\n\r\n.layout-wrapper .layout-main .layout-topbar .layout-topbar-menu-wrapper .topbar-menu>li.profile-item>a .profile-image-wrapper img {\r\n    width: 30px !important;\r\n    height: 30px !important;\r\n}\r\n\r\n.searchCss {\r\n    background: -webkit-gradient(linear, left top, right top, from(#ffffff), to(#e2e2e2));\r\n    background: linear-gradient(to right, #ffffff, #e2e2e2);\r\n}\r\n\r\n.searchIcon {\r\n    color: black\r\n}\r\n\r\n.toppadd {\r\n    margin-top: -7px !important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/topbar/topbar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"layout-topbar\">\n\n    <a href=\"#\" class=\"menu-btn\" (click)=\"app.onMenuButtonClick($event)\">\n        <i class=\"material-icons\">&#xE5D2;</i>\n    </a>\n\n    <a href=\"#\" class=\"topbar-menu-btn\" (click)=\"app.onTopbarMobileMenuButtonClick($event)\">\n        <i class=\"material-icons\">&#xE853;</i>\n    </a>\n    <div class=\"layout-topbar-menu-wrapper\">\n        <ul class=\"topbar-menu fadeInDown toppadd\" [ngClass]=\"{'topbar-menu-active': app.topbarMenuActive}\" (click)=\"app.onTopbarMenuClick($event)\">\n            <li #profile class=\"profile-item\" [ngClass]=\"{'active-topmenuitem': app.activeTopbarItem === profile}\" (click)=\"app.onTopbarRootItemClick($event, profile)\">\n                <a href=\"#\">\n                    <span class=\"profile-image-wrapper\">\n                    <img src=\"assets/images/userImage.png\"/>\n                    </span>\n                    <span class=\"topbar-item-name profile-name\">{{currentUser.fulName}}</span>\n                </a>\n                <ul class=\"fadeInDown\">\n                    <li role=\"menuitem\">\n                        <a (click)=\"settings()\" class=\"pointer\">\n                            <i class=\"material-icons\">settings_application</i>\n                            <span>{{'Settings' | translate }}</span>\n                        </a>\n                    </li>\n                    <li role=\"menuitem\">\n                        <a (click)=\"userHistory()\" class=\"pointer\">\n                            <i class=\"material-icons\">history</i>\n                            <span>{{'User History' | translate }}</span>\n                        </a>\n                    </li>\n                    <li role=\"menuitem\">\n                        <a (click)=\"logOutUser()\" class=\"pointer\">\n                            <i class=\"material-icons\">exit_to_app</i>\n                            <span>{{'Logout' | translate }}</span>\n                        </a>\n                    </li>\n                </ul>\n            </li>\n            <li class=\"search-item toggleWidth\">\n                <!-- <div class=\" ui-g\">\n                    <div class=\" ui-g-12 ui-lg-12 ui-sm-12 ui-g-nopad\">\n                        <div class=\" ui-g-12 ui-lg-4 ui-sm-4\">\n                            <p-inputSwitch [(ngModel)]=\"switchChecked\" (onChange)=\"toggle($event)\"></p-inputSwitch>\n                        </div>\n                        <div class=\" ui-g-12 ui-lg-8 ui-sm-8\">\n                            <label>  {{toggleValue}}  </label>\n                        </div>\n\n                    </div>\n                </div> -->\n                <p-inputSwitch [(ngModel)]=\"switchChecked\" (onChange)=\"toggle($event)\"></p-inputSwitch> &nbsp;&nbsp;&nbsp;\n                <label>  {{placeholderText | translate }}  </label>\n            </li>\n            <li class=\"search-item\">\n                <span class=\"md-inputfield\">\n                                <button pButton type=\"button\" icon=\"ui-icon-youtube-searched-for\" (click)=\"advancedSearchclicked()\" tooltipPosition=\"top\" pTooltip=\"{{'Advanced Search' | translate}}\"  class=\"orange-btn\" ></button>\n                    </span>\n            </li>\n            <li class=\"search-item listSearchWidth\">\n                <form [formGroup]=\"searchForm\" (ngSubmit)=\"searchFormSubmit(searchForm)\">\n                    <div class=\"ui-g ui-g-12 ui-g-nopad\">\n                        <div class=\"ui-g-10 ui-g-nopad\">\n                            <span class=\"md-inputfield search-item\">\n                                <!-- searchCss -->\n                                <input type=\"text\" pInputText formControlName=\"searchtext\" />\n                                <label style=\"\">{{ 'Search' | translate}} </label>\n                                <i class=\"topbar-icon material-icons pointer searchIcon\" (click)=\"searchForm.reset()\">clear</i>\n                        <!-- <i class=\"topbar-icon material-icons pointer searchIcon\"   (click)=\"searchFormSubmit(searchForm)\">search</i> -->\n                            </span>\n                        </div>\n                        <div class=\"ui-g-2 ui-g-nopad\">\n                            <button type=\"button\" (click)=\"searchFormSubmit(searchForm)\" style=\"margin-left: 5px; width: 36.5px\" tooltipPosition=\"top\" pTooltip=\"{{'Search' | translate}}\" pButton class=\"green-btn\" icon=\"ui-icon-search\"></button>\n                        </div>\n                    </div>\n                </form>\n            </li>\n        </ul>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/app-components/layout/topbar/topbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TopbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__admin_layout_admin_layout_component__ = __webpack_require__("../../../../../src/app/components/app-components/layout/admin-layout/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_user_service__ = __webpack_require__("../../../../../src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__node_modules_ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/@ngx-translate/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_document_service__ = __webpack_require__("../../../../../src/app/service/document.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TopbarComponent = (function () {
    function TopbarComponent(app, us, ds, router, fb, location, translate) {
        this.app = app;
        this.us = us;
        this.ds = ds;
        this.router = router;
        this.fb = fb;
        this.location = location;
        this.translate = translate;
        this.switchChecked = false;
        this.toggleValue = 'Correspondence';
        this.previewLogo = '';
        this.placeholderText = this.translate.instant('Workflow');
        this.currentUser = this.us.getCurrentUser();
        this.searchForm = this.fb.group({
            searchtext: [null, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required])]
        });
        var browserLang = translate.getBrowserLang();
        translate.use('en');
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
    TopbarComponent.prototype.ngOnInit = function () {
        if (localStorage.getItem('Default Search') === 'DOCUMENTS') {
            this.switchChecked = true;
            this.toggleValue = 'Document';
            this.placeholderText = this.translate.instant('Document');
        }
        else {
            this.toggleValue = 'Correspondence';
            this.placeholderText = this.translate.instant('Workflow');
        }
        var url = this.location.prepareExternalUrl(this.location.path());
        if (url.includes('advancedsearch')) {
            this.router.navigate(['/advancedsearch'], { queryParams: { 'seachTypeseachType': this.toggleValue } });
        }
        else if (url.includes('search')) {
            this.router.navigate(['/search'], { queryParams: { 'id': this.searchForm.controls.searchtext.value, 'type': this.toggleValue } });
        }
        else {
        }
    };
    TopbarComponent.prototype.logOutUser = function () {
        this.router.navigateByUrl('/');
        setTimeout(function () {
            window.location.reload();
        }, 10);
        this.us.logoutUser().subscribe(function (data) {
        }, function (error) { });
    };
    TopbarComponent.prototype.searchFormSubmit = function (searchForm) {
        if (this.searchForm.valid) {
            var searchString = this.searchForm.controls.searchtext.value;
            this.router.navigate(['/search'], { queryParams: { 'id': searchString, 'type': this.toggleValue } });
        }
    };
    TopbarComponent.prototype.advancedSearchclicked = function () {
        this.router.navigate(['/advancedsearch'], { queryParams: { 'seachType': this.toggleValue } });
    };
    TopbarComponent.prototype.toggle = function (event) {
        var _this = this;
        if (event.checked === true) {
            this.toggleValue = 'Document';
            this.placeholderText = this.translate.instant('Document');
        }
        else {
            this.toggleValue = 'Correspondence';
            this.placeholderText = this.translate.instant('Workflow');
        }
        setTimeout(function () {
            var url = _this.location.prepareExternalUrl(_this.location.path());
            if (url.includes('advancedsearch')) {
                _this.router.navigate(['/advancedsearch'], { queryParams: { 'seachType': _this.toggleValue } });
            }
            else if (url.includes('search')) {
                if (_this.searchForm.valid) {
                    _this.router.navigate(['/search'], { queryParams: { 'id': _this.searchForm.controls.searchtext.value, 'type': _this.toggleValue } });
                }
            }
            else {
            }
        }, 0);
    };
    TopbarComponent.prototype.settings = function () {
        this.router.navigate(['/settings']);
    };
    TopbarComponent.prototype.userHistory = function () {
        this.router.navigate(['/user-history']);
    };
    TopbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-topbar',
            template: __webpack_require__("../../../../../src/app/components/app-components/layout/topbar/topbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/app-components/layout/topbar/topbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__admin_layout_admin_layout_component__["a" /* AdminLayoutComponent */], __WEBPACK_IMPORTED_MODULE_2__service_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_7__service_document_service__["a" /* DocumentService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_5__angular_common__["Location"], __WEBPACK_IMPORTED_MODULE_6__node_modules_ngx_translate_core__["c" /* TranslateService */]])
    ], TopbarComponent);
    return TopbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/app-components/sign-add-page/sign-add-page.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".size {\r\n    min-height: 700px !important;\r\n}\r\n\r\n.imageBorder {\r\n    border: 1px solid #000 !important;\r\n}\r\n\r\n.top {\r\n    margin-top: 10px;\r\n}\r\n\r\n.amtClass {\r\n    font-weight: bold !important;\r\n    /* // margin: 3px;\r\n    // padding: 5px;\r\n    // border-radius: 3px;\r\n    // display: block; */\r\n}\r\n\r\n.background {\r\n    /* background: #246275 !important; */\r\n    color: #246275 !important;\r\n    border-radius: 3px !important;\r\n    /* // word-break: break-word !important;\r\n    // max-height: 36px !important;\r\n    margin-right: 10px;\r\n    // padding-bottom: 4px; */\r\n    /* padding-top: 7px !important; */\r\n    padding-right: 5px !important;\r\n    padding-bottom: 7px;\r\n}\r\n\r\n.nameDisplay {\r\n    /* // margin-top: -10px !important; */\r\n    margin-left: 3px !important;\r\n    padding-right: 3px !important;\r\n}\r\n\r\n.buttonStrip {\r\n    position: fixed;\r\n    z-index: 1001;\r\n    bottom: 0;\r\n    width: 100%;\r\n    margin: 0;\r\n    padding: 0;\r\n    right: 0 !important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/app-components/sign-add-page/sign-add-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"layout-wrapper layout-rtl size \" (click)=\"onWrapperClick()\" [ngClass]=\"{'layout-wrapper-static': layoutStatic,\n    'layout-wrapper-active': mobileMenuActive,\n    'layout-rtl': 'rtl'}\">\n    <app-menu-items [sideMenuItems]=\"pageValues\" (pageChanged)=\"pageChangedEvent($event)\" [disblePage]=\"showSignTab\" (submitted)=\"submitClicked($event)\"></app-menu-items>\n    <div [class]=\"isMobileYup === false ? 'ui-g-10 ui-g-nopad text-right' : 'ui-g-12 ui-g-nopad text-right'\">\n        <div class=\"ui-g-12 ui-g-nopad\">\n            <app-document-annotation [docId]=\"docid\" [activityId]=\"roleid\" [documentType]=\"docType\" [currentPage]=\"AnnotationCurrentPage\" [annotationType]=\"'COMMENT'\" [viewType]=\"'document'\" (pageChangeEmit)=\"pageChangeEmitted($event)\" [showPropBack]=\"'true'\" [timeStamp]=\"timestamp\"\n                (clickedOnPlaceholderEvent)=\"showSignPop($event)\"></app-document-annotation>\n            <br>\n            <div class=\"ui-g-12 ui-g-nopad\" *ngIf=\"isMobileYup\">\n                <h4 class=\"margin_0\" *ngIf=\"pageValues\">Page List</h4>\n                <div class=\"ui-g-12 ui-g-nopad left\">\n                    <div class=\"ui-g-12 ui-g-nopad\" *ngFor=\"let value of pageValues\" class=\"top background\" style=\"display: inline-block\">\n                        <span class=\"amtClass top\">\n                                        <div class=\"ui-g-12 ui-g-nopad nameDisplay \"><div class=\"ui-g-12 ui-g-nopad \"><u (click)=\"pageChanged(value)\">Page {{value.pageno}}</u></div></div>\n                            </span>\n                    </div>\n                </div>\n                <br>\n                <div class=\"ui-g-12 ui-g-nopad subBut center\">\n                    <button type=\"button\" pButton label=\"Submit\" icon=\"ui-icon-save\" [disabled]=\"disableSubmitbut\" (click)=\"submittedValue()\" class=\"ui-button-success\" [disabled]=\"disableSubmitbut\"></button>\n                </div>\n                <br>\n                <a class=\"nav-link buttonStrip\" style=\"font-size: 10px; text-align: right; left: 0; color: #246275\">\n                    <span class=\"ff-headers footerValue pointer\">{{'Copyright' | translate}} &copy; {{'HashECM 2018-20' | translate}}</span>\n                </a>\n            </div>\n        </div>\n    </div>\n</div>\n<ngx-smart-modal #addPad identifier=\"addPad\" [escapable]=\"false\" [dismissable]=\"false\" [closable]=\"false\">\n    <div class=\"ui-g-12 ui-g-nopad center\">\n        <div [class]=\"isMobileYup ? 'ui-g-2 left': 'ui-g-2 ui-g-nopad' \" *ngIf=\"isMobileYup\">\n            <button pButton type=\"button\" icon=\"fa-close\" (click)=\"closeSignModal($event)\"></button>\n        </div>\n        <div [class]=\"isMobileYup ? 'ui-g-4 ui-g-nopad' :  'ui-g-10 ui-g-nopad' \">\n            <h3>{{ 'Sign Here' | translate }}</h3>\n\n        </div>\n        <div [class]=\"isMobileYup ? 'ui-g-2 ui-g-nopad': 'ui-g-2 ui-g-nopad' \" *ngIf=\"!isMobileYup\">\n            <button pButton type=\"button\" icon=\"fa-close\" (click)=\"closeSignModal($event)\"></button>\n        </div>\n    </div>\n    <div class=\" ui-g-12 ui-lg-12 ui-sm-12\">\n        <div class=\"ui-g-12 ui-g-nopad\" *ngIf=\"!imageShow && !isMobileYup\">\n            <app-signature-pad #signatureSignAdd [canvasWidth]=\"460\" [canvasHeight]=\"300\" (onEndEvent)=\"AddSignComplete($event)\"></app-signature-pad>\n        </div>\n        <div clss=\"ui-g-12 ui-g-nopad\" *ngIf=\"isMobileYup && !imageShow\">\n            <app-signature-pad #signatureSignAdd [canvasWidth]=\"310\" [canvasHeight]=\"210\" (onEndEvent)=\"AddSignComplete($event)\"></app-signature-pad>\n        </div>\n        <div class=\"ui-g-12 ui-g-nopad\" *ngIf=\"imageShow && !isMobileYup\">\n            <img id=\"output_image\" [src]=\"sanitizer.bypassSecurityTrustUrl(imgURL)\" width=\"460\" height=\"330\" class=\"imageBorder\">\n        </div>\n        <div class=\"ui-g-12 ui-g-nopad\" *ngIf=\"imageShow && isMobileYup\">\n            <img id=\"output_image\" [src]=\"sanitizer.bypassSecurityTrustUrl(imgURL)\" width=\"310\" height=\"200\" class=\"imageBorder\">\n        </div>\n    </div>\n    <div class=\"modal-footer text-center\">\n        <br>\n        <div class=\" ui-g ui-g-12 ui-g-nopad\">\n\n            <div class=\"ui-g-6 ui-sm-12 \">\n                <div class=\"ui-g-12 ui-g-nopad\">\n                    <div [class]=\"isMobileYup?  'ui-g-9 ui-g-nopad' : 'ui-g-12 ui-g-nopad'\">\n                        <button pButton type=\"button\" class=\"pointer\" icon=\"ui-icon-attach-file\" tooltipPosition=\"top\" pTooltip=\"{{'Attach' | translate}}\" class=\"pointer\">\n                    <input type=\"file\" id=\"fileid\" ng2FileSelect accept=\"image/jpeg, image/png\" [uploader]=\"globelFileUploder\"   class=\"upload pointer\"  (change)=\"inputfileChanged($event)\" (click)=\"tempObject($event)\" />\n                    </button>\n                    </div>\n                </div>\n            </div>\n            <div [class]=\"isMobileYup ? 'ui-g-6 ui-md-6 ui-sm-12  left' : 'ui-g-6 ui-md-6 ui-sm-12 ui-g-nopad center' \">\n                <div class=\"ui-g-12 ui-sm-2\"></div>\n                <div class=\"ui-g-12 ui-sm-9 ui-g-nopad\">\n                    <button type=\"button\" class=\"ui-button ui-widget ui-corner-all ui-button-text-icon-left width \" (click)=\"clearSignature();\">\n                            <span class=\"ui-button-icon-left fa fa-close\"></span>\n                            <span class=\"ui-button-text\">clear</span>\n                        </button>\n                    <button type=\"button\" class=\"ui-button ui-widget ui-corner-all ui-button-text-icon-left width \" (click)=\"savesignature()\">\n                            <span class=\"ui-button-icon-left fa fa-check\"></span>\n                            <span class=\"ui-button-text\">Add</span>\n                        </button>\n                </div>\n            </div>\n        </div>\n    </div>\n</ngx-smart-modal>\n<ng4-loading-spinner [zIndex]=\"999999999999\" [timeout]=\"99000\"> </ng4-loading-spinner>"

/***/ }),

/***/ "../../../../../src/app/components/app-components/sign-add-page/sign-add-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignAddPageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_document_service__ = __webpack_require__("../../../../../src/app/service/document.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_smart_modal__ = __webpack_require__("../../../../ngx-smart-modal/esm5/ngx-smart-modal.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__generic_components_signature_pad_signature_pad_component__ = __webpack_require__("../../../../../src/app/components/generic-components/signature-pad/signature-pad.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_work_service__ = __webpack_require__("../../../../../src/app/service/work.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_toastr__ = __webpack_require__("../../../../ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var SignAddPageComponent = (function () {
    function SignAddPageComponent(router, activeRoute, ds, spinner, changeDetectRef, ws, tr, ngxModal, sanitizer) {
        this.router = router;
        this.activeRoute = activeRoute;
        this.ds = ds;
        this.spinner = spinner;
        this.changeDetectRef = changeDetectRef;
        this.ws = ws;
        this.tr = tr;
        this.ngxModal = ngxModal;
        this.sanitizer = sanitizer;
        this.layoutStatic = true;
        this.AnnotationCurrentPage = 1;
        this.showSignTab = false;
        this.docType = 'annotate';
        this.isMobileYup = false;
        this.globelFileUploder = new __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload__["FileUploader"]({});
        this.imageShow = false;
        this.reader = new FileReader();
        this.disableSubmitbut = false;
    }
    SignAddPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.spinner.show();
        this.showSignTab = false;
        this.activeRoute.queryParams.subscribe(function (params) {
            _this.activityid = params.activityid;
            _this.docid = params.docid;
            _this.roleid = params.roleid;
            _this.ds.getPagesForSignature(_this.docid, _this.roleid).subscribe(function (data) {
                _this.pagesResult(data);
                _this.spinner.hide();
            }, function (error) { _this.spinner.hide(); });
            if (window.innerWidth <= 1024) {
                _this.isMobileYup = true;
            }
            else {
                _this.isMobileYup = false;
            }
        });
    };
    SignAddPageComponent.prototype.ngOnChanges = function () {
        if (window.innerWidth <= 1024) {
            this.isMobileYup = true;
        }
        else {
            this.isMobileYup = false;
        }
        this.docType = 'annotate';
    };
    SignAddPageComponent.prototype.pagesResult = function (data) {
        if (data) {
            var value = JSON.parse(data._body);
            this.pageValues = value;
            if (this.pageValues) {
                for (var i = 0; i < this.pageValues.length; i++) {
                    if (this.pageValues[i].status === 'SIGNED') {
                        this.disableSubmitbut = false;
                    }
                    else {
                        this.disableSubmitbut = true;
                        break;
                    }
                }
                if (this.pageValues.length === 0) {
                    this.disableSubmitbut = false;
                }
            }
            else {
                this.disableSubmitbut = false;
            }
        }
    };
    SignAddPageComponent.prototype.logout = function () {
        this.router.navigateByUrl('/');
        setTimeout(function () {
            window.location.reload();
        }, 10);
    };
    SignAddPageComponent.prototype.onWrapperClick = function () {
        if (!this.menuClick && !this.menuButtonClick) {
            this.mobileMenuActive = false;
        }
        if (!this.topbarMenuClick && !this.topbarMenuButtonClick) {
            this.topbarMenuActive = false;
            this.activeTopbarItem = null;
        }
        this.menuClick = false;
        this.menuButtonClick = false;
        this.topbarMenuClick = false;
        this.topbarMenuButtonClick = false;
    };
    SignAddPageComponent.prototype.isMobile = function () {
        return window.innerWidth <= 1024;
    };
    SignAddPageComponent.prototype.onMenuButtonClick = function (event) {
        this.menuButtonClick = true;
        if (this.isMobile()) {
            this.mobileMenuActive = !this.mobileMenuActive;
        }
        event.preventDefault();
    };
    SignAddPageComponent.prototype.pageChangedEvent = function (event) {
        this.changedValue = event;
        this.AnnotationCurrentPage = event.pageno;
        this.showSignTab = false;
    };
    SignAddPageComponent.prototype.submitClicked = function (event) {
        var _this = this;
        if (event === 'yes') {
            this.spinner.show();
            this.ws.getActivityForAction(this.activityid).subscribe(function (data) { return _this.getactivityinfo(data); }, function (error) { _this.spinner.hide(); });
        }
    };
    SignAddPageComponent.prototype.submittedValue = function () {
        var _this = this;
        this.spinner.show();
        this.ws.getActivityForAction(this.activityid).subscribe(function (data) { return _this.getactivityinfo(data); }, function (error) { _this.spinner.hide(); });
    };
    SignAddPageComponent.prototype.getactivityinfo = function (datas) {
        var _this = this;
        var activityinfo = JSON.parse(datas._body);
        if (datas) {
            var value = {
                id: this.activityid,
                routes: [{
                        activityType: activityinfo.type.responses[0].routeToId,
                        roleId: activityinfo.type.responses[0].roleId
                    }],
                responseId: activityinfo.type.responses[0].id,
                comments: 'signed'
            };
            this.ws.finishActivity(value).subscribe(function (data) { _this.submitSuccessed(data); _this.spinner.hide(); }, function (error) { _this.spinner.hide(); });
        }
    };
    SignAddPageComponent.prototype.submitSuccessed = function (data) {
        this.router.navigateByUrl('/successPage');
        localStorage.removeItem('signapptoken');
    };
    SignAddPageComponent.prototype.pageChangeEmitted = function (event) {
        this.spinner.show();
        this.AnnotationCurrentPage = event.page + 1;
    };
    SignAddPageComponent.prototype.pageChanged = function (value) {
        this.spinner.show();
        this.AnnotationCurrentPage = value.pageno;
        var date = new Date();
        this.timestamp = date.getSeconds();
    };
    SignAddPageComponent.prototype.AddSignComplete = function (event) {
        this.addSignture = this.signaturePadSignAdd.toDataURL();
        if (this.isMobile()) {
            this.mobileMenuActive = false;
        }
    };
    SignAddPageComponent.prototype.clearSignature = function () {
        if (!this.imageShow) {
            this.signaturePadSignAdd.clear();
        }
        else {
            this.globelFileUploder = new __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload__["FileUploader"]({});
            this.imgURL = '';
            this.imageShow = false;
            this.addSignture = null;
        }
    };
    SignAddPageComponent.prototype.savesignature = function () {
        var _this = this;
        if (!this.imageShow) {
            if (this.addSignture) {
                this.addSignture = this.signaturePadSignAdd.toDataURL();
                var value = {
                    docId: this.docid,
                    pageNo: this.AnnotationCurrentPage,
                    comment: this.addSignture
                };
                this.spinner.show();
                this.ds.signDocPageWithNewSignature(value).subscribe(function (data) { _this.savedsignature(data, _this.docid); _this.spinner.hide(); }, function (error) { _this.spinner.hide(); });
            }
            else {
                this.tr.info('', 'Please add sign to save');
            }
        }
        else {
            var value = {
                docId: this.docid,
                pageNo: this.AnnotationCurrentPage,
            };
            this.formData = new FormData();
            this.formData.append('Signature', JSON.stringify(value));
            this.formData.append('file', this.globelFileUploder.queue[0]._file);
            this.spinner.show();
            this.ds.signDocPageWithFileSignature(this.formData).subscribe(function (data) { _this.savedsignature(data, _this.docid); _this.spinner.hide(); }, function (error) { _this.spinner.hide(); });
        }
    };
    SignAddPageComponent.prototype.savedsignature = function (data, docid) {
        if (!this.imageShow) {
            this.signaturePadSignAdd.clear();
        }
        this.ngxModal.getModal('addPad').close();
        var date = new Date();
        this.timestamp = date.getSeconds();
        this.imageShow = false;
        this.addSignture = null;
        this.ngOnInit();
    };
    SignAddPageComponent.prototype.showSignPop = function (event) {
        this.ngxModal.getModal('addPad').open();
        this.addSignture = null;
        this.showSignTab = true;
    };
    SignAddPageComponent.prototype.closeSignModal = function (e) {
        this.ngxModal.getModal('addPad').close();
        this.showSignTab = false;
        this.addSignture = null;
    };
    SignAddPageComponent.prototype.inputfileChanged = function (event) {
        if (this.globelFileUploder.queue[0]._file.type === 'image/png' || this.globelFileUploder.queue[0]._file.type === 'image/jpeg' || this.globelFileUploder.queue[0]._file.type === 'image/jpg') {
            this.imageShow = true;
            var url = (window.URL) ? window.URL.createObjectURL(this.globelFileUploder.queue[0]._file) : window.webkitURL.createObjectURL(this.globelFileUploder.queue[0]._file);
            this.imgURL = url;
            var value = window.btoa(event.target.files[0]);
            this.addSignture = 'data:image/png;base64,' + value;
        }
        else {
            this.tr.warning('Please select only .png or .jpeg/.jpg files');
            this.imageShow = false;
        }
    };
    SignAddPageComponent.prototype.tempObject = function (event) {
        this.globelFileUploder = new __WEBPACK_IMPORTED_MODULE_8_ng2_file_upload__["FileUploader"]({});
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('signatureSignAdd'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__generic_components_signature_pad_signature_pad_component__["a" /* SignatureComponent */])
    ], SignAddPageComponent.prototype, "signaturePadSignAdd", void 0);
    SignAddPageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sign-add-page',
            template: __webpack_require__("../../../../../src/app/components/app-components/sign-add-page/sign-add-page.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/app-components/sign-add-page/sign-add-page.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"], __WEBPACK_IMPORTED_MODULE_2__service_document_service__["a" /* DocumentService */],
            __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_6__service_work_service__["a" /* WorkService */], __WEBPACK_IMPORTED_MODULE_7_ngx_toastr__["b" /* ToastrService */], __WEBPACK_IMPORTED_MODULE_4_ngx_smart_modal__["b" /* NgxSmartModalService */], __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__["DomSanitizer"]])
    ], SignAddPageComponent);
    return SignAddPageComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/app-components/signin/signin.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"login-body\">\n    <div class=\"login-panel ui-fluid\">\n        <div class=\"login-panel-content\">\n            <div class=\"ui-g-12\">\n                <div class=\"ui-g-12 text-center\">\n                    <div class=\"ui-g-2 ui-md-2 ui-sm-2\"></div>\n                    <div class=\"ui-g-8 ui-md-8 ui-sm-12\">\n                        <img height=\"100px\" src=\"assets/images/logos/logo.png\">\n                    </div>\n                    <div class=\"ui-g-2 ui-md-2 ui-sm-2\"></div>\n                </div>\n                <div class=\"ui-g-12 ui-g-nopad\" *ngIf=\"validateToken\">\n                    <div class=\"ui-g-4 ui-g-nopad\">\n                        <label>PIN : </label>\n                    </div>\n                    <div class=\"ui-g-7 ui-g-nopad\">\n                        <input type=\"password\" pInputText class=\"ui-inputtext ui-corner-all ui-widget\" name=\"first\" id=\"one\" [(ngModel)]=\"uname\" pattern=\"\\d{4}\">\n                    </div>\n                </div>\n                <div class=\"ui-g-12\" *ngIf=\"validateToken\">\n                    <div class=\"ui-g-4 ui-sm-2\"></div>\n                    <div class=\"ui-g-5 ui-sm-9\">\n                        <button type=\"submit\" class=\"ui-button ui-widget ui-corner-all ui-button-text-icon-left small\" (click)=\"onSubmit(form)\">\n                                        <span class=\"ui-button-icon-left fa ui-icon-person\"></span>\n                                        <span class=\"ui-button-text\">Submit</span>\n                                    </button>\n                    </div>\n                    <div class=\"ui-g-3\"></div>\n\n                </div>\n                <div class=\"ui-g-12 \" *ngIf=\"!validateToken && errorText\">\n                    <label class=\"redstar\">{{errorText}}</label>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div id=\"spinner\" class=\"splash-screen \">\n    <div class=\"splash-loader-container \">\n        <svg class=\"splash-loader \" width=\"65px \" height=\"65px \" viewBox=\"0 0 66 66 \" xmlns=\"http://www.w3.org/2000/svg \">\n                   <circle class=\"path \" fill=\"none \" stroke-width=\"6 \" stroke-linecap=\"round \" cx=\"33 \" cy=\"33 \" r=\"30 \"></circle>\n                </svg>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/app-components/signin/signin.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".shadow {\n  -webkit-box-shadow: 7px 7px 10px 0px #ccc;\n          box-shadow: 7px 7px 10px 0px #ccc;\n  height: 50px;\n  padding-top: 15px;\n  text-align: center; }\n\n.boxposition {\n  position: absolute;\n  bottom: 0; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/app-components/signin/signin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SigninComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__service_user_service__ = __webpack_require__("../../../../../src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_document_service__ = __webpack_require__("../../../../../src/app/service/document.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_toastr__ = __webpack_require__("../../../../ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_stroe_service__ = __webpack_require__("../../../../../src/app/service/stroe.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SigninComponent = (function () {
    function SigninComponent(router, userservice, ds, tr, store, route) {
        this.router = router;
        this.userservice = userservice;
        this.ds = ds;
        this.tr = tr;
        this.store = store;
        this.route = route;
        this.errorMessage = false;
        this.user = localStorage.getItem('user');
        this.capsLock = false;
        this.showcookiepolicy = true;
        this.validateToken = false;
    }
    SigninComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.store.signIn = true; // change later
        this.uname = undefined;
        localStorage.removeItem('signapptoken');
        this.route.params.subscribe(function (data) {
            var token1 = data.urlLink.split(',');
            var token = token1[0];
            var val = token1[1];
            var value = {
                'key': token,
                'value': val
            };
            _this.ds.validateSignRequest(value).subscribe(function (datas) { _this.validToken(datas, value.key); }, function (error) { _this.validateToken = false; _this.showError(error); });
        });
        if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].sso) {
            var value = 0;
            if (this.showcookiepolicy === true) {
                value = 1;
            }
        }
        else {
            document.getElementById('spinner').style.display = 'none';
        }
    };
    SigninComponent.prototype.showError = function (error) {
        var err = JSON.parse(error._body);
        this.errorText = err.message;
    };
    SigninComponent.prototype.validToken = function (data, key) {
        if (data._body) {
            if (data.status === 200) {
                this.validateToken = true;
                var dataValue = JSON.parse(data._body);
                this.activityid = dataValue.activityid;
                this.docid = dataValue.docid;
                this.roleid = dataValue.roleid;
                this.token = key;
                this.store.token = key;
                localStorage.removeItem('signapptoken');
                localStorage.setItem('signapptoken', key);
            }
        }
    };
    SigninComponent.prototype.getDeploymentType = function (data) {
        if (data._body === 'CLOUD') {
            var emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
        }
    };
    SigninComponent.prototype.keyup = function (event, value) {
        var nextInput = event.srcElement.nextElementSibling; // get the sibling element
        var target = event.target || event.srcElement;
        var id = target.id;
        if (nextInput == null) {
            return;
        }
        else {
            nextInput.focus();
        } // focus if not null
    };
    SigninComponent.prototype.onSubmit = function (form) {
        var _this = this;
        if ((this.uname + '').length === 4) {
            var value = {
                pin: this.uname,
                activityid: this.activityid,
                docid: this.docid,
                roleid: this.roleid
            };
            document.getElementById('spinner').style.display = 'block';
            this.ds.validateSignaturePIN(value, this.token).subscribe(function (data) { _this.pinSucess(data); }, function (error) { document.getElementById('spinner').style.display = 'none'; }); // this.tr.error(error.message);
        }
        else {
            this.tr.error('', 'Please enter valid 4 didgit pin');
            return;
        }
    };
    SigninComponent.prototype.pinSucess = function (data) {
        this.router.navigate(['/home'], { queryParams: { activityid: this.activityid, docid: this.docid, roleid: this.roleid } });
        // this.app.signIn = true;
        document.getElementById('spinner').style.display = 'none';
    };
    SigninComponent.prototype.login = function (username, datares) {
        var _this = this;
        this.userservice.getUserDetails(username).subscribe(function (data) { return _this.signInUser(data); }, function (error) {
            _this.errorMessage = true;
            document.getElementById('spinner').style.display = 'none';
            localStorage.removeItem('token');
        });
    };
    SigninComponent.prototype.signTyped = function () {
        this.signIn = false;
    };
    SigninComponent.prototype.signInUser = function (data) {
        if (data._body !== '') {
            this.user = JSON.parse(data._body);
            localStorage.setItem('user', JSON.stringify(this.user));
            if (this.user.settings !== undefined) {
                for (var index = 0; index < this.user.settings.length; index++) {
                    if (this.user.settings[index].key === 'Default Document View') {
                        localStorage.setItem(this.user.settings[index].key, this.user.settings[index].val);
                        break;
                    }
                }
                for (var index = 0; index < this.user.settings.length; index++) {
                    if (this.user.settings[index].key === 'Default Theme') {
                        localStorage.setItem(this.user.settings[index].key, this.user.settings[index].val);
                        break;
                    }
                }
                for (var other = 0; other < this.user.settings.length; other++) {
                    if (this.user.settings[other].key !== 'Default Document View' && this.user.settings[other].key !== 'Default Theme') {
                        localStorage.setItem(this.user.settings[other].key, this.user.settings[other].val);
                    }
                }
            }
            this.router.navigateByUrl("/home");
        }
        else {
            this.signIn = true;
            document.getElementById('spinner').style.display = 'none';
        }
    };
    SigninComponent.prototype.detectCapsLock = function (event) {
        if (event.getModifierState('CapsLock')) {
            this.capsLock = true;
        }
        else {
            this.capsLock = false;
        }
    };
    SigninComponent.prototype.saveLoginDetails = function (event) {
        if (event === true) {
            this.showcookiepolicy = true;
        }
        else {
            this.showcookiepolicy = false;
        }
    };
    SigninComponent.prototype.policy = function () {
        window.open('../../../../assets/CookiePolicy/Cookie Policy.html');
    };
    SigninComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'app-signin',
            template: __webpack_require__("../../../../../src/app/components/app-components/signin/signin.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/app-components/signin/signin.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_0__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_4__service_document_service__["a" /* DocumentService */], __WEBPACK_IMPORTED_MODULE_5_ngx_toastr__["b" /* ToastrService */], __WEBPACK_IMPORTED_MODULE_6__service_stroe_service__["a" /* StoreService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["ActivatedRoute"]])
    ], SigninComponent);
    return SigninComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/app-components/success-page/success-page.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/app-components/success-page/success-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"login-body\">\n    <div class=\"login-panel ui-fluid\">\n        <div class=\"login-panel-content\">\n            <div class=\"ui-g-12\">\n                <div class=\"ui-g-12 text-center\">\n                    <div class=\"ui-g-2 ui-md-2 ui-sm-2\"></div>\n                    <div class=\"ui-g-8 ui-md-8 ui-sm-12\">\n                        <img height=\"100px\" src=\"assets/images/logos/logo.png\">\n                    </div>\n                    <div class=\"ui-g-2 ui-md-2 ui-sm-2\"></div>\n                </div>\n\n                <div class=\"ui-g-12 \">\n                    <label>The signed document is sent to the requester. Thank you for using Pistaceo Sign.</label>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/app-components/success-page/success-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SuccessPageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SuccessPageComponent = (function () {
    function SuccessPageComponent() {
    }
    SuccessPageComponent.prototype.ngOnInit = function () {
    };
    SuccessPageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-success-page',
            template: __webpack_require__("../../../../../src/app/components/app-components/success-page/success-page.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/app-components/success-page/success-page.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SuccessPageComponent);
    return SuccessPageComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/generic-components/document-annotation/document-annotation.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".imageBorder {\r\n    border: 1px solid black;\r\n}\r\n\r\n\r\n/* Spinner CSS */\r\n\r\n\r\n.lds-roller {\r\n    display: inline-block;\r\n    height: 64px;\r\n    left: 48%;\r\n    top: 47%;\r\n    margin-left: 50%;\r\n    margin-right: 50%;\r\n    position: inherit;\r\n}\r\n\r\n\r\n.lds-roller div {\r\n    -webkit-animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;\r\n            animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;\r\n    -webkit-transform-origin: 32px 32px;\r\n            transform-origin: 32px 32px;\r\n}\r\n\r\n\r\n.lds-roller div:after {\r\n    content: \" \";\r\n    display: block;\r\n    position: absolute;\r\n    width: 6px;\r\n    height: 6px;\r\n    border-radius: 50%;\r\n    background: #455a64;\r\n    margin: -3px 0 0 -3px;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(1) {\r\n    -webkit-animation-delay: -0.036s;\r\n            animation-delay: -0.036s;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(1):after {\r\n    top: 50px;\r\n    left: 50px;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(2) {\r\n    -webkit-animation-delay: -0.072s;\r\n            animation-delay: -0.072s;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(2):after {\r\n    top: 54px;\r\n    left: 45px;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(3) {\r\n    -webkit-animation-delay: -0.108s;\r\n            animation-delay: -0.108s;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(3):after {\r\n    top: 57px;\r\n    left: 39px;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(4) {\r\n    -webkit-animation-delay: -0.144s;\r\n            animation-delay: -0.144s;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(4):after {\r\n    top: 58px;\r\n    left: 32px;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(5) {\r\n    -webkit-animation-delay: -0.18s;\r\n            animation-delay: -0.18s;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(5):after {\r\n    top: 57px;\r\n    left: 25px;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(6) {\r\n    -webkit-animation-delay: -0.216s;\r\n            animation-delay: -0.216s;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(6):after {\r\n    top: 54px;\r\n    left: 19px;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(7) {\r\n    -webkit-animation-delay: -0.252s;\r\n            animation-delay: -0.252s;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(7):after {\r\n    top: 50px;\r\n    left: 14px;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(8) {\r\n    -webkit-animation-delay: -0.288s;\r\n            animation-delay: -0.288s;\r\n}\r\n\r\n\r\n.lds-roller div:nth-child(8):after {\r\n    top: 45px;\r\n    left: 10px;\r\n}\r\n\r\n\r\n@-webkit-keyframes lds-roller {\r\n    0% {\r\n        -webkit-transform: rotate(0deg);\r\n                transform: rotate(0deg);\r\n    }\r\n    100% {\r\n        -webkit-transform: rotate(360deg);\r\n                transform: rotate(360deg);\r\n    }\r\n}\r\n\r\n\r\n@keyframes lds-roller {\r\n    0% {\r\n        -webkit-transform: rotate(0deg);\r\n                transform: rotate(0deg);\r\n    }\r\n    100% {\r\n        -webkit-transform: rotate(360deg);\r\n                transform: rotate(360deg);\r\n    }\r\n}\r\n\r\n\r\n.ng-sticky-item h2,\r\n.ng-sticky-item p {\r\n    overflow: hidden;\r\n    text-overflow: ellipsis\r\n}\r\n\r\n\r\n.ng-sticky-item {\r\n    position: relative;\r\n    overflow: hidden;\r\n    text-decoration: none;\r\n    color: #000;\r\n    background: #ffc;\r\n    display: block;\r\n    height: 10em;\r\n    width: 10em;\r\n    padding: 1em;\r\n    -webkit-box-shadow: 5px 5px 7px rgba(33, 33, 33, .7);\r\n            box-shadow: 5px 5px 7px rgba(33, 33, 33, .7)\r\n}\r\n\r\n\r\n.ng-sticky-item .ng-sticky-item-header {\r\n    position: absolute;\r\n    right: 0;\r\n    width: 100%;\r\n    top: 0;\r\n    text-align: right;\r\n    opacity: .2\r\n}\r\n\r\n\r\n.ng-sticky-item .ng-sticky-item-header .ng-sticky-notes-button {\r\n    margin-right: 5px\r\n}\r\n\r\n\r\n.ng-sticky-item .ng-sticky-item-footer {\r\n    height: 1em;\r\n    position: absolute;\r\n    width: 100%;\r\n    bottom: 0;\r\n    text-align: right;\r\n    left: 0\r\n}\r\n\r\n\r\n.ng-sticky-item .ng-sticky-item-footer p {\r\n    margin: -5px;\r\n    padding: 0;\r\n    font-size: 1em;\r\n    font-weight: 700;\r\n    color: #979797\r\n}\r\n\r\n\r\n.ng-sticky-item .ng-sticky-item-content {\r\n    width: 100%;\r\n    height: 100%;\r\n    overflow: hidden\r\n}\r\n\r\n\r\n.signatureStickyContent {\r\n    width: 100%;\r\n    height: 100%;\r\n    overflow: hidden;\r\n    word-break: break-all;\r\n}\r\n\r\n\r\n.ng-sticky-item:hover .ng-sticky-item-footer,\r\n.ng-sticky-item:hover .ng-sticky-item-header {\r\n    opacity: 1\r\n}\r\n\r\n\r\n.ng-sticky-item .watermark {\r\n    opacity: .2\r\n}\r\n\r\n\r\n.ng-sticky-item h2 {\r\n    margin: 0;\r\n    font-family: arial, sans-serif;\r\n    font-size: 140%;\r\n    padding: 0 0 10px;\r\n    background: 0 0;\r\n    border: 0;\r\n    width: 100%;\r\n    white-space: nowrap\r\n}\r\n\r\n\r\n.ng-sticky-item h2 input[type=text],\r\n.ng-sticky-item h2 textarea {\r\n    font-weight: 700;\r\n    background: 0 0;\r\n    border: 0;\r\n    width: 100%\r\n}\r\n\r\n\r\n.ng-sticky-item p,\r\n.ng-sticky-item p input[type=text],\r\n.ng-sticky-item p textarea {\r\n    width: 100%;\r\n    border: 0;\r\n    background: 0 0\r\n}\r\n\r\n\r\n.ng-sticky-item p {\r\n    min-height: 2em;\r\n    white-space: normal\r\n}\r\n\r\n\r\n.ng-sticky-item p input[type=text],\r\n.ng-sticky-item p textarea {\r\n    resize: none;\r\n    font-size: 100%\r\n}\r\n\r\n\r\n.ng-sticky-notes li,\r\n.ng-sticky-notes ul {\r\n    list-style: none\r\n}\r\n\r\n\r\n.ng-sticky-notes ul {\r\n    padding-right: 10em;\r\n    min-height: 14em\r\n}\r\n\r\n\r\n.ng-sticky-notes ul li {\r\n    margin: 1em;\r\n    float: left;\r\n    width: 20px\r\n}\r\n\r\n\r\n.ng-sticky-notes ul li .ng-sticky-item {\r\n    z-index: 1;\r\n    -webkit-transform: rotate(-6deg);\r\n    transform: rotate(-6deg);\r\n    -webkit-box-shadow: 5px 5px 7px rgba(33, 33, 33, .7);\r\n            box-shadow: 5px 5px 7px rgba(33, 33, 33, .7);\r\n    -webkit-transition: -webkit-transform .15s linear;\r\n    transition: -webkit-transform .15s linear;\r\n    transition: transform .15s linear;\r\n    transition: transform .15s linear, -webkit-transform .15s linear\r\n}\r\n\r\n\r\n.ng-sticky-notes ul li:nth-child(even) .ng-sticky-item {\r\n    -webkit-transform: rotate(4deg);\r\n    transform: rotate(4deg);\r\n    position: relative;\r\n    top: 5px\r\n}\r\n\r\n\r\n.ng-sticky-notes ul li:nth-child(3n) .ng-sticky-item {\r\n    -webkit-transform: rotate(-3deg);\r\n    transform: rotate(-3deg);\r\n    position: relative;\r\n    top: -5px\r\n}\r\n\r\n\r\n.ng-sticky-notes ul li:nth-child(5n) .ng-sticky-item {\r\n    -webkit-transform: rotate(5deg);\r\n    transform: rotate(5deg);\r\n    position: relative;\r\n    top: -10px\r\n}\r\n\r\n\r\n.ng-sticky-notes ul li .ng-sticky-item:focus,\r\n.ng-sticky-notes ul li .ng-sticky-item:hover {\r\n    -webkit-box-shadow: 10px 10px 7px rgba(0, 0, 0, .7);\r\n            box-shadow: 10px 10px 7px rgba(0, 0, 0, .7);\r\n    -webkit-transform: scale(1.25);\r\n    transform: scale(1.25);\r\n    position: relative;\r\n    z-index: 5\r\n}\r\n\r\n\r\n.notHover {\r\n    position: relative;\r\n    overflow: hidden;\r\n    text-decoration: none;\r\n    color: #000;\r\n    background: #ffc;\r\n    display: block;\r\n    height: 10em;\r\n    width: 10em;\r\n    padding: 1em;\r\n    -webkit-box-shadow: 5px 5px 7px rgba(33, 33, 33, .7);\r\n            box-shadow: 5px 5px 7px rgba(33, 33, 33, .7)\r\n}\r\n\r\n\r\n.ng-sticky-notes-button {\r\n    outline: 0;\r\n    border: 0;\r\n    cursor: pointer\r\n}\r\n\r\n\r\n.ng-sticky-item:hover {\r\n    -webkit-box-shadow: 10px 10px 7px rgba(0, 0, 0, .7);\r\n            box-shadow: 10px 10px 7px rgba(0, 0, 0, .7);\r\n    -webkit-transform: scale(1.25);\r\n    transform: scale(1.25);\r\n    position: relative;\r\n    z-index: 5;\r\n}\r\n\r\n\r\n.box {\r\n    height: 100%;\r\n    width: 100%;\r\n    background-color: #e3e4c2;\r\n    line-height: 140px;\r\n    text-align: center;\r\n    -webkit-box-shadow: 6px 6px 15px 2px rgba(0, 0, 0, .3);\r\n            box-shadow: 6px 6px 15px 2px rgba(0, 0, 0, .3);\r\n    display: inline-block;\r\n    margin: 20px;\r\n}\r\n\r\n\r\n.drag-block {\r\n    position: absolute;\r\n    left: 0;\r\n    top: 0;\r\n    /* set these so Chrome doesn't return 'auto' from getComputedStyle */\r\n    background: rgba(255, 255, 255, 0.66);\r\n    border: 2px solid rgba(0, 0, 0, 0.5);\r\n    border-radius: 4px;\r\n}\r\n\r\n\r\n.box.box-right {\r\n    float: right;\r\n    width: 240px;\r\n}\r\n\r\n\r\n.right {\r\n    text-align: right !important;\r\n}\r\n\r\n\r\n.textGap {\r\n    margin-top: 6px;\r\n    padding-left: 31px;\r\n    font-weight: bold;\r\n}\r\n\r\n\r\n.headBackground {\r\n    color: #fff !important;\r\n    background: #246275;\r\n}\r\n\r\n\r\n.top {\r\n    margin-top: 10px !important;\r\n}\r\n\r\n\r\n.left {\r\n    text-align: left !important;\r\n}\r\n\r\n\r\n.placeholder {\r\n    background: rgb(244, 244, 14);\r\n    height: 37px !important;\r\n    padding: 6px !important;\r\n    /* padding-left: 10px; */\r\n    width: 120px !important;\r\n    border: none;\r\n    cursor: pointer;\r\n}\r\n\r\n\r\n.placeholder:active {\r\n    border: none !important;\r\n}\r\n\r\n\r\n.removeSignPlace {\r\n    width: 10px !important;\r\n    height: 23px !important;\r\n    float: right !important;\r\n}\r\n\r\n\r\n.addBtnTop {\r\n    margin-top: 36% !important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/generic-components/document-annotation/document-annotation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"ui-g dashboard ui-g-nopad\" id=\"myP\">\n    <div class=\"ui-g-12 ui-lg-12 messages ui-g-nopad\">\n        <div *ngIf=\"imageToShow !== '' \">\n            <div class=\"ui-g-12\" id=\"textAnnotation\" *ngIf=\"textAnnotationDisplay\" class=\"pointer\">\n                <div class=\"ui-g-12 ui-g-nopad\">\n                    <p-paginator #ppaginatort [rows]=\"1\" [first]=\"PaginatorcurrentPage\" [totalRecords]=\"totalPages\" (onPageChange)=\"paginate($event)\"></p-paginator>\n                    <div class=\"ui-g-12 ui-g-nopad\" id=\"myImage\">\n                        <img class=\"ui-g-12  ui-g-nopad\" [src]=\"imageToShow\" #myBounds style=\"border: 2px solid;\">\n                    </div>\n                    <div *ngFor=\"let block of blocks\">\n                        <div *ngIf=\"block.type ==='TEXT'\">\n                            <div *ngIf=\"block.page === currentPage\" ngDraggable class=\"drag-block\" [position]=\"{x: block.pageWidthX, y: block.pageHeightY}\" (stopped)=\"onStop($event , block.id)\" (movingOffset)=\"onMoving($event)\" [preventDefaultEvent]=\"false\" (endOffset)=\"onMoveEnd($event,  block.id)\"\n                                [bounds]=\"myBounds\" [inBounds]=\"inBounds\">\n                                <div class=\"ng-sticky-item\" ng-style=\"getStyle()\" style=\"background: rgb(255, 255, 204);\">\n                                    <div class=\"ng-sticky-item-header\">\n                                        <button pButton type=\"button\" icon=\"fa-close\" (click)=\"removeAnnotation(block.id)\"></button>\n                                    </div>\n                                    <div class=\"ng-sticky-item-content\">\n                                        <p class=\"ng-hide\">\n                                            <textarea [name]=\"block.id\" placeholder=\"Enter comment\" [id]=\"block.id\" cols=\"30\" rows=\"4\" [value]=\"block.text\" (input)=\"annotationText($event , block.id )\"></textarea>\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        <div *ngIf=\"block.type ==='SIGN'\">\n                            <div *ngIf=\"block.page === currentPage\" ngDraggable class=\"drag-block\" [position]=\"{x: block.pageWidthX, y: block.pageHeightY}\" (stopped)=\"onStop($event , block.id)\" (movingOffset)=\"onMoving($event)\" [preventDefaultEvent]=\"false\" (endOffset)=\"onMoveEnd($event,  block.id)\"\n                                [bounds]=\"myBounds\" [inBounds]=\"inBounds\">\n                                <div class=\"ui-g-12 ui-g-nopad\" style=\"background: rgb(244, 244, 14);height: 75px; padding: 0; width: 120px\">\n                                    <button pButton type=\"button\" class=\"removeSignPlace\" icon=\"fa-close\" (click)=\"removeAnnotation(block.id)\"></button>\n                                    <textarea class=\"placeholder\" [name]=\"block.id\" [id]=\"block.id\" cols=\"30\" rows=\"3\" [value]=\"block.text\" readonly></textarea>\n                                </div>\n                            </div>\n                        </div>\n\n                    </div>\n\n                    <div *ngFor=\"let block of allSavedAnnotaion\">\n                        <div *ngIf=\"userSelected === '' && statusSelected === '' \">\n                            <div *ngIf=\"block.pageNo === currentPage\" ngDraggable class=\"drag-block savedannotation\" [trackPosition]=\"false\" [position]=\"{x: ((block.xPos * (this.imageDivX / previewImage.width)) + addX), y: ((block.yPos * (this.imageDivY / previewImage.height)) + addY)}\">\n                                <div class=\"row\" *ngIf=\"block.type === 'COMMENT'\">\n                                    <div class=\"ng-sticky-item\" ng-style=\"getStyle()\" style=\"background: rgb(255, 255, 204);\">\n                                        <div class=\"ng-sticky-item-header\">\n                                            <button type=\"button\" pButton *ngIf=\"block.userName === us.getCurrentUser().fulName || us.getCurrentUser().isAdmin === 'Y'\" (click)=\"completeannotation(block)\" label=\"{{'Delete' | translate}}\"></button>\n                                            <button type=\"button\" pButton *ngIf=\"block.status !=='ACTIVE'\" icon=\"fa-check\"></button>\n                                        </div>\n                                        <br>\n                                        <div class=\"ng-sticky-item-content\">\n                                            <p class=\"ng-hide\">\n                                                <textarea cols=\"30\" rows=\"5\" [value]=\"block.comment\" readonly></textarea>\n                                            </p>\n                                        </div>\n                                        <div class=\"ng-sticky-item-footer\">\n                                            <p>\n                                                -{{block.userName}}\n                                            </p>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"row\" *ngIf=\"block.type === 'SIGN'\" (click)=\"clickedOnPlaceholder(block)\">\n                                    <div class=\"ui-g-12 ui-g-nopad\" style=\"background: rgb(244, 244, 14);height: 50px; padding: 0;\">\n                                        <textarea class=\"placeholder\" [name]=\"block.id\" [id]=\"block.id\" cols=\"30\" rows=\"3\" [value]=\"'Sign Here'\" readonly></textarea>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n\n                        <div *ngIf=\"userSelected !== ''  && statusSelected === '' \">\n                            <div *ngIf=\"block.userName === userSelected || block.status === statusSelected\">\n                                <div>\n                                    <div *ngIf=\"block.pageNo === currentPage \" ngDraggable class=\"drag-block savedannotation\" [trackPosition]=\"false\" [position]=\"{x: ((block.xPos * (this.imageDivX / previewImage.width)) + addX), y: ((block.yPos * (this.imageDivY / previewImage.height)) + addY)}\">\n                                        <div class=\"row\" *ngIf=\"block.type === 'COMMENT'\">\n                                            <div class=\"ng-sticky-item\" ng-style=\"getStyle()\" style=\"background: rgb(255, 255, 204);\">\n                                                <div class=\"ng-sticky-item-header\" *ngIf=\"block.status ==='ACTIVE'\">\n                                                    <button type=\"button\" pButton *ngIf=\"block.userName === us.getCurrentUser().fulName || us.getCurrentUser().isAdmin === 'Y'\" (click)=\"completeannotation(block)\" label=\"{{'Delete' | translate}}\"></button>\n                                                    <button type=\"button\" pButton *ngIf=\"block.status !=='ACTIVE'\" icon=\"fa-check\"></button>\n                                                </div>\n                                                <br>\n                                                <div class=\"ng-sticky-item-content\">\n                                                    <p class=\"ng-hide\">\n                                                        <textarea cols=\"30\" rows=\"5\" [value]=\"block.comment\" readonly></textarea>\n                                                    </p>\n                                                </div>\n                                                <div class=\"ng-sticky-item-footer\">\n                                                    <p>\n                                                        -{{block.userName}}\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n\n                        <div *ngIf=\"statusSelected !== '' &&  userSelected === '' \">\n                            <div *ngIf=\"block.userName === userSelected || block.status === statusSelected\">\n                                <div>\n                                    <div *ngIf=\"block.pageNo === currentPage \" ngDraggable class=\"drag-block savedannotation\" [trackPosition]=\"false\" [position]=\"{x: ((block.xPos * (this.imageDivX / previewImage.width)) + addX), y: ((block.yPos * (this.imageDivY / previewImage.height)) + addY)}\">\n                                        <div class=\"row\" *ngIf=\"block.type === 'COMMENT'\">\n                                            <div class=\"ng-sticky-item\" ng-style=\"getStyle()\" style=\"background: rgb(255, 255, 204);\">\n                                                <div class=\"ng-sticky-item-header\" *ngIf=\"block.status ==='ACTIVE'\">\n                                                    <button type=\"button\" pButton *ngIf=\"block.userName === us.getCurrentUser().fulName || us.getCurrentUser().isAdmin === 'Y'\" (click)=\"completeannotation(block)\" label=\"{{'Delete' | translate}}\"></button>\n                                                    <button type=\"button\" pButton *ngIf=\"block.status !=='ACTIVE'\" icon=\"fa-check\"></button>\n                                                </div>\n                                                <br>\n                                                <div class=\"ng-sticky-item-content\">\n                                                    <p class=\"ng-hide\">\n                                                        <textarea cols=\"30\" rows=\"5\" [value]=\"block.comment\" readonly></textarea>\n                                                    </p>\n                                                </div>\n                                                <div class=\"ng-sticky-item-footer\">\n                                                    <p>\n                                                        -{{block.userName}}\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n\n                        <div *ngIf=\"userSelected !== '' && statusSelected !== '' \">\n                            <div *ngIf=\"block.userName === userSelected \">\n                                <div *ngIf=\"block.status === statusSelected\">\n                                    <div *ngIf=\"block.pageNo === currentPage \" ngDraggable class=\"drag-block savedannotation\" [trackPosition]=\"false\" [position]=\"{x: ((block.xPos * (this.imageDivX / previewImage.width)) + addX), y: ((block.yPos * (this.imageDivY / previewImage.height)) + addY)}\">\n                                        <div class=\"row\" *ngIf=\"block.type === 'COMMENT'\">\n                                            <div class=\"ng-sticky-item\" ng-style=\"getStyle()\" style=\"background: rgb(255, 255, 204);\">\n                                                <div class=\"ng-sticky-item-header\" *ngIf=\"block.status ==='ACTIVE'\">\n                                                    <button type=\"button\" pButton *ngIf=\"block.userName === us.getCurrentUser().fulName || us.getCurrentUser().isAdmin === 'Y'\" (click)=\"completeannotation(block)\" label=\"{{'Delete' | translate}}\"></button>\n                                                    <button type=\"button\" pButton *ngIf=\"block.status !=='ACTIVE'\" icon=\"fa-check\"></button>\n                                                </div>\n                                                <br>\n                                                <div class=\"ng-sticky-item-content\">\n                                                    <p class=\"ng-hide\">\n                                                        <textarea cols=\"30\" rows=\"5\" [value]=\"block.comment\" readonly></textarea>\n                                                    </p>\n                                                </div>\n                                                <div class=\"ng-sticky-item-footer\">\n                                                    <p>\n                                                        -{{block.userName}}\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"ui-g-12  ui-g-nopad\">\n                <p-paginator #ppaginatorth [rows]=\"1\" [first]=\"PaginatorcurrentPage\" [totalRecords]=\"totalPages\" (onPageChange)=\"paginate($event)\"></p-paginator>\n            </div>\n        </div>\n    </div>\n</div>\n\n<ngx-smart-modal #annotationModelList identifier=\"annotationModelList\">\n    <h3>{{ 'Annotation list' | translate }}</h3>\n    <div class=\"dashboard\" style=\"max-height: 450px;overflow-y: auto\">\n        <div class=\"global-sales\">\n            <table class=\"ui-g-12\">\n                <thead>\n                    <tr>\n                        <td>\n\n                        </td>\n                        <td>\n                            {{'User Name' | translate}}\n                        </td>\n                        <td>\n                            {{'Comment' | translate}}\n                        </td>\n                        <td>\n                            {{'Type' | translate}}\n                        </td>\n                        <td>\n                            {{'Page Number' | translate}}\n                        </td>\n                        <td>\n\n                        </td>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr class=\"pointer showRemove\" *ngFor=\"let annotation of listOfDcumentAnnotation; let i = index\" (click)=\"documnetAnnoation(annotation.pageNo , annotation.type);annotationListModel = false;\">\n                        <td>\n                            {{i +1 }}\n                        </td>\n                        <td>\n                            {{annotation.userName}}\n                        </td>\n                        <td>\n                            {{annotation.comment}}\n                        </td>\n                        <td>\n                            {{annotation.type}}\n                        </td>\n                        <td>\n                            {{annotation.pageNo}}\n                        </td>\n                        <td class=\"removeClass\">\n                            <button type=\"button\" pButton icon=\"ui-icon-visibility\" (click)=\"documnetAnnoation(annotation.pageNo , annotation.type);annotationListModel = false;\"></button>\n                            <button type=\"button\" pButton icon=\"ui-icon-close\" (click)=\"deleteAnnotation(annotation);\"></button>\n\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n        <br>\n    </div>\n</ngx-smart-modal>\n<ngx-smart-modal #signHolder identifier=\"signHolder\">\n    <h3>{{ 'Add Role' | translate }}</h3>\n    <div class=\"ui-g-12 ui-g-nopad\">\n        <div class=\"ui-grid ui-grid-responsive ui-fluid\">\n            <div class=\" ui-grid-row \">\n                <div class=\"ui-grid-col-12 dropdownTop\">\n                    <p-dropdown [options]=\"showRoles\" [autoWidth]=\"false\" [(ngModel)]=\"selectedRole\" filter=\"true\" optionLabel=\"label\" placeholder=\"Select Role\" (onChange)=\"roleNamechanged($event)\"></p-dropdown>\n                </div>\n            </div>\n        </div>\n    </div>\n    <br>\n    <div class=\"modal-footer\">\n        <br>\n        <div class=\" ui-g ui-g-12 ui-g-nopad\">\n            <div class=\"ui-g-8 ui-g-nopad\"></div>\n            <div class=\"ui-g-4 ui-g-nopad text-right\">\n\n                <button type=\"button\" class=\"ui-button ui-widget ui-corner-all ui-button-text-icon-left width\" (click)=\"signHolder.close(); selectedSigned = 'select'\">\n                        <span class=\"ui-button-icon-left fa fa-close\"></span>\n                        <span class=\"ui-button-text\">No</span>\n                    </button>\n                <button type=\"button\" class=\"ui-button ui-widget ui-corner-all ui-button-text-icon-left width\" (click)=\"addRole()\">\n                        <span class=\"ui-button-icon-left fa fa-check\"></span>\n                        <span class=\"ui-button-text\">Add</span>\n                    </button>\n            </div>\n        </div>\n    </div>\n</ngx-smart-modal>\n<ngx-smart-modal #addSignaturePad identifier=\"addSignaturePad\" [escapable]=\"false\" [dismissable]=\"false\" [closable]=\"false\">\n    <div class=\"ui-g-12 ui-g-nopad\">\n        <div class=\"ui-g-10 ui-g-nopad\">\n            <h3>{{ 'Add Signature' | translate }}</h3>\n\n        </div>\n        <div class=\"ui-g-2 ui-g-nopad right\">\n            <button pButton type=\"button\" icon=\"fa-close\" (click)=\"closeSignModal($event)\"></button>\n        </div>\n    </div>\n    <div class=\" ui-g-12 ui-lg-12 ui-sm-12\">\n        <div class=\"ui-g-7 ui-g-nopad\">\n            <app-signature-pad #signatureSignAdd [canvasWidth]=\"460\" [canvasHeight]=\"300\" (onEndEvent)=\"AddSignComplete($event)\"></app-signature-pad>\n        </div>\n        <div class=\"ui-g-5 ui-g-nopad\">\n            <div class=\" ui-g ui-g-12 ui-g-nopad addBtnTop\">\n                <div class=\"ui-g-2 ui-g-nopad\"></div>\n                <div class=\"ui-g-4 ui-g-nopad text-right \">\n\n                    <button type=\"button\" class=\"ui-button ui-widget ui-corner-all ui-button-text-icon-left width\" (click)=\"clearSignature();\">\n                                    <span class=\"ui-button-icon-left fa fa-close\"></span>\n                                    <span class=\"ui-button-text\">Clear</span>\n                                </button>\n\n                </div>\n                <div class=\"ui-g-4 ui-g-nopad\">\n                    <button type=\"button\" class=\"ui-button ui-widget ui-corner-all ui-button-text-icon-left width\" (click)=\"savesignature()\">\n                                <span class=\"ui-button-icon-left fa fa-check\"></span>\n                                <span class=\"ui-button-text\">Add</span>\n                            </button>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</ngx-smart-modal>\n<ngx-smart-modal #isAddExistingSignature identifier=\"isAddExistingSignature\" [escapable]=\"false\" [dismissable]=\"false\" [closable]=\"false\">\n    <div class=\"ui-g-12 ui-g-nopad\">\n        <div class=\"ui-g-10 ui-g-nopad\">\n            <h3>{{ 'Confirmation' | translate }}</h3>\n        </div>\n        <div class=\"ui-g-2 ui-g-nopad right\">\n            <button pButton type=\"button\" icon=\"fa-close\" (click)=\"closeConfirmed($event)\"></button>\n        </div>\n    </div>\n    <div class=\" ui-g-12 ui-lg-12 ui-sm-12\">\n        <label>{{'The system will sign the document page with your saved signature. Want to proceed?' | translate}}</label>\n    </div>\n    <div class=\"modal-footer\">\n        <br>\n        <div class=\" ui-g ui-g-12 ui-g-nopad\">\n            <div class=\"ui-g-8 ui-g-nopad\"></div>\n            <div class=\"ui-g-4 ui-g-nopad text-right\">\n\n                <button type=\"button\" class=\"ui-button ui-widget ui-corner-all ui-button-text-icon-left width\" (click)=\"isAddExistingSignature.close(); selectedSigned = 'select'\">\n                        <span class=\"ui-button-icon-left fa fa-close\"></span>\n                        <span class=\"ui-button-text\">No</span>\n                    </button>\n                <button type=\"button\" class=\"ui-button ui-widget ui-corner-all ui-button-text-icon-left width\" (click)=\"addedExistingSignature()\">\n                        <span class=\"ui-button-icon-left fa fa-check\"></span>\n                        <span class=\"ui-button-text\">YES</span>\n                    </button>\n            </div>\n        </div>\n    </div>\n</ngx-smart-modal>"

/***/ }),

/***/ "../../../../../src/app/components/generic-components/document-annotation/document-annotation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocumentAnnotationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/@ngx-translate/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_document_service__ = __webpack_require__("../../../../../src/app/service/document.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signature_pad_signature_pad_component__ = __webpack_require__("../../../../../src/app/components/generic-components/signature-pad/signature-pad.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__ = __webpack_require__("../../../../primeng/primeng.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_primeng_primeng___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_primeng_primeng__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_file_saver__ = __webpack_require__("../../../../file-saver/src/FileSaver.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_file_saver___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_file_saver__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_components_layout_breadcrumb_breadcrumb_service__ = __webpack_require__("../../../../../src/app/components/app-components/layout/breadcrumb/breadcrumb.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_user_service__ = __webpack_require__("../../../../../src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ngx_toastr__ = __webpack_require__("../../../../ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__service_content_service__ = __webpack_require__("../../../../../src/app/service/content.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ngx_smart_modal__ = __webpack_require__("../../../../ngx-smart-modal/esm5/ngx-smart-modal.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var DocumentAnnotationComponent = (function () {
    function DocumentAnnotationComponent(translate, ds, ngxSmartModal, us, tr, cs, router, breadcrumbService, spinnerService) {
        var _this = this;
        this.translate = translate;
        this.ds = ds;
        this.ngxSmartModal = ngxSmartModal;
        this.us = us;
        this.tr = tr;
        this.cs = cs;
        this.router = router;
        this.breadcrumbService = breadcrumbService;
        this.spinnerService = spinnerService;
        this.pageChangeEmit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.documentTypeChangeEmit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.completeActivityEmit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.showProps = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.clickedOnPlaceholderEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.pageBackCount = 0;
        this.imageToShow = '';
        this.displayImage = true;
        this.allSavedAnnotaion = [];
        this.usernamedropdownText = [];
        this.annotationStatusdropdownText = [];
        this.usernamedropdownImage = [];
        this.imageannotationURL = [];
        this.hideheading = true;
        this.signatureIsEmpty = false;
        this.blocks = [];
        this.inBounds = true;
        this.myBounds = {
            width: 200,
            height: 200
        };
        this.edge = {
            top: true,
            bottom: true,
            left: true,
            right: true
        };
        this.movingOffset = { x: 0, y: 0 };
        this.endOffset = { x: 0, y: 0 };
        this.annotationid = 1;
        this.statusSelected = '';
        this.userSelected = '';
        this.statusList = [];
        this.userList = [];
        this.hideAllAnnotation = false;
        this.listOfDcumentAnnotation = [];
        this.showRoles = [];
        this.signedList = [];
        this.addSignOptions = [];
        this.selectedSigned = 'select';
        this.showAddSignDropOptions = false;
        this.imageToShow = '';
        this.annotationList = [
            { label: this.translate.instant('Image annotation'), value: 'imageAnnotation' },
            { label: this.translate.instant('Text annotation'), value: 'textAnnotation' }
        ];
        this.addSignOptions = [
            { label: this.translate.instant('Select Signature Option'), value: 'select' },
            { label: this.translate.instant('Saved Signature'), value: 'existing' },
            { label: this.translate.instant('Add New Signature'), value: 'newsignature' }
        ];
        this.imageContext = [
            {
                label: this.translate.instant('Download'),
                icon: 'ui-icon-file-download',
                command: function (event) { return _this.downloadThisdocument(); }
            },
            {
                label: this.translate.instant('With annotation'),
                icon: 'ui-icon-file-download',
                command: function (event) { return _this.downloadannotedDoc(); }
            }
        ];
    }
    DocumentAnnotationComponent.prototype.sizeChange = function (event) {
        var ele = document.getElementById('myImage');
        if (ele) {
            var actual = ele.getBoundingClientRect();
            this.imageDivX = actual.width;
            this.imageDivY = actual.height;
            this.multiplyWidth = this.previewImage.width / this.imageDivX;
            this.multiplyHeight = this.previewImage.height / this.imageDivY;
        }
    };
    DocumentAnnotationComponent.prototype.trigger = function () {
        window.dispatchEvent(new Event('resize'));
    };
    DocumentAnnotationComponent.prototype.ngOnChanges = function () {
        var _this = this;
        this.spinnerService.show();
        setTimeout(function () {
            _this.canvasWidth = document.getElementById('myP').clientWidth - 20;
        }, 10);
        this.pageBackCount = -1;
        this.imageAnnotationDisplay = false;
        this.textAnnotationDisplay = false;
        this.imageToShow = '';
        this.imageannotationURL = [];
        if (this.documentType === 'readonly') {
            this.signatureDisabled = 'readonly';
        }
        if (this.annotationType === 'IMAGE') {
            this.textAnnotationDisplay = false;
            this.annotationTypeSelected = 'imageAnnotation';
            setTimeout(function () {
                _this.imageAnnotationDisplay = true;
            }, 1000);
            if (this.docId !== undefined) {
                this.ds.getSignHolders(this.docId, this.currentPage, this.activityId).subscribe(function (resdata) { return _this.getAllAnnotation(resdata); }, function (error) { return _this.spinnerService.hide(); });
            }
        }
        else {
            if (this.docId !== undefined) {
                this.ds.getSignHolders(this.docId, this.currentPage, this.activityId).subscribe(function (resdata) { return _this.getAllAnnotation(resdata); }, function (error) { return _this.spinnerService.hide(); });
            }
        }
    };
    DocumentAnnotationComponent.prototype.showAddSignDrop = function (data) {
        if (data) {
            var value = data._body;
            if (this.toUppercase(value) === 'YES') {
                this.showAddSignDropOptions = true;
            }
            else {
                this.showAddSignDropOptions = false;
            }
        }
    };
    DocumentAnnotationComponent.prototype.toUppercase = function (value) {
        return value.toUpperCase();
    };
    DocumentAnnotationComponent.prototype.addSignHolder = function (event) {
        var _this = this;
        this.spinnerService.show();
        this.us.getRoles().subscribe(function (data) { _this.rolesResult(data); _this.spinnerService.hide(); }, function (error) { _this.spinnerService.hide(); });
        this.selectedRole = '';
        this.ngxSmartModal.getModal('signHolder').open();
    };
    DocumentAnnotationComponent.prototype.rolesResult = function (data) {
        var _this = this;
        this.showRoles = [];
        if (data) {
            var roleArray = JSON.parse(data._body);
            roleArray.forEach(function (element) {
                var value = {
                    label: element.name,
                    value: element.id
                };
                _this.showRoles.push(value);
            });
        }
    };
    DocumentAnnotationComponent.prototype.roleNamechanged = function (event) {
        this.selectedRole = event.value;
    };
    DocumentAnnotationComponent.prototype.addRole = function () {
        if (this.selectedRole) {
            this.blocks.push({
                text: this.selectedRole.label,
                type: 'SIGN',
                imageId: '',
                previewUrl: '',
                id: this.annotationid,
                roleId: this.selectedRole.value,
                xPos: this.offsetX,
                yPos: this.offsetY,
                page: this.currentPage,
                pageWidthX: this.pageWidthX,
                pageHeightY: this.pageHeightY
            });
            this.annotationid++;
            this.ngxSmartModal.getModal('signHolder').close();
        }
        else {
            this.tr.info('', 'Please select one role');
        }
    };
    DocumentAnnotationComponent.prototype.ngAfterViewInit = function () {
        if (this.annotationType === 'IMAGE') {
            if (this.imageannotationURL.length > 0) {
            }
        }
    };
    DocumentAnnotationComponent.prototype.goToProperties = function () {
        this.showProps.emit('true');
    };
    DocumentAnnotationComponent.prototype.getPreviewImage = function (data) {
        var _this = this;
        this.previewImage = JSON.parse(data._body);
        this.spinnerService.hide();
        this.imageToShow = 'data:image/png;base64,' + this.previewImage.image;
        this.currentPage = this.previewImage.pageNo;
        this.PaginatorcurrentPage = this.currentPage - 1;
        this.totalPages = this.previewImage.pageCount;
        this.displayImage = false;
        if (this.annotationType === 'IMAGE') {
            this.imageAnnotationDisplay = true;
            this.textAnnotationDisplay = false;
            setTimeout(function () {
                _this.signaturePad.fromDataURL(_this.imageannotationURL);
            }, 2000);
            this.annotationTypeSelected = 'imageAnnotation';
        }
        else {
            this.imageAnnotationDisplay = false;
            this.textAnnotationDisplay = true;
            this.annotationTypeSelected = 'textAnnotation';
        }
        setTimeout(function () {
            var element = document.getElementById('myP'); // replace elementId with your element's Id.
            var rect = element.getBoundingClientRect();
            var ele = document.getElementById('myImage');
            var actual = ele.getBoundingClientRect();
            _this.imageDivX = actual.width;
            _this.imageDivY = actual.height;
            _this.addX = actual.left + window.pageXOffset;
            _this.addY = actual.top + window.pageYOffset;
            _this.multiplyWidth = _this.previewImage.width / _this.imageDivX;
            _this.multiplyHeight = _this.previewImage.height / _this.imageDivY;
            _this.spinnerService.hide();
            // this.storePosition = true;
            _this.sizeChange('event');
        }, 10);
    };
    DocumentAnnotationComponent.prototype.paginate = function (event) {
        if (this.blocks.length === 0 && this.signatureIsEmpty === false) {
            event.docId = this.docId;
            this.pageChangeEmit.emit(event);
        }
        else {
            this.tr.info('', 'Please save annotations before changing page');
        }
    };
    DocumentAnnotationComponent.prototype.getAllAnnotation = function (data) {
        var _this = this;
        this.allSavedAnnotaion = JSON.parse(data._body);
        this.usernamedropdownText = [{ label: this.translate.instant('Select'), value: '' }];
        this.annotationStatusdropdownText = [{ label: this.translate.instant('Select'), value: '' }];
        this.usernamedropdownImage = [{ label: this.translate.instant('Select'), value: '' }];
        var usernameSet = new Set();
        var usernameSetImage = new Set();
        var annotationStatus = new Set();
        for (var index = 0; index < this.allSavedAnnotaion.length; index++) {
            if (this.allSavedAnnotaion[index].type === 'COMMENT' && this.allSavedAnnotaion[index].pageNo === +this.currentPage) {
                usernameSet.add(this.allSavedAnnotaion[index].userName);
                annotationStatus.add(this.allSavedAnnotaion[index].status);
            }
            if (this.allSavedAnnotaion[index].type === 'IMAGE' && this.allSavedAnnotaion[index].pageNo === +this.currentPage) {
                usernameSetImage.add(this.allSavedAnnotaion[index].userName);
            }
        }
        usernameSet.forEach(function (ele) {
            _this.usernamedropdownText.push({ label: _this.translate.instant(ele), value: ele });
        });
        usernameSetImage.forEach(function (ele) {
            _this.usernamedropdownImage.push({ label: _this.translate.instant(ele), value: ele });
        });
        annotationStatus.forEach(function (ele) {
            _this.annotationStatusdropdownText.push({ label: _this.translate.instant(ele), value: ele });
        });
        if (this.annotationType === 'IMAGE') {
            var url = {
                url: this.ds.downloadPreviewPage(this.docId, this.currentPage)
            };
            this.imageannotationURL.push(url);
            for (var index = 0; index < this.allSavedAnnotaion.length; index++) {
                if (this.allSavedAnnotaion[index].type === 'IMAGE' && this.allSavedAnnotaion[index].pageNo === +this.currentPage) {
                    var url2 = {
                        url: this.ds.downloadAnnotation(this.allSavedAnnotaion[index].imageId, this.currentPage)
                    };
                    this.imageannotationURL.push(url2);
                }
            }
        }
        this.spinnerService.show();
        this.ds.getPreviewPage(this.docId, this.currentPage).subscribe(function (datares) { return _this.getPreviewImage(datares); }, function (error) { return _this.spinnerService.hide(); });
    };
    DocumentAnnotationComponent.prototype.annotationTypeChanged = function () {
        var _this = this;
        if (this.signatureIsEmpty === false && (this.blocks.length === 0)) {
            if (this.annotationTypeSelected !== 'imageAnnotation') {
                this.documentTypeChangeEmit.emit({
                    docId: this.docId,
                    pageno: this.currentPage,
                    annoatationType: 'COMMENT'
                });
                localStorage.setItem('Default Annotation', 'COMMENT');
                this.annotationType = 'COMMENT';
            }
            else {
                this.documentTypeChangeEmit.emit({
                    docId: this.docId,
                    pageno: this.currentPage,
                    annoatationType: 'IMAGE'
                });
                localStorage.setItem('Default Annotation', 'IMAGE');
                this.annotationTypeSelected = 'imageAnnotation';
                this.annotationType = 'IMAGE';
            }
        }
        else {
            setTimeout(function () {
                if (_this.annotationTypeSelected === 'imageAnnotation') {
                    _this.annotationTypeSelected = 'textAnnotation';
                    _this.annotationType = 'COMMENT';
                }
                else {
                    _this.annotationTypeSelected = 'imageAnnotation';
                    _this.annotationType = 'IMAGE';
                }
            }, 1000);
            this.tr.error('', 'Please save annotations');
        }
    };
    DocumentAnnotationComponent.prototype.drawComplete = function () {
        // will be notified of szimek/signature_pad's onEnd event
        this.signatureIsEmpty = true;
    };
    DocumentAnnotationComponent.prototype.AddSignComplete = function (event) {
        this.addSignture = this.signaturePadSignAdd.toDataURL();
    };
    DocumentAnnotationComponent.prototype.clearSignature = function () {
        this.signaturePadSignAdd.clear();
    };
    DocumentAnnotationComponent.prototype.closeSignModal = function (e) {
        this.ngxSmartModal.getModal('addSignaturePad').close();
        this.selectedSigned = 'select';
    };
    DocumentAnnotationComponent.prototype.savesignature = function () {
        var _this = this;
        this.addSignture = this.signaturePadSignAdd.toDataURL();
        var value = {
            docId: this.docId,
            pageNo: this.currentPage,
            comment: this.addSignture
        };
        this.spinnerService.show();
        this.ngxSmartModal.getModal('addSignaturePad').close();
        this.ds.signDocPageWithNewSignature(value).subscribe(function (data) { _this.savedsignature(data); _this.spinnerService.hide(); });
    };
    DocumentAnnotationComponent.prototype.backButtonClicked = function () {
        this.router.navigateByUrl(localStorage.getItem('backClick'));
        // this.location.back();
    };
    DocumentAnnotationComponent.prototype.downloadThisdocument = function () {
        var _this = this;
        this.ds.downloadDocument(this.docId).subscribe(function (data) { return _this.downloadCurrentDocument(data); });
    };
    DocumentAnnotationComponent.prototype.downloadannotedDoc = function () {
        var _this = this;
        this.ds.downloadAnnotatedDocument(this.docId).subscribe(function (data) { return _this.downloadCurrentDocument(data); });
    };
    DocumentAnnotationComponent.prototype.downloadCurrentDocument = function (data) {
        var filename = '';
        var disposition = data.headers.get('Content-Disposition');
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
            filename = matches[1].replace(/['"]/g, '');
        }
        Object(__WEBPACK_IMPORTED_MODULE_5_file_saver__["saveAs"])(data._body, filename);
    };
    DocumentAnnotationComponent.prototype.removeAnnotation = function (id) {
        for (var index = 0; index < this.blocks.length; index++) {
            if (this.blocks[index].id === id) {
                this.blocks.splice(index, 1);
            }
        }
    };
    DocumentAnnotationComponent.prototype.annotationText = function (event, id) {
        for (var index = 0; index < this.blocks.length; index++) {
            if (this.blocks[index].id === id) {
                this.blocks[index].text = event.target.value;
            }
        }
    };
    DocumentAnnotationComponent.prototype.addTextAnnotation = function (offsetX, offsetY) {
        this.blocks.push({
            text: '',
            type: 'TEXT',
            imageId: '',
            previewUrl: '',
            id: this.annotationid,
            xPos: offsetX,
            yPos: offsetY,
            page: this.currentPage,
            pageWidthX: this.pageWidthX,
            pageHeightY: this.pageHeightY
        });
        this.annotationid++;
    };
    DocumentAnnotationComponent.prototype.onStop = function (event, id) {
        for (var index = 0; index < this.blocks.length; index++) {
            if (this.blocks[index].id === id) {
                this.blocks[index].xPos = this.endOffset.x;
                this.blocks[index].yPos = this.endOffset.y;
            }
        }
    };
    DocumentAnnotationComponent.prototype.onMoving = function (event) {
        this.movingOffset.x = event.x;
        this.movingOffset.y = event.y;
    };
    DocumentAnnotationComponent.prototype.addSignChanged = function (event) {
        if (this.selectedSigned === 'existing') {
            this.ngxSmartModal.getModal('isAddExistingSignature').open();
        }
        else if (this.selectedSigned === 'newsignature') {
            this.ngxSmartModal.getModal('addSignaturePad').open();
            // this.signaturePadSignAdd.clear();
        }
    };
    DocumentAnnotationComponent.prototype.addedExistingSignature = function () {
        var _this = this;
        this.ngxSmartModal.getModal('isAddExistingSignature').close();
        this.spinnerService.show();
        this.ds.signDocumentPageWithStoredSignature(this.docId, this.currentPage).subscribe(function (data) { _this.savedsignature(data); _this.spinnerService.hide(); }, function (error) { _this.spinnerService.hide(); });
    };
    DocumentAnnotationComponent.prototype.closeConfirmed = function (e) {
        this.ngxSmartModal.getModal('isAddExistingSignature').close();
        this.selectedSigned = 'select';
    };
    DocumentAnnotationComponent.prototype.savedsignature = function (data) {
        this.tr.success('', 'Signatured is added');
        this.selectedSigned = 'select';
        this.imageannotationURL = [];
        this.blocks = [];
        if (this.annotationType === 'IMAGE') {
            var url = {
                url: this.ds.downloadPreviewPage(this.docId, this.currentPage)
            };
            this.imageannotationURL.push(url);
            for (var index = 0; index < this.allSavedAnnotaion.length; index++) {
                if (this.allSavedAnnotaion[index].type === 'IMAGE' && this.allSavedAnnotaion[index].pageNo === +this.currentPage) {
                    var url1 = {
                        url: this.ds.downloadAnnotation(this.allSavedAnnotaion[index].imageId, 1)
                    };
                    this.imageannotationURL.push(url1);
                }
            }
            var url2 = {
                url: this.ds.downloadAnnotation('', 1)
            };
            this.imageannotationURL.push(url2);
            this.signaturePad.fromDataURL(this.imageannotationURL);
        }
        this.annotationcompleted('');
    };
    DocumentAnnotationComponent.prototype.addExistingSignature = function () {
        // this.tr.info('', 'Please select a location to add signature');
    };
    DocumentAnnotationComponent.prototype.mouseDown = function (e) {
        var rect = e.currentTarget.getBoundingClientRect();
        var ele = document.getElementById('myImage');
        var actual = ele.getBoundingClientRect();
        this.imageDivX = actual.width;
        this.imageDivY = actual.height;
        this.multiplyWidth = this.previewImage.width / this.imageDivX;
        this.multiplyHeight = this.previewImage.height / this.imageDivY;
        this.offsetX = (e.offsetX * this.multiplyWidth);
        this.pageWidthX = e.pageX;
        this.offsetY = (e.offsetY * this.multiplyHeight);
        this.pageHeightY = e.pageY;
    };
    DocumentAnnotationComponent.prototype.findImagePos = function (e) {
    };
    DocumentAnnotationComponent.prototype.addTextAnnotationPosition = function (event) {
        this.addTextAnnotation(this.offsetX, this.offsetY);
    };
    DocumentAnnotationComponent.prototype.onMoveEnd = function (event, id) {
        for (var index = 0; index < this.blocks.length; index++) {
            if (this.blocks[index].id === id) {
                this.blocks[index].xPos = (event.x - this.addX) * this.multiplyWidth; // addx
                this.blocks[index].yPos = (event.y - this.addY) * this.multiplyHeight;
                this.blocks[index].pageX = event.x;
                this.blocks[index].pageY = event.y;
                this.pageWidthX = event.x;
                this.pageHeightY = event.y;
            }
        }
    };
    DocumentAnnotationComponent.prototype.statusDropdownChange = function (event) {
        this.annotationStatusSelected = this.statusSelected;
    };
    DocumentAnnotationComponent.prototype.userDropdownChange = function (event) {
        var _this = this;
        var usernameSet = new Set();
        this.annotationStatusdropdownText = [];
        this.annotationStatusdropdownText = [{ label: this.translate.instant('Select'), value: '' }];
        if (this.userSelected !== '') {
            for (var index = 0; index < this.allSavedAnnotaion.length; index++) {
                if (this.allSavedAnnotaion[index].type === 'COMMENT' && this.allSavedAnnotaion[index].userName === this.userSelected) {
                    usernameSet.add(this.allSavedAnnotaion[index].status);
                }
            }
            usernameSet.forEach(function (ele) {
                _this.annotationStatusdropdownText.push({ label: _this.translate.instant(ele), value: ele });
            });
        }
        else {
            for (var index = 0; index < this.allSavedAnnotaion.length; index++) {
                if (this.allSavedAnnotaion[index].type === 'COMMENT') {
                    usernameSet.add(this.allSavedAnnotaion[index].status);
                }
            }
            usernameSet.forEach(function (ele) {
                _this.annotationStatusdropdownText.push({ label: _this.translate.instant(ele), value: ele });
            });
        }
    };
    DocumentAnnotationComponent.prototype.completeannotation = function (block) {
        var _this = this;
        this.cs.deleteAnnotation(block.id).subscribe(function (res) { _this.annotationcompleted(res); _this.tr.success('', 'Annotation deleted'); });
        // this.ds.completeAnnotation(block.id).subscribe(res => this.annotationcompleted(res) );
    };
    DocumentAnnotationComponent.prototype.annotationcompleted = function (res) {
        var _this = this;
        if (this.docId !== undefined) {
            // this.ds.getAnnotations(this.docId).subscribe(resdata => this.getAllAnnotation(resdata));
            this.ds.getSignHolders(this.docId, this.currentPage, this.activityId).subscribe(function (resdata) { return _this.getAllAnnotation(resdata); }, function (error) { return _this.spinnerService.hide(); });
        }
    };
    DocumentAnnotationComponent.prototype.saveTextAnnotation = function () {
        var _this = this;
        if (this.blocks.length > 0) {
            for (var index = 0; index < this.blocks.length; index++) {
                var annoation = void 0;
                if (this.blocks[index].type === 'TEXT') {
                    annoation = {
                        docId: this.docId,
                        comment: this.blocks[index].text,
                        type: 'COMMENT',
                        empNo: this.us.getCurrentUser().EmpNo,
                        xPos: this.blocks[index].xPos,
                        yPos: this.blocks[index].yPos,
                        userName: this.us.getCurrentUser().userLogin,
                        pageNo: this.blocks[index].page,
                        pageWidth: this.previewImage.width,
                        pageHeight: this.previewImage.height
                    };
                }
                else if (this.blocks[index].type === 'SIGN') {
                    annoation = {
                        docId: this.docId,
                        comment: this.blocks[index].text,
                        roleId: this.blocks[index].roleId,
                        type: 'SIGN',
                        empNo: this.us.getCurrentUser().EmpNo,
                        xPos: this.blocks[index].xPos,
                        yPos: this.blocks[index].yPos,
                        userName: this.us.getCurrentUser().userLogin,
                        pageNo: this.blocks[index].page,
                        pageWidth: this.previewImage.width,
                        pageHeight: this.previewImage.height
                    };
                }
                this.ds.saveAnnotation(annoation).subscribe(function (data) { return _this.savedAnnotation(data, ''); });
            }
        }
    };
    DocumentAnnotationComponent.prototype.savedAnnotation = function (data, docId) {
        this.tr.success('', 'Saved');
        // }
        this.imageannotationURL = [];
        this.blocks = [];
        if (this.annotationType === 'IMAGE') {
            var url = {
                url: this.ds.downloadPreviewPage(this.docId, this.currentPage)
            };
            this.imageannotationURL.push(url);
            for (var index = 0; index < this.allSavedAnnotaion.length; index++) {
                if (this.allSavedAnnotaion[index].type === 'IMAGE' && this.allSavedAnnotaion[index].pageNo === +this.currentPage) {
                    var url1 = {
                        url: this.ds.downloadAnnotation(this.allSavedAnnotaion[index].imageId, 1)
                    };
                    this.imageannotationURL.push(url1);
                }
            }
            var url2 = {
                url: this.ds.downloadAnnotation(docId, 1)
            };
            this.imageannotationURL.push(url2);
            this.signaturePad.fromDataURL(this.imageannotationURL);
        }
        this.annotationcompleted('');
    };
    DocumentAnnotationComponent.prototype.clearSignaturepad = function () {
        this.signatureIsEmpty = false;
        this.signaturePad.clear();
    };
    DocumentAnnotationComponent.prototype.saveImageAnnotation = function () {
        var _this = this;
        // if (this.addSignture === false) {
        this.blocks = [];
        this.signUrl = this.signaturePad.toDataURL();
        var byteCharacters = atob(this.signUrl.replace('data:image/png;base64,', ''));
        var byteNumbers = new Array(byteCharacters.length);
        for (var i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        var blob = new Blob([byteArray], { type: 'image/png' });
        // saveAs(blob, this.activityinfo.refNo);
        var string;
        string = this.us.getCurrentUser().EmpNo + '-imageannotation.png';
        var docInfo = {
            docclass: 'ProductivitiDocument',
            props: [{
                    'name': 'Document Title',
                    'symName': 'DocumentTitle',
                    'dtype': 'STRING',
                    'mvalues': [string],
                    'mtype': 'N',
                    'len': 255,
                    'rOnly': 'false',
                    'hidden': 'false',
                    'req': 'false'
                }],
            accessPolicies: []
        };
        var formData = new FormData();
        formData.append('DocInfo', JSON.stringify(docInfo));
        // formData.append('file', scannedPdfName);
        formData.append('document', blob, this.us.getCurrentUser().EmpNo + '-imageannotation.png');
        this.ds.addDocument(formData).subscribe(function (data) { return _this.digitalSignAnnotation(data); });
    };
    DocumentAnnotationComponent.prototype.digitalSignAnnotation = function (data) {
        var _this = this;
        this.signatureIsEmpty = false;
        this.signaturePad.clear();
        var image = {
            docId: this.docId,
            imageId: data._body,
            type: 'IMAGE',
            empNo: this.us.getCurrentUser().EmpNo,
            xPos: 0,
            yPos: 0,
            userName: this.us.getCurrentUser().userLogin,
            pageNo: this.currentPage
        };
        this.annotationid++;
        this.ds.saveAnnotation(image).subscribe(function (resdata) { return _this.savedAnnotation(resdata, data._body); });
    };
    DocumentAnnotationComponent.prototype.userDropdownImageChange = function (event) {
        this.imageannotationURL = [];
        var url = {
            url: this.ds.downloadPreviewPage(this.docId, this.currentPage)
        };
        this.imageannotationURL.push(url);
        for (var index = 0; index < this.allSavedAnnotaion.length; index++) {
            if (this.allSavedAnnotaion[index].type === 'IMAGE' && this.allSavedAnnotaion[index].pageNo === +this.currentPage && this.allSavedAnnotaion[index].userName === this.userSelected) {
                var url1 = {
                    url: this.ds.downloadAnnotation(this.allSavedAnnotaion[index].imageId, 1)
                };
                this.imageannotationURL.push(url1);
            }
        }
        if (this.userSelected === '') {
            for (var index = 0; index < this.allSavedAnnotaion.length; index++) {
                if (this.allSavedAnnotaion[index].type === 'IMAGE' && this.allSavedAnnotaion[index].pageNo === +this.currentPage) {
                    var url2 = {
                        url: this.ds.downloadAnnotation(this.allSavedAnnotaion[index].imageId, 1)
                    };
                    this.imageannotationURL.push(url2);
                }
            }
        }
        this.signaturePad.fromDataURL(this.imageannotationURL);
    };
    DocumentAnnotationComponent.prototype.listOfannotationOnDocument = function (annotationDocumentList) {
        var _this = this;
        if (this.docId !== undefined) {
            // this.cs.getAnnotations(this.docId).subscribe(data => this.getListOfAnnoattion(data));
            this.ds.getSignHolders(this.docId, this.currentPage, this.activityId).subscribe(function (resdata) { return _this.getAllAnnotation(resdata); }, function (error) { return _this.spinnerService.hide(); });
        }
    };
    DocumentAnnotationComponent.prototype.getListOfAnnoattion = function (data) {
        var resData = JSON.parse(data._body);
        if (resData.length === 0) {
            this.tr.info('', 'No annotation found on this document');
        }
        else {
            this.listOfDcumentAnnotation = JSON.parse(data._body);
            this.ngxSmartModal.getModal('annotationModelList').open();
        }
    };
    DocumentAnnotationComponent.prototype.cassed = function (event) {
        // console.log('focus');
    };
    DocumentAnnotationComponent.prototype.documnetAnnoation = function (pageNo, type) {
        this.currentPage = pageNo;
        this.annotationType = type;
        if (this.router.url.includes('inbox')) {
            this.documentType = 'annotate';
        }
        else if (this.router.url.includes('sent')) {
            this.documentType = 'readonly';
        }
        else if (this.router.url.includes('draft')) {
            this.documentType = 'annotate';
        }
        else if (this.router.url.includes('register')) {
            this.documentType = 'readonly';
        }
        else if (this.router.url.includes('archive')) {
            this.documentType = 'readonly';
        }
        else {
            this.documentType = 'annotate';
        }
        this.ngxSmartModal.getModal('annotationModelList').close();
        this.ngOnChanges();
    };
    DocumentAnnotationComponent.prototype.deleteAnnotation = function (annotation) {
        var _this = this;
        this.cs.deleteAnnotation(annotation.id).subscribe(function (data) {
            _this.ngxSmartModal.getModal('annotationModelList').close();
            _this.cs.getAnnotations(_this.docId).subscribe(function (datares) { return _this.getListOfAnnoattion(datares); });
            _this.ds.getSignHolders(_this.docId, _this.currentPage, _this.activityId).subscribe(function (resdata) { return _this.getAllAnnotation(resdata); }, function (error) { return _this.spinnerService.hide(); });
        });
    };
    DocumentAnnotationComponent.prototype.annotationComplete = function () {
        if (this.signatureIsEmpty === false && (this.blocks.length === 0)) {
            var date = new Date();
            this.completeActivityEmit.emit(date);
        }
        else {
            this.tr.error('', 'Please save annotations');
        }
    };
    DocumentAnnotationComponent.prototype.clickedOnPlaceholder = function (event) {
        this.clickedOnPlaceholderEvent.emit(event);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "docId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "documentType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "currentPage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "annotationType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "viewType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "pageChangeEmit", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "documentTypeChangeEmit", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "completeActivityEmit", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "timeStamp", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "activityId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('signature'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__signature_pad_signature_pad_component__["a" /* SignatureComponent */])
    ], DocumentAnnotationComponent.prototype, "signaturePad", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('signatureSignAdd'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__signature_pad_signature_pad_component__["a" /* SignatureComponent */])
    ], DocumentAnnotationComponent.prototype, "signaturePadSignAdd", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('ppaginatoro'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__["Paginator"])
    ], DocumentAnnotationComponent.prototype, "ppaginatoro", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('ppaginatort'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__["Paginator"])
    ], DocumentAnnotationComponent.prototype, "ppaginatort", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('ppaginatorth'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_primeng_primeng__["Paginator"])
    ], DocumentAnnotationComponent.prototype, "ppaginatorth", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "showPropBack", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "showProps", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], DocumentAnnotationComponent.prototype, "clickedOnPlaceholderEvent", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], DocumentAnnotationComponent.prototype, "sizeChange", null);
    DocumentAnnotationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-document-annotation',
            template: __webpack_require__("../../../../../src/app/components/generic-components/document-annotation/document-annotation.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/generic-components/document-annotation/document-annotation.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_2__service_document_service__["a" /* DocumentService */], __WEBPACK_IMPORTED_MODULE_11_ngx_smart_modal__["b" /* NgxSmartModalService */],
            __WEBPACK_IMPORTED_MODULE_8__service_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_9_ngx_toastr__["b" /* ToastrService */], __WEBPACK_IMPORTED_MODULE_10__service_content_service__["a" /* ContentService */], __WEBPACK_IMPORTED_MODULE_6__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_7__app_components_layout_breadcrumb_breadcrumb_service__["a" /* BreadcrumbService */], __WEBPACK_IMPORTED_MODULE_12_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], DocumentAnnotationComponent);
    return DocumentAnnotationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/generic-components/signature-pad/signature-pad.component.html":
/***/ (function(module, exports) {

module.exports = "<canvas id=\"mycanvas\" style=\"border: 2px solid black;\"></canvas>"

/***/ }),

/***/ "../../../../../src/app/components/generic-components/signature-pad/signature-pad.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignatureComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
'use strict';

var SignatureComponent = (function () {
    function SignatureComponent(elementRef) {
        this.elementRef = elementRef;
        this.options = this.options || {};
        this.onBeginEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.onEndEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    SignatureComponent.prototype.ngAfterContentInit = function () {
        var sp = __webpack_require__("../../../../signature_pad/dist/signature_pad.m.js")['default'];
        var canvas = this.elementRef.nativeElement.querySelector('canvas');
        var ctx = canvas.getContext('2d');
        canvas.width = this.canvasWidth;
        canvas.height = this.canvasHeight;
        this.signaturePad = new sp(canvas, this.options);
        this.signaturePad.onBegin = this.onBegin.bind(this);
        this.signaturePad.onEnd = this.onEnd.bind(this);
        if (this.disabled === 'readonly') {
            this.signaturePad.off();
        }
    };
    SignatureComponent.prototype.resizeCanvas = function () {
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        var canvas = this.signaturePad._canvas;
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas.getContext('2d').scale(ratio, ratio);
        this.signaturePad.clear();
    };
    SignatureComponent.prototype.toData = function () {
        return this.signaturePad.toData();
    };
    SignatureComponent.prototype.fromData = function (points) {
        this.signaturePad.fromData(points);
    };
    SignatureComponent.prototype.toDataURL = function (imageType, quality) {
        return this.signaturePad.toDataURL(imageType, quality);
    };
    SignatureComponent.prototype.fromDataURL = function (backgroundImages) {
        var imageUrl = '';
        for (var index = backgroundImages.length; index > 0; index--) {
            imageUrl = imageUrl + ("url(" + backgroundImages[index - 1].url + ")").replace('NaN', '1');
            if (index - 1 > 0) {
                imageUrl = imageUrl + ',';
            }
        }
        this.signaturePad.canvas.style.backgroundImage = imageUrl;
        this.signaturePad.canvas.style.backgroundSize = '100% 100%';
    };
    SignatureComponent.prototype.clear = function () {
        this.signaturePad.clear();
    };
    SignatureComponent.prototype.isEmpty = function () {
        return this.signaturePad.isEmpty();
    };
    // Unbinds all event handlers
    SignatureComponent.prototype.off = function () {
        this.signaturePad.off();
    };
    SignatureComponent.prototype.on = function () {
        this.signaturePad.on();
    };
    SignatureComponent.prototype.set = function (option, value) {
        switch (option) {
            case 'canvasHeight':
                this.signaturePad._canvas.height = value;
                break;
            case 'canvasWidth':
                this.signaturePad._canvas.width = value;
                break;
            default:
                this.signaturePad[option] = value;
        }
    };
    // notify subscribers on signature begin
    SignatureComponent.prototype.onBegin = function () {
        this.onBeginEvent.emit(true);
    };
    // notify subscribers on signature end
    SignatureComponent.prototype.onEnd = function () {
        this.onEndEvent.emit(true);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], SignatureComponent.prototype, "options", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], SignatureComponent.prototype, "canvasWidth", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], SignatureComponent.prototype, "canvasHeight", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], SignatureComponent.prototype, "onBeginEvent", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], SignatureComponent.prototype, "onEndEvent", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], SignatureComponent.prototype, "backgroundImages", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], SignatureComponent.prototype, "disabled", void 0);
    SignatureComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__("../../../../../src/app/components/generic-components/signature-pad/signature-pad.component.html"),
            selector: 'app-signature-pad',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
    ], SignatureComponent);
    return SignatureComponent;
}());



/***/ }),

/***/ "../../../../../src/app/global.variables.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppURLSettings; });

// export let base_url = 'http://106.51.66.219:9000/Productiviti/resources/';
// export let base_url = 'http://192.168.0.104:8080/Productiviti/resources/';
// export let base_url = 'http://172.20.10.219:9000/CMSV2/resources/';
var AppURLSettings = (function () {
    function AppURLSettings() {
    }
    AppURLSettings.SANDBOX_URL = window.location.origin + '/pistaceo/resources/';
    // public static SANDBOX_URL = 'http://ecmdemo1:8080/pistaceo/resources/';
    // public static DEV_URL = 'http://10.10.10.47:8080/pistaceo/resources/';
    // public static DEV_URL = 'http://10.10.10.44:8080/pistaceo/resources/';
    // public static DEV_URL = 'http://10.10.10.44:8080/Productiviti/resources/';
    // public static DEV_URL = 'http://172.16.10.31:8080/Productiviti/resources/';
    // public static DEV_URL = 'http://192.168.1.66:8080/pistaceo/resources/';
    // public static DEV_URL = 'http://ecmdemo1:8080/pistaceo/resources/'; // Pr0dP@$$
    // public static DEV_URL = 'http://www.pistac.io/pistaceo31/resources/';
    // public static DEV_URL = 'https://jtc-dmsprodtest02.jtckw.com/pistaceo/resources/';
    AppURLSettings.DEV_URL = 'https://www.pistaceo.com/pistaceo/resources/';
    return AppURLSettings;
}());



/***/ }),

/***/ "../../../../../src/app/service/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_toastr__ = __webpack_require__("../../../../ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = (function () {
    function AuthGuard(router, tr) {
        this.router = router;
        this.tr = tr;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        if (sessionStorage.getItem('token')) {
            return true;
        }
        else {
            return true;
            // this.tr.warning('', 'You have to signin');
            //  this.router.navigateByUrl('/');
            //     sessionStorage.clear();
            // setTimeout(() => {
            //     window.location.reload();
            // }, 100);
        }
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2_ngx_toastr__["b" /* ToastrService */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "../../../../../src/app/service/content.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContentService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__global_variables__ = __webpack_require__("../../../../../src/app/global.variables.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__interceptor_service__ = __webpack_require__("../../../../../src/app/service/interceptor.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_service__ = __webpack_require__("../../../../../src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ContentService = (function () {
    function ContentService(http, us) {
        this.http = http;
        this.us = us;
        if (__WEBPACK_IMPORTED_MODULE_5__environments_environment__["a" /* environment */].production) {
            this.base_url = __WEBPACK_IMPORTED_MODULE_2__global_variables__["a" /* AppURLSettings */].SANDBOX_URL;
        }
        else {
            this.base_url = __WEBPACK_IMPORTED_MODULE_2__global_variables__["a" /* AppURLSettings */].DEV_URL;
        }
        this.header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        this.header.append('token', localStorage.getItem('token'));
        this.header.append('repo', localStorage.getItem('repoName'));
    }
    ContentService.prototype.getRepositories = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getRepositories?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getAppRepository = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getAppRepository?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getTopFolders = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getTopFolders?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getSubfolders = function (folderId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getSubfolders?id=" + folderId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getDocumentClasses = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getDocumentClasses?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getFolderDocuments = function (folderId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getFolderDocuments?id=" + folderId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.createSubfolder = function (foldername, folderId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/createSubfolder?parentid=" + folderId + "&name=" + foldername + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getUnfiledDocuments = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getUnfiledDocuments?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getDocumentFolders = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getDocumentFolders?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getRecent = function (empNo) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getRecent?empno=" + empNo + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getFavorites = function (empNo) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getFavorites?empno=" + empNo + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.addToFavorites = function (empno, id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/addToFavorites?empno=" + empno + "&id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.removeFromFavorites = function (empno, id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/removeFromFavorites?empno=" + empno + "&id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.moveToFolder = function (fromId, toid) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/moveToFolder?sourceid=" + fromId + "&targetid=" + toid + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getFolderActions = function (folderId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getFolderActions?id=" + folderId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.deleteDocument = function (documentId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/deleteDocument?id=" + documentId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.deleteFolder = function (folderId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/deleteFolder?folderid=" + folderId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.fileInFolder = function (folderId, documentId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/fileInFolder?folderid=" + folderId + "&id=" + documentId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.unfileFromFolder = function (folderId, documentId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/unfileFromFolder?folderid=" + folderId + "&id=" + documentId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.updateProperties = function (val) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/updateProperties?sysdatetime=" + fulldatetime;
        return this.http.post(url, val, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.addDocument = function (form) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/addDocument?sysdatetime=" + fulldatetime;
        return this.http.post(url, form, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.downloadMultipleDocuments = function (docIds) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        this.header.append('responseType', 'arraybuffer');
        var url = this.base_url + "ContentService/downloadMultipleDocuments?sysdatetime=" + fulldatetime;
        return this.http.post(url, docIds, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.downloadDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/downloadDocument?id=" + docId + "&sysdatetime=" + fulldatetime + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.downloadThisDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/downloadThisDocument?id=" + docId + "&sysdatetime=" + fulldatetime + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.downloadMultiDocuments = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/downloadMultiDocuments?docids=" + docId + "&sysdatetime=" + fulldatetime + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.checkOut = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/checkOut?id=" + docId + "&sysdatetime=" + fulldatetime + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getThisDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getThisDocument?id=" + docId + "&sysdatetime=" + fulldatetime + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.searchDocuments = function (request) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/search?sysdatetime=" + fulldatetime;
        return this.http.post(url, request, { headers: this.header }).map(function (res) { return res.json(); });
    };
    ContentService.prototype.cancelCheckOut = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/cancelCheckOut?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.checkIn = function (formData) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/checkIn?sysdatetime=" + fulldatetime;
        return this.http.post(url, formData, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.viewDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/downloadDocument?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getDocumentVersions = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getDocumentVersions?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getDocumentPermissions = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getDocumentPermissions?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getDocument = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getDocument?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getDocumentActions = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getDocumentActions?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getDocumentPageCount = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getDocumentPageCount?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getPreviewPage = function (docId, pagenumber) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getPreviewPage?id=" + docId + "&page=" + pagenumber + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.downloadPreviewPage = function (docId, pagenumber) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/downloadPreviewPage?id=" + docId + "&page=" + pagenumber + "&empno=" + this.us.getCurrentUser().EmpNo + "&sysdatetime=" + fulldatetime;
        return url;
    };
    ContentService.prototype.downloadAnnotation = function (docId, pagenumber) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/downloadAnnotation?id=" + docId + "&page=" + pagenumber + "&empno=" + this.us.getCurrentUser().EmpNo + "&sysdatetime=" + fulldatetime;
        return url;
    };
    ContentService.prototype.saveAnnotation = function (annotation) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/saveAnnotation?sysdatetime=" + fulldatetime;
        return this.http.post(url, annotation, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getAnnotations = function (documentId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getAnnotations?id=" + documentId + "&status=ALL&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getDocumentHistory = function (documentId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getDocumentHistory?id=" + documentId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getDocumentLinks = function (documentId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getDocumentLinks?docid=" + documentId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.downloadAnnotatedDocument = function (documentId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/downloadAnnotatedDocument?id=" + documentId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.downloadSignedDocument = function (documentId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/downloadSignedDocument?id=" + documentId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.completeAnnotation = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/completeAnnotation?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.deleteAnnotation = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/deleteAnnotation?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getJSONFromDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getJSONFromDocument?docId=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.addJSONDocument = function (formDetails) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/addJSONDocument?sysdatetime=" + fulldatetime;
        return this.http.post(url, formDetails, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getFolder = function (folderId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getFolder?id=" + folderId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.updateFolderSecurity = function (folderObj) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/updateFolderSecurity?sysdatetime=" + fulldatetime;
        return this.http.post(url, folderObj, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.setFolderPriority = function (folderId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/setFolderPriority?folderId=" + folderId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.updateDocumentSecurity = function (documentObj) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/updateDocumentSecurity?sysdatetime=" + fulldatetime;
        return this.http.post(url, documentObj, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getDocumentPagePermission = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/setFolderPriority?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.downloadSharedDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var token = localStorage.getItem('token');
        var url = this.base_url + "ContentService/downloadSharedDocument?id=" + docId + "&token=" + token + "&sysdatetime=" + fulldatetime;
        return url;
    };
    ContentService.prototype.renameFolder = function (folderId, folderName) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/renameFolder?folderid=" + folderId + "&name=" + folderName + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getSearchPreviewPage = function (docId, pageNum, searchText) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getSearchPreviewPage?id=" + docId + "&page=" + pageNum + "&text=" + searchText + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.downloadHLDocument = function (docId, pageNum, searchText) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/downloadHLDocument?id=" + docId + "&text=" + searchText + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.linkDocuments = function (firstdoc, seconddoc) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/linkDocuments?firstdoc=" + firstdoc + "&seconddoc=" + seconddoc + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.unlinkDocument = function (firstdoc, seconddoc) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/unlinkDocuments?firstdoc=" + firstdoc + "&seconddoc=" + seconddoc + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.indexDocument = function (firstdoc) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/indexDocument?id=" + firstdoc + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.downloadPrintDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/downloadPrintDocument?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    // deleteAnnotation(annotationId) {
    //   const sysDateTime = new Date();
    //   const fulldatetime = sysDateTime.getTime();
    //   const url = `${this.base_url}ContentService/linkDocuments?firstdoc=${firstdoc}&seconddoc=${seconddoc}&sysdatetime=${fulldatetime}`;
    //   return this.http.get(url , {headers : this.header}).map(
    //     res => res
    //   );
    // }
    ContentService.prototype.changeDocumentClass = function (id, className) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/changeDocumentClass?id=" + id + "&class=" + className + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.getFolderContentsAsExcel = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/getFolderContentsAsExcel?id=" + id + "&sysdatetime=" + fulldatetime;
        //  return url;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    ContentService.prototype.exportSearchToExcel = function (search) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "ContentService/exportSearchToExcel?sysdatetime=" + fulldatetime;
        return this.http.post(url, search, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    ContentService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__interceptor_service__["a" /* HttpInterceptor */], __WEBPACK_IMPORTED_MODULE_4__user_service__["a" /* UserService */]])
    ], ContentService);
    return ContentService;
}());



/***/ }),

/***/ "../../../../../src/app/service/document.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocumentService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__global_variables__ = __webpack_require__("../../../../../src/app/global.variables.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__interceptor_service__ = __webpack_require__("../../../../../src/app/service/interceptor.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__user_service__ = __webpack_require__("../../../../../src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var DocumentService = (function () {
    function DocumentService(http, us) {
        this.http = http;
        this.us = us;
        if (__WEBPACK_IMPORTED_MODULE_8__environments_environment__["a" /* environment */].production) {
            this.base_url = __WEBPACK_IMPORTED_MODULE_5__global_variables__["a" /* AppURLSettings */].SANDBOX_URL;
        }
        else {
            this.base_url = __WEBPACK_IMPORTED_MODULE_5__global_variables__["a" /* AppURLSettings */].DEV_URL;
        }
        this.header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        this.header.append('token', localStorage.getItem('signapptoken'));
    }
    DocumentService.prototype.addDocument = function (form) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/addDocument?sysdatetime=" + fulldatetime;
        return this.http.post(url, form, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.downloadMultipleDocuments = function (docIds) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/downloadMultipleDocuments?sysdatetime=" + fulldatetime;
        return this.http.post(url, docIds, { headers: this.header }).map(function (res) { return res; });
        //  return url;
    };
    DocumentService.prototype.downloadDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/downloadDocument?id=" + docId + "&sysdatetime=" + fulldatetime;
        //  return url;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.downloadThisDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/downloadThisDocument?id=" + docId + "&sysdatetime=" + fulldatetime;
        // return url;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.checkOut = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/checkOut?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getThisDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getThisDocument?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    // searchDocuments(request: any): any {
    //   const url = `${this.base_url}DocumentService/search`;
    //   return this.http.post(url, request, {headers : this.header}).map(res => res.json());
    // }
    DocumentService.prototype.cancelCheckOut = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/cancelCheckOut?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.checkIn = function (formData) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/checkIn?sysdatetime=" + fulldatetime;
        return this.http.post(url, formData, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.viewDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/downloadDocument?id=" + docId + "&sysdatetime=" + fulldatetime;
        // return url;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getDocumentFolders = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getDocumentFolders?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getFavorites = function (empNo) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getFavorites?empno=" + empNo + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.addToFavorites = function (empno, id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/addToFavorites?empno=" + empno + "&id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.removeFromFavorites = function (empno, id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/removeFromFavorites?empno=" + empno + "&id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getDocumentVersions = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getDocumentVersions?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getDocumentPermissions = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getDocumentPermissions?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getRecent = function (empNo) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getRecent?empno=" + empNo + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getDocumentPageCount = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getDocumentPageCount?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getPreviewPage = function (docId, pagenumber) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getPreviewPage?id=" + docId + "&page=" + pagenumber + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.downloadPreviewPage = function (docId, pagenumber) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var token = localStorage.getItem('signapptoken');
        var url = this.base_url + "DocumentService/downloadPreviewPage?id=" + docId + "&page=" + pagenumber + "&empno=" + this.us.getCurrentUser().EmpNo + "&token=" + token + "&sysdatetime=" + fulldatetime;
        return url;
    };
    DocumentService.prototype.downloadAnnotation = function (docId, pagenumber) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var token = localStorage.getItem('signapptoken');
        var url = this.base_url + "DocumentService/downloadAnnotation?id=" + docId + "&page=" + pagenumber + "&empno=" + this.us.getCurrentUser().EmpNo + "&token=" + token + "&sysdatetime=" + fulldatetime;
        return url;
    };
    DocumentService.prototype.saveAnnotation = function (annotation) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/saveAnnotation?sysdatetime=" + fulldatetime;
        return this.http.post(url, annotation, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getAnnotations = function (documentId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getAnnotations?id=" + documentId + "&status=ALL&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.updateProperties = function (val) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/updateProperties?sysdatetime=" + fulldatetime;
        return this.http.post(url, val, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.downloadAnnotatedDocument = function (documentId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/downloadAnnotatedDocument?id=" + documentId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.downloadSignedDocument = function (documentId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/downloadSignedDocument?id=" + documentId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.completeAnnotation = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/completeAnnotation?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.deleteAnnotation = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/deleteAnnotation?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getJSONFromDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getJSONFromDocument?docId=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.addJSONDocument = function (formDetails) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/addJSONDocument?sysdatetime=" + fulldatetime;
        return this.http.post(url, formDetails, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.downloadPrintDocument = function (docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/downloadPrintDocument?id=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getFormJSONDocumentForActivity = function (docId, activitiId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getFormJSONDocumentForActivity?docId=" + docId + "&activityId=" + activitiId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getDocumentActions = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getDocumentActions?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.saveFormDocument = function (formDetails) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/saveFormDocument?sysdatetime=" + fulldatetime;
        return this.http.post(url, formDetails, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.canSignDocumentPage = function (docid, pageno, activityid) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/canSignDocumentPage?docid=" + docid + "&pageno=" + pageno + "&&activityid=" + activityid + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.signDocumentPageWithStoredSignature = function (docid, pageno) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/signDocPageWithStoredSignature?docid=" + docid + "&pageno=" + pageno + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.signDocPageWithNewSignature = function (value) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/signDocPageWithNewSignature?sysdatetime=" + fulldatetime;
        return this.http.post(url, value, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.signDocPageWithFileSignature = function (value) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/signDocPageWithFileSignature?sysdatetime=" + fulldatetime;
        return this.http.post(url, value, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.validateSignRequest = function (value) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/validateSignRequest?sysdatetime=" + fulldatetime;
        return this.http.post(url, value).map(function (res) { return res; });
    };
    DocumentService.prototype.validateSignaturePIN = function (value, token) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        // localStorage.removeItem('token');
        // localStorage.setItem('token', token);
        this.header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        this.header.append('token', token);
        var url = this.base_url + "DocumentService/validateSignaturePIN?sysdatetime=" + fulldatetime;
        return this.http.post(url, value, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getPagesForSignature = function (docid, roleid) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getPagesForSignature?docid=" + docid + "&roleid=" + roleid + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService.prototype.getSignHolders = function (docid, page, roleid) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "DocumentService/getSignHolders?docid=" + docid + "&page=" + page + "&roleid=" + roleid + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    DocumentService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__interceptor_service__["a" /* HttpInterceptor */], __WEBPACK_IMPORTED_MODULE_7__user_service__["a" /* UserService */]])
    ], DocumentService);
    return DocumentService;
}());



/***/ }),

/***/ "../../../../../src/app/service/interceptor.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/_esm5/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__node_modules_ngx_toastr__ = __webpack_require__("../../../../ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__environments_environment_prod__ = __webpack_require__("../../../../../src/environments/environment.prod.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__global_variables__ = __webpack_require__("../../../../../src/app/global.variables.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// operators







var HttpInterceptor = (function (_super) {
    __extends(HttpInterceptor, _super);
    function HttpInterceptor(backend, options, http, router, tr) {
        var _this = _super.call(this, backend, options) || this;
        _this.http = http;
        _this.router = router;
        _this.tr = tr;
        _this.flag = 0;
        // ngOnInit() {
        //     this.flag = 0;
        // }
        _this.handleError = function (error) {
            var errorCaught = JSON.parse(error._body);
            if (errorCaught.code === 1001 && _this.flag === 0) {
                _this.flag = 1;
                // this.errorCall(); http://ecmdemo1:8080/pistaceo/resources/UserService/authenticateUser?
                if (__WEBPACK_IMPORTED_MODULE_8__environments_environment_prod__["a" /* environment */].production) {
                    _this.base_url = __WEBPACK_IMPORTED_MODULE_9__global_variables__["a" /* AppURLSettings */].SANDBOX_URL;
                }
                else {
                    _this.base_url = __WEBPACK_IMPORTED_MODULE_9__global_variables__["a" /* AppURLSettings */].DEV_URL;
                }
                // this.header = new Headers();
                // const url = `${this.base_url}UserService/authenticateUser?`;
                // const headers = new Headers();
                // return this.http.get(url, { headers: headers }).map(
                //     data => {
                //         this.storeToken(data);
                //     }).catch((err: any) => Observable.throw(this.errorHandler1(err)));
            }
            else {
                _this.tr.error(errorCaught.message, errorCaught.status);
            }
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["a" /* Observable */].throw(error);
        };
        _this.flag = 0;
        return _this;
    }
    HttpInterceptor.prototype.request = function (url, options) {
        return _super.prototype.request.call(this, url, options)
            .catch(this.handleError);
    };
    HttpInterceptor.prototype.errorHandler1 = function (e) {
        var error = JSON.parse(e._body);
        console.log(error);
        if (error.code === 9999) {
            alert('Token is expired you need to login again');
            this.router.navigateByUrl('/');
            localStorage.removeItem('signapptoken');
            setTimeout(function () {
                window.location.reload();
            }, 100);
        }
    };
    HttpInterceptor.prototype.storeToken = function (data) {
        localStorage.setItem('signapptoken', data._body);
        this.header.append('signapptoken', localStorage.getItem('signapptoken'));
        setTimeout(function () {
            window.location.reload();
        }, 0);
    };
    HttpInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* XHRBackend */],
            __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */],
            __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_6__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_7__node_modules_ngx_toastr__["b" /* ToastrService */]])
    ], HttpInterceptor);
    return HttpInterceptor;
}(__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]));



/***/ }),

/***/ "../../../../../src/app/service/schema.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SchemaService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__global_variables__ = __webpack_require__("../../../../../src/app/global.variables.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__interceptor_service__ = __webpack_require__("../../../../../src/app/service/interceptor.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SchemaService = (function () {
    function SchemaService(http) {
        this.http = http;
        if (__WEBPACK_IMPORTED_MODULE_7__environments_environment__["a" /* environment */].production) {
            this.base_url = __WEBPACK_IMPORTED_MODULE_5__global_variables__["a" /* AppURLSettings */].SANDBOX_URL;
        }
        else {
            this.base_url = __WEBPACK_IMPORTED_MODULE_5__global_variables__["a" /* AppURLSettings */].DEV_URL;
        }
        this.header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        this.header.append('token', localStorage.getItem('token'));
    }
    SchemaService.prototype.getWorkTypes = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "SchemaService/getWorkTypes?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    SchemaService.prototype.getRegisterWorkTypes = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "SchemaService/getRegisterWorkTypes?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    SchemaService.prototype.getWorkTypeFromID = function (activityId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "SchemaService/getActivityTypeFromID?id=" + activityId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    SchemaService.prototype.getActivityTypeFromID = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "SchemaService/getActivityTypeFromID?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    SchemaService.prototype.getWorkCategories = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "SchemaService/getWorkCategories?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    SchemaService.prototype.getWorkRegisterTypes = function () {
        // const sysDateTime = new Date();
        // const fulldatetime = sysDateTime.getTime();
        // const url = `${this.base_url}SchemaService/getWorkRegisterTypeSearch?id=${id}`;
        // return this.http.get(url, {headers: this.header}).map(
        //   res => res
        // );
    };
    SchemaService.prototype.saveWorkCategory = function (value) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "SchemaService/saveWorkCategory?sysdatetime=" + fulldatetime;
        return this.http.post(url, value, { headers: this.header }).map(function (res) { return res; });
    };
    SchemaService.prototype.getWorkTypeSearch = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "SchemaService/getWorkTypeSearch?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    SchemaService.prototype.getWorkTypesForSearch = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "SchemaService/getWorkTypesForSearch?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    SchemaService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__interceptor_service__["a" /* HttpInterceptor */]])
    ], SchemaService);
    return SchemaService;
}());



/***/ }),

/***/ "../../../../../src/app/service/stroe.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StoreService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__interceptor_service__ = __webpack_require__("../../../../../src/app/service/interceptor.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_service__ = __webpack_require__("../../../../../src/app/service/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StoreService = (function () {
    function StoreService(http, us) {
        this.http = http;
        this.us = us;
    }
    StoreService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__interceptor_service__["a" /* HttpInterceptor */], __WEBPACK_IMPORTED_MODULE_2__user_service__["a" /* UserService */]])
    ], StoreService);
    return StoreService;
}());



/***/ }),

/***/ "../../../../../src/app/service/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__global_variables__ = __webpack_require__("../../../../../src/app/global.variables.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__interceptor_service__ = __webpack_require__("../../../../../src/app/service/interceptor.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var UserService = (function () {
    function UserService(http) {
        this.http = http;
        if (__WEBPACK_IMPORTED_MODULE_8__environments_environment__["a" /* environment */].production) {
            this.base_url = __WEBPACK_IMPORTED_MODULE_6__global_variables__["a" /* AppURLSettings */].SANDBOX_URL;
        }
        else {
            this.base_url = __WEBPACK_IMPORTED_MODULE_6__global_variables__["a" /* AppURLSettings */].DEV_URL;
        }
        this.header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        if (localStorage.getItem('signapptoken') !== null) {
            this.header.append('signapptoken', localStorage.getItem('signapptoken'));
        }
    }
    UserService.prototype.authenticateUser = function (username, password, policy) {
        localStorage.removeItem('token');
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('userid', username);
        headers.append('password', password);
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var timeZone = new Date().getTimezoneOffset();
        headers.append('timezone', timeZone + '');
        // const url = `${this.base_url}UserService/authenticateUser?savelogin=${policy}&sysdatetime=${fulldatetime}`;
        // return this.http.get(url, { headers: headers }).map(
        //   response => { this.responsefromlog(response); }
        // );
    };
    UserService.prototype.responsefromlog = function (data) {
        localStorage.setItem('signapptoken', data._body);
        this.header.append('signapptoken', localStorage.getItem('signapptoken'));
    };
    UserService.prototype.searchUsers = function (text) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/searchUsers?key=NAME&text=" + text + "&usertype=role&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.errorHandler = function (error) {
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error || 'Server Error');
    };
    UserService.prototype.userDetail = function (res) {
        var x = res._body;
    };
    UserService.prototype.getCurrentUser = function () {
        return JSON.parse(localStorage.getItem('user'));
    };
    UserService.prototype.getRoles = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/getRoles?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.getUserDetails = function (userId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/getUserDetails?userid=" + userId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.getUsers = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/getUsers?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.saveUser = function (user) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/saveUser?sysdatetime=" + fulldatetime;
        return this.http.post(url, user, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.saveRole = function (role) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/saveRole?sysdatetime=" + fulldatetime;
        return this.http.post(url, role, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.getOrgRoles = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/getOrgRoles?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.getSubOrgRoles = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/getSubOrgRoles?orgId=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.getUserDelegations = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/getUserDelegations?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.saveDelegation = function (userDetail) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/saveDelegation?sysdatetime=" + fulldatetime;
        return this.http.post(url, userDetail, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.getRoleMembers = function (roleid) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/getRoleMembers?roleId=" + roleid + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.revokeDelegation = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/revokeDelegation?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.generatePin = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/generatePIN?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.saveSignature = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/saveSignature?signature=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.validatepin = function (pin) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/validatePIN?pin=" + pin + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.searchRoles = function (text) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/searchRoles?text=" + text + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.changeUserPassword = function (password) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/changeUserPassword?sysdatetime=" + fulldatetime;
        return this.http.post(url, password, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.updateUserSettings = function (updateSetting) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/updateUserSettings?sysdatetime=" + fulldatetime;
        return this.http.post(url, updateSetting, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.getUserHistoryItems = function () {
        var sysDateTime = new Date();
        var otherDate = new Date().toUTCString();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/getUserHistory?before=" + otherDate + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.searchSubordinateRoles = function (text) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/searchSubordinateRoles?text=" + text + "&roleid=" + this.getCurrentUser().roles[0].id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService.prototype.logoutUser = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "UserService/logoutUser?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    UserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__interceptor_service__["a" /* HttpInterceptor */]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "../../../../../src/app/service/work.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WorkService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/throw.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__global_variables__ = __webpack_require__("../../../../../src/app/global.variables.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__user_service__ = __webpack_require__("../../../../../src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__interceptor_service__ = __webpack_require__("../../../../../src/app/service/interceptor.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var WorkService = (function () {
    function WorkService(http, us) {
        this.http = http;
        this.us = us;
        if (__WEBPACK_IMPORTED_MODULE_8__environments_environment__["a" /* environment */].production) {
            this.base_url = __WEBPACK_IMPORTED_MODULE_5__global_variables__["a" /* AppURLSettings */].SANDBOX_URL;
        }
        else {
            this.base_url = __WEBPACK_IMPORTED_MODULE_5__global_variables__["a" /* AppURLSettings */].DEV_URL;
        }
        this.header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        this.header.append('token', localStorage.getItem('signapptoken'));
    }
    WorkService.prototype.saveWork = function (value) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/saveWork?sysdatetime=" + fulldatetime;
        return this.http.post(url, value, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getWorkStatistics = function (empno, datetype, type) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getWorkStatistics?empNo=" + empno + "&dateType=" + datetype + "&type=" + type + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getActivityStatistics = function (empno, datetype, type) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getActivityStatistics?empNo=" + empno + "&dateType=" + datetype + "&type=" + type + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getOrgWorkStatistics = function (orgId, datetype, type) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getOrgWorkStatistics?orgId=" + orgId + "&dateType=" + datetype + "&type=" + type + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getOrgActivityStatistics = function (orgId, datetype, type) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getOrgActivityStatistics?orgId=" + orgId + "&dateType=" + datetype + "&type=" + type + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getOrgWorkItems = function (orgId, datetype, status) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getOrgWorkItems?orgId=" + orgId + "&dateType=" + datetype + "&status=" + status + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getOrgWorkItemsType = function (orgId, dateType, typeId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getOrgWorkItems?orgId=" + orgId + "&dateType=" + dateType + "&typeId=" + typeId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getUserWorkItemsType = function (dateType, typeId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getUserWorkItems?dateType=" + dateType + "&typeId=" + typeId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getUserWorkItemsStatus = function (dateType, status) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getUserWorkItems?dateType=" + dateType + "&status=" + status + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getOrgAcitityItemsType = function (orgId, dateType, typeId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getOrgActivityItems?orgId=" + orgId + "&dateType=" + dateType + "&typeId=" + typeId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getOrgActivityItems = function (orgId, datetype, status) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getOrgActivityItems?orgId=" + orgId + "&dateType=" + datetype + "&status=" + status + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getAcitvityHistory = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getWorkHistory?activityId=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getSentItems = function (empId, page) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getSentItems?empNo=" + empId + "&pageNo=" + page + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getInboxItems = function (empId, page) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getInboxItems?empNo=" + empId + "&pageNo=" + page + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getDraftItems = function (empId, page) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getDraftItems?empNo=" + empId + "&pageNo=" + page + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getArchiveItems = function (empId, page) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getArchiveItems?empNo=" + empId + "&pageNo=" + page + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getActivityInfo = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getActivityInfo?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.saveActivity = function (formdetails) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/saveActivity?sysdatetime=" + fulldatetime;
        return this.http.post(url, formdetails, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.finishActivity = function (activity) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/finishActivity?sysdatetime=" + fulldatetime;
        return this.http.post(url, activity, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.markActivityAsRead = function (empNo, id, roleId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/markActivityAsRead?id=" + id + "&empNo=" + empNo + "&roleId=" + roleId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getWorkHistory = function (workId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getWorkHistory?workId=" + workId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.searchInbox = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/searchInbox?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.searchSentItems = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/searchSentItems?sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.recallActivity = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/recallActivity?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getActivityResponses = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getActivityResponses?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.finishMultipleActivities = function (formdetails) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/finishMultipleActivities?sysdatetime=" + fulldatetime;
        return this.http.post(url, formdetails, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getWorkRegister = function (type, category) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getWorkRegister?type=" + type + "&category=" + category + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getWorkAttachments = function (workId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getWorkAttachments?workId=" + workId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getWorkDraftActivity = function (workid) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getWorkDraftActivity?workId=" + workid + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getWorkActivity = function (workId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getWorkActivity?workId=" + workId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.searchWork = function (request) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/searchWork?sysdatetime=" + fulldatetime;
        return this.http.post(url, request, { headers: this.header }).map(function (res) { return res.json(); });
    };
    WorkService.prototype.updatePrimaryDocument = function (workId, activityid) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/updatePrimaryDocument?id=" + workId + "&activityid=" + activityid + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.updateSubject = function (activityId, subject) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/updateSubject?actid=" + activityId + "&subject=" + subject + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.initiateTaskFlow = function (workId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/initiateTaskFlow?docid=" + workId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getActivityItems = function (datetype, status) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getUserActivityItems?dateType=" + datetype + "&status=" + status + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getActivityItemsType = function (datetype, typeId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getUserActivityItems?dateType=" + datetype + "&typeId=" + typeId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.flagActivity = function (workid, flag, roleId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/flagActivity?id=" + workid + "&flag=" + flag + "&roleId=" + roleId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.setAsPrimaryDocument = function (workid, docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/setAsPrimaryDocument?workid=" + workid + "&docid=" + docId + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getActivityForAction = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getActivityForAction?id=" + id + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.sendPendingActivityNotifications = function (workid) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/sendPendingActivityNotifications?workid=" + workid + "&sysdatetime=" + fulldatetime;
        return this.http.get(url, { headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getInboxItemsAsExcel = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getInboxItemsAsExcel?sysdatetime=" + fulldatetime;
        //  return url;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getSentItemsAsExcel = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getSentItemsAsExcel?sysdatetime=" + fulldatetime;
        //  return url;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getArchiveItemsAsExcel = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getArchiveItemsAsExcel?sysdatetime=" + fulldatetime;
        //  return url;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.getDraftItemsAsExcel = function () {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/getDraftItemsAsExcel?sysdatetime=" + fulldatetime;
        //  return url;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.searchWorkToExcel = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/searchWorkToExcel?sysdatetime=" + fulldatetime;
        return this.http.post(url, id, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.removeWorkAttachment = function (id) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/removeWorkAttachment?id=" + id + "&sysdatetime=" + fulldatetime;
        //  return url;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    WorkService.prototype.updateAttachmentActivity = function (id, docId) {
        var sysDateTime = new Date();
        var fulldatetime = sysDateTime.getTime();
        var url = this.base_url + "WorkService/updateAttachmentActivity?id=" + id + "&activity=" + docId + "&sysdatetime=" + fulldatetime;
        //  return url;
        return this.http.get(url, { responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* ResponseContentType */].Blob, headers: this.header }).map(function (res) { return res; });
    };
    WorkService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__interceptor_service__["a" /* HttpInterceptor */], __WEBPACK_IMPORTED_MODULE_6__user_service__["a" /* UserService */]])
    ], WorkService);
    return WorkService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.prod.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true,
    sso: false
};


/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true,
    sso: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map